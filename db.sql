-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 05/11/2021 às 19:21
-- Versão do servidor: 5.7.31
-- Versão do PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `teste`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `accounts`
--

CREATE TABLE `accounts` (
  `account_id` int(10) UNSIGNED NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_username` varchar(255) NOT NULL,
  `account_passwd` varchar(255) NOT NULL,
  `account_reg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_level` varchar(255) NOT NULL,
  `account_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `accounts`
--

INSERT INTO `accounts` (`account_id`, `account_name`, `account_username`, `account_passwd`, `account_reg_time`, `account_level`, `account_enabled`, `modified`, `created`) VALUES
(1, 'Bruno Luiz', 'bruno@lnxweb.com.br', '$2y$10$hlxRXZbAlwApnxZQiLh6k.a1FVKiLDmoZBIRobz/D8uBu.BriBsRK', '2020-10-16 15:35:20', 'admin', 1, '2021-09-22 11:38:24', '2021-03-15 02:01:44'),
(2, 'Rafael', 'rafael@lnxweb.com.br', '$2y$10$rL.Lon879PcnwvPAw5nKWu5pG8dL4aCetGVq27v/Sog6PQY1yyHna', '2020-10-16 15:35:20', 'admin', 1, '2021-09-22 14:25:12', '2021-03-15 02:01:44'),
(3, 'Dev', 'dev@lnxweb.com.br', '$2y$10$1kBVQFd60GDG05v8usEM8Oi8YQXM4akpUb064E/8hBp8szk8q2eQ2', '2020-10-18 02:11:55', 'admin', 1, '2021-11-05 15:52:05', '2021-03-15 02:01:44');

-- --------------------------------------------------------

--
-- Estrutura para tabela `account_sessions`
--

CREATE TABLE `account_sessions` (
  `session_id` varchar(255) NOT NULL,
  `account_id` int(10) UNSIGNED NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `account_sessions`
--

INSERT INTO `account_sessions` (`session_id`, `account_id`, `login_time`) VALUES
('0i4ij6cpsa3qsljm7s181sluro', 1, '2020-10-23 13:43:16'),
('0itpkft3atiot6ngcf1m535lsn', 1, '2021-05-28 21:52:59'),
('0mkti7i66ooulpcdq4t2l2b5cf', 1, '2021-09-21 22:17:29'),
('1kijami5u0tgj14vn3bsor0ira', 3, '2021-03-17 14:17:02'),
('26ftna8nl6fa79ic3d6106olgm', 3, '2021-02-17 20:54:31'),
('2gphnmnuo3ggk3ke9aeofodbf2', 2, '2020-10-17 14:49:10'),
('2hl9vjj459b5v0biidmc2dr8od', 4, '2021-03-24 14:38:14'),
('2hsc80a0avcuk95o3k6s1ttq1a', 3, '2021-08-02 17:29:00'),
('2kqlth9fc9i9oillivrdns7enj', 2, '2020-10-18 03:08:17'),
('2nfnjh1uh8m5na59sng2v73e0c', 2, '2021-02-09 23:22:21'),
('2v96jupfpelqnkcpgoqb0vds23', 1, '2020-10-21 21:23:52'),
('32q4v351fv824lv4ek0t74huak', 1, '2021-09-14 12:51:37'),
('37amsa2u42595gb4n59iku5j0o', 4, '2021-04-01 01:09:10'),
('3d129mu7ipcngig4ucu6uo2aoo', 1, '2021-09-20 02:13:23'),
('3imrbndnc6s6b8a04flbnp31hh', 1, '2021-08-16 16:04:02'),
('3tmm9qk2a6knlkse9p2q6inn59', 1, '2021-08-19 21:46:31'),
('3v1h5ude5kquocmfjlmjs76ran', 4, '2021-09-16 21:45:52'),
('561rpvutv58i6i13usv0pk3mpm', 3, '2021-02-19 22:44:25'),
('5lps93l8m801j9cggkejv19dn0', 4, '2021-04-12 15:38:50'),
('633kum8qnv3ni55qq01he2npqq', 1, '2021-01-13 18:26:19'),
('6vrafuaa6ubif9cngk0eo1r9dm', 1, '2021-09-09 01:00:55'),
('71qh0jup6b2ks96250dsg4jb9s', 3, '2021-03-14 15:48:31'),
('78nncbunv02769d7n6micv9t9l', 3, '2021-03-11 16:15:47'),
('7g88qemjbbovrd5od95fl0ge64', 1, '2021-01-18 19:06:21'),
('7m61ltndekv65khvte28eaki3o', 1, '2021-07-11 21:49:32'),
('7u88fs8d9ilvm80ek5ss14r3mh', 1, '2021-08-25 16:44:43'),
('8cvsu9bf77pvcssg3fojsnogjr', 3, '2021-03-12 14:59:40'),
('8deitrbise5cntjnok1pdndu4n', 1, '2021-09-16 13:37:03'),
('8g0pvbmo1t83pldgir546g4ujt', 1, '2020-11-25 13:45:57'),
('8u6d5ha4tas88d2nbdruh72um3', 1, '2021-07-14 23:05:35'),
('99dofdsmfmv6d129pduj4j37mp', 3, '2021-02-17 21:09:02'),
('9krb8mqir5bms08aesh4rtd9pb', 1, '2020-11-09 12:23:38'),
('a7gbgmq89jk7r1hjdgp2r6j3mr', 3, '2021-07-25 23:52:04'),
('aagcnno8776c2crg9gsea0ak86', 3, '2021-02-17 20:48:01'),
('aivjplt19q5636gb53fifhpq6b', 1, '2020-12-21 15:07:00'),
('aou17tcurip52br7n6l8qksp45', 1, '2021-03-17 19:32:15'),
('ar6dhb7v5ucd3clg9uvlka72ho', 1, '2020-11-16 12:55:04'),
('b57ntp9id1n5mqs9cm6mmk5pvf', 1, '2020-11-03 14:15:36'),
('bmp8a72ljhg5s8o0pah4peqrmm', 1, '2021-09-11 18:54:29'),
('bsb12hq5e5rehac5l5bimnkps2', 3, '2021-03-05 21:17:18'),
('ccssaqnvt44fj586qpmisjfvgj', 1, '2021-11-05 18:19:13'),
('cmsbs5nvbcooinu8vm1peu8apu', 1, '2021-07-20 20:44:51'),
('crghpg3u8k2vl888evftv0tbor', 1, '2020-11-06 14:58:00'),
('dbfc04mhaht8hkj44ns6ng0i56', 1, '2021-01-21 22:27:08'),
('dbjlrrkm92826rhkm0fbk1f25t', 7, '2021-06-06 18:53:36'),
('dqr69b1i760s5m6fch1uroscaj', 1, '2021-07-13 20:38:25'),
('ej72p590jhaqpogl0o9kln6bmg', 1, '2021-01-25 15:30:00'),
('ejjc5q15cs3ejhg2l7qth1hq10', 1, '2021-05-28 13:26:12'),
('et64rjblflsl55s2be5hk6dcng', 1, '2020-12-07 21:16:10'),
('evhenngoq19jdh1ooulk5ctae8', 3, '2021-02-23 16:47:12'),
('f42397je8rpi6ha7e919vhlhvg', 1, '2021-01-27 16:42:45'),
('fas3vt9hfo6hpnu9vscg128ptk', 1, '2021-01-23 18:33:13'),
('fgplag6u8helgemr8k32kafbfa', 1, '2021-09-08 13:58:11'),
('fjela1n6krbd5oebvmil4tqiuc', 1, '2020-12-15 16:01:50'),
('g626e93c5ctm745ft0ajlpci9e', 1, '2021-08-23 16:27:18'),
('hrek6lk9h6otk7k5s5bjn5q5g4', 1, '2021-05-21 19:12:35'),
('htsh50apqkmke8sp9ma825erf7', 1, '2020-11-23 13:35:49'),
('jhue01ifi2i07rqj94vlqmng5k', 1, '2021-05-19 17:25:56'),
('k33qcpqnrkvbcq6jfhknhpii9t', 2, '2021-09-29 15:10:51'),
('k55neuundmdnte8bbp1qal7b66', 1, '2021-06-10 19:26:12'),
('k5kufqtg718ntl1ne9rmji26ej', 3, '2021-07-08 14:19:54'),
('kc5s3r81vihaibg8ih9r6dpf0v', 3, '2021-02-17 19:38:16'),
('komedu5fdkh3eicp4ln12b23c5', 1, '2020-11-11 13:29:05'),
('l8r76jivhqkop88k9ic20qht8q', 1, '2021-09-23 20:43:26'),
('lueneo3gmvqoa2r34knekmvkni', 1, '2021-01-11 12:53:18'),
('mcbm4i9sej4u29qvfct45tk29c', 3, '2021-03-04 19:42:43'),
('mebsdjpi36v3ot5nqp3c8hf1cq', 1, '2021-02-10 17:39:15'),
('n7ea2lf4d12c0djtj559qhitrp', 1, '2020-12-23 17:13:32'),
('nmkekks47mcf25r9damg863083', 3, '2021-02-17 20:48:56'),
('nuifd7tkuohvfc45f0kjekho55', 1, '2021-02-17 21:10:16'),
('obj6phj7652ut6lr4pt9v73gb6', 3, '2021-02-17 14:31:36'),
('offupq7pn0uhloaq27afjtlc4j', 1, '2021-01-21 19:44:36'),
('ola516r2ts4nol8pihtb495346', 4, '2021-03-28 16:33:40'),
('otqapp743r8cqv0rakd5l0lvd1', 1, '2020-12-01 18:57:44'),
('p70ba601cqvoj4n4mm7ited2vq', 1, '2020-11-27 15:49:23'),
('qpvs9vdmn8fi2loen5oqgud4rg', 1, '2021-01-08 14:25:00'),
('qqnbre4q91c8ve92td1v02jp34', 3, '2021-07-23 19:57:08'),
('r3bmrk1b0ciumdhmja3ld76okg', 1, '2021-09-01 21:46:04'),
('r4ap6hnluuutiq1jh8mimltfof', 1, '2020-12-17 16:07:34'),
('rdo9rs4npgj04fqu442mut31eq', 1, '2021-05-24 20:53:54'),
('rdrhsvleo521u646iavvvt4kru', 1, '2020-10-18 14:55:21'),
('sdcqvgbh5vef3jkla7q7s0463u', 1, '2020-12-13 15:14:02'),
('sdhpgbkr7v6rm43etlqlqvk4ql', 1, '2020-12-09 18:04:54'),
('sl9ie9m9htbuf8c3s878jlbkgh', 1, '2021-02-11 16:19:43'),
('stbuhql0vtb5lhb72vi8iirs5m', 1, '2021-02-17 17:31:33'),
('t2qdeml2acma75llqkv3t0u0hv', 1, '2020-10-26 14:58:23'),
('tdoldrcfppe14u8phs5apig61c', 1, '2021-07-30 14:59:13'),
('tim6umts4ljgk7b4m1aqld85uj', 3, '2021-03-07 19:43:54'),
('tl7dk36vkdh2pbn7dqn39k1qfj', 1, '2021-01-05 16:17:09'),
('trpv89a9jahnthehk8batklmf0', 7, '2021-06-08 19:40:42'),
('umqa5tlrpi68os6995f6faocdu', 3, '2021-07-08 00:51:06'),
('v18h976tl3fisl0qdnr23vjlhk', 5, '2021-04-22 17:50:14'),
('vhh6l34jegdrlk2doojj4urrh6', 3, '2021-03-01 14:23:06'),
('vhjtesgug9c8k40b541rimh8qa', 1, '2021-07-31 13:28:51');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

CREATE TABLE `clientes` (
  `cliente_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresas`
--

CREATE TABLE `empresas` (
  `empresas_id` int(11) NOT NULL,
  `nome_fantasia` varchar(255) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `cnpj` varchar(255) NOT NULL,
  `cep` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `endereco_n` varchar(255) NOT NULL,
  `endereco_complemento` varchar(255) NOT NULL,
  `endereco_bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `nome_responsavel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `celular` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `email_relatorios` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `logo_path` text,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `service_enabled` int(11) DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `empresas`
--

INSERT INTO `empresas` (`empresas_id`, `nome_fantasia`, `razao_social`, `cnpj`, `cep`, `endereco`, `endereco_n`, `endereco_complemento`, `endereco_bairro`, `cidade`, `uf`, `nome_responsavel`, `email`, `celular`, `telefone`, `email_relatorios`, `website`, `logo_path`, `enabled`, `service_enabled`, `created`, `modified`) VALUES
(1, 'LNXWEB TECNOLOGI1', 'LNXWEB TECNOLOGIA DA INFORMACAO LTDA', '37947006000180', '03721035', 'Rua Pedro de Brito', '93', 'Casa 01', 'Cangaiba', 'São Paulo', 'SP', 'Rafael Almeida', 'rafael@lnxweb.com.br', '11964185042', '1137741095', 'bruno@lnxweb.com.br', NULL, NULL, 1, 1, '2020-10-16 23:46:20', '2021-11-05 15:50:00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `page_permissions`
--

CREATE TABLE `page_permissions` (
  `page_permissions_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `dashboard` int(11) NOT NULL,
  `empresas` int(11) NOT NULL,
  `transportadoras` int(11) NOT NULL,
  `fornecedores` int(11) NOT NULL,
  `clientes` int(11) NOT NULL,
  `produtos` int(11) NOT NULL,
  `relatorios` int(11) NOT NULL,
  `administracao` int(11) NOT NULL,
  `gerenciar_usuarios` int(11) NOT NULL,
  `registros` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `page_permissions`
--

INSERT INTO `page_permissions` (`page_permissions_id`, `role`, `dashboard`, `empresas`, `transportadoras`, `fornecedores`, `clientes`, `produtos`, `relatorios`, `administracao`, `gerenciar_usuarios`, `registros`, `modified`, `created`) VALUES
(2, 'admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, '2021-03-09 10:44:28'),
(4, 'colaborador', 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, NULL, '2021-03-09 10:44:28');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Índices de tabela `account_sessions`
--
ALTER TABLE `account_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Índices de tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Índices de tabela `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`empresas_id`);

--
-- Índices de tabela `page_permissions`
--
ALTER TABLE `page_permissions`
  ADD PRIMARY KEY (`page_permissions_id`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `accounts`
--
ALTER TABLE `accounts`
  MODIFY `account_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `empresas`
--
ALTER TABLE `empresas`
  MODIFY `empresas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de tabela `page_permissions`
--
ALTER TABLE `page_permissions`
  MODIFY `page_permissions_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
