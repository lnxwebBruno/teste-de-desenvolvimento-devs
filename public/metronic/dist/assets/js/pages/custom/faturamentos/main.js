$("#clientes_id").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/clientes/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term,
            };
        },
        processResults: function(data, params) {
            params.page = params.page || 1;

            return {
                results: data.data
            };
        },
        cache: true
    },
});

$("#vendedores_id").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/vendedores/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term,
            };
        },
        processResults: function(data, params) {
            params.page = params.page || 1;

            return {
                results: data.data
            };
        },
        cache: true
    },
});

$("#transportadoras_id").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/transportadoras/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term,
            };
        },
        processResults: function(data, params) {
            params.page = params.page || 1;

            return {
                results: data.data
            };
        },
        cache: true
    },
});

var Today= new Date();
Today.setDate(Today.getMonth() - 1);//any date you want
$('#kt_daterangepicker_3').daterangepicker({
    "showDropdowns": true,
    "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "fromLabel": "De",
        "toLabel": "Até",
        "customRangeLabel": "Custom",
        "weekLabel": "S",
        "daysOfWeek": [
            "D",
            "S",
            "T",
            "Q",
            "Q",
            "S",
            "S"
        ],
        "monthNames": [
            "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
        ],
        "firstDay": 0
    },

    "opens": "left"
}, function(start, end, label) {
    $('#kt_daterangepicker_3 .form-control').val( start.format('DD-MM-YYYY') + ' Até ' + end.format('DD-MM-YYYY'));
    $('#input_start_date').val( start.format('YYYY-MM-DD'));
    $('#input_end_date').val( end.format('YYYY-MM-DD'));
});

$('#kt_repeater_2').repeater({
    initEmpty: false,
    
    defaultValues: {
        'text-input': 'foo'
    },

    show: function () {

        $(this).slideDown();
        $(".money").maskMoney({allowNegative: true, thousands:'.', decimal:',', affixesStay: false, allowZero : true});

    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement).remove();
    }
});


function calculate_total() {
    var els = document.getElementsByClassName("ctot");
    for (var i = 0; i < els.length; i++) {
        var preco = parseFloat(document.getElementsByName("carrinho["+i+"][c_preco]")[0].value.replace('.', '').replace(',','.'));
        var qtd = parseFloat(document.getElementsByName("carrinho["+i+"][c_qtd]")[0].value);
        var desc = parseFloat(document.getElementsByName("carrinho["+i+"][c_desc]")[0].value.replace('.', '').replace(',','.'));

        preco = isNaN(preco) ? 0 : preco;
        desc = isNaN(desc) ? 0 : desc;

        var res = (preco * qtd) - desc;

        if (!isNaN(res) && qtd > 0) {
            els[i].value = res.toFixed(2);
            els[i].value = els[i].value.replace('.', ',');
        } else {
            els[i].value = 0
        }
    }
}
//mesma coisa do evento change anterior, veja os comentarios
$(".tipo_servico_select").change(function(){
           //cria o nome da class para ser "focada" quando finalizar a checagem
           var class_input = new Date().getTime() + "_input";
           //verifica se mudou para produto
           if( $(this).val() == 0){
               //captura o name do input e salva na variavel
               var name = $(this).parent().parent().children( "input.c_prod" ).attr('name');
               //remove o input
               $(this).parent().parent().children( "input.c_prod" ).remove(); 
               //cria um select no lugar do input utiliza a classe e name definidos anteriormente
               $(this).parent().parent().before().append( '<select class="form-control form-control-solid form-control-md select2 select_search_produtos '+class_input+'" name="'+name+'"  ><option label="label"></option></select>' ); 
               //Instancia o novo Select2
               $("." + class_input).select2({
                   placeholder: "Clique para buscar e selecionar",
                   allowClear: true,
                   ajax: {
                       url: "../../../../../api/produtos/simplelist",
                       dataType: 'json',
                       delay: 250,
                       data: function(params) {
                           return {
                               q: params.term, // search term
                               //produtos_id: $("#c_prod").val()
                               //page: params.page
                           };
                       },
                       processResults: function(data, params) {
                           // parse the results into the format expected by Select2
                           // since we are using custom formatting functions we do not need to
                           // alter the remote JSON data, except to indicate that infinite
                           // scrolling can be used
                           params.page = params.page || 1;
               
                           return {
                               results: data.data
       
                           };
                       },
                       cache: false
                   },
                }).on("select2:select", function (e) { 

                    var response_produto = $(this).select2('data')[0];
                    console.log(response_produto);
                    var cpreco = $(this).parent().parent().parent().find('.cpreco');
                    cpreco.val(parseFloat(response_produto['preco_venda']).toFixed(2).replace('.', ','));
            /*         $("#gv_descricao").val(response_grupo[0]['descricao']);
                    $("#gv_cfop").val(response_grupo[0]['cfop']);
                    $("#gv_icms").val(response_grupo[0]['icms']);
                    $("#gv_ipi").val(response_grupo[0]['ipi']);
                    $("#gv_pis").val(response_grupo[0]['aliq_pis']);
                    $("#gv_cofins").val(response_grupo[0]['aliq_cofins']); */
                });
               //ajusta o width
               $('#kt_repeater_1 > div div > div > div > div> div > span.select2-container').css('width','100%');
               //foca no novo input
               $('.' + class_input).focus();
       
           }else{
               //Verifica se mudou para produto
               if($(this).attr('data-previous-value') == 0){
                   //captura o name do input e salva na variavel
                   var name = $(this).parent().parent().children( "select.select_search_produtos").attr('name');
                   //cria um input no lugar do input utiliza a classe e name definidos anteriormente
                   $(this).parent().parent().before().append( '<input type="text" class="form-control form-control-solid form-control-md '+class_input+' c_prod" name="'+name+'" value="" />' ); 
                   //foca no input
                   $('.' + class_input).focus();
                   //destroi o select2
                   $(this).parent().parent().children( "select.select_search_produtos" ).select2('destroy'); 
                   $(this).parent().parent().children( "select.select_search_produtos" ).remove(); 
               }
           }
           //salva o value no atributo
           $(this).attr('data-previous-value', $(this).val());
});

$("#empresas_id").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/empresas/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term, // search term
                //page: params.page
            };
        },
        processResults: function(data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
                results: data.data
     /*            pagination: {
                    more: (params.page * 30) < data.total_count
                } */
            };
        },
        cache: true
    },
    //escapeMarkup: function(markup) {
    //    return markup;
    //}, // let our custom formatter work
    //minimumInputLength: 1,
    //templateResult: formatRepo, // omitted for brevity, see the source of this page
   // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});

$("#marketplace_id").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/marketplaces/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term, // search term
                //page: params.page
            };
        },
        processResults: function(data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
                results: data.data
     /*            pagination: {
                    more: (params.page * 30) < data.total_count
                } */
            };
        },
        cache: true
    },
    //escapeMarkup: function(markup) {
    //    return markup;
    //}, // let our custom formatter work
    //minimumInputLength: 1,
    //templateResult: formatRepo, // omitted for brevity, see the source of this page
   // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});