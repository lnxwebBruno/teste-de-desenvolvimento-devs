function deleta(clientes_id, list = false) {
  Swal.fire({
      icon: 'error',
      title: "Tem certeza? <br> Esta ação será irreversivel!",
     // html: html,
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      focusConfirm: false,
      allowEscapeKey: true,
      allowOutsideClick: true
    }).then((value) => {
         if(value['isConfirmed'] == true){
              $.ajax({
                  type:'post',
                  url:'../../../../../api/clientes/disable',
                  dataType:'json',
                  data:{
                    clientes_id:parseInt(clientes_id),
                  },
                  beforeSend: function(){
                      KTApp.blockPage({
                        opacity: 0.2,
                        overlayColor: '#3699FF',
                        state: 'danger',
                        message: 'Enviando...'
                      });
                    },
                    success:function(response) {
                      KTApp.unblockPage();
                      Swal.fire({
                          icon: response['status'],
                          title: response['status-message'],
                          //html: html,
                          showCancelButton: false,
                          showConfirmButton: true,
                          showCloseButton: true,
                          focusConfirm: true,
                          allowEscapeKey: true,
                          allowOutsideClick: true
                        }).then((value) => {
                          if(list){
                            searchQueryDt();
                          }else{
                            location.href = '../../../../../clientes';
                          }
                        })
                  },
                  error: function (request, status, error) {
                 }
              });
        }
  });
}
$( "#select_tipo_cadastro" ).change(function () {
  var type = "";
  $( "#select_tipo_cadastro option:selected" ).each(function() {
    type += $( this ).val();
  });
  switch (type) {
    case 'PF':
      $("#tipo_cadastro_pj").hide();
      $("#tipo_pessoa_fisica").show();
      $('#tipo_pessoa_juridica :input').removeAttr("required");
      $("#tipo_pessoa_fisica :input").attr("required", "required");
    break;
    case 'PJ':
      $("#tipo_pessoa_fisica").hide();
      $("#tipo_cadastro_pj").show();
      $('#tipo_pessoa_fisica :input').removeAttr("required");
      $("#tipo_pessoa_juridica :input").attr("required", "required");

    break;
  }
  console.log(type);
});

// A $( document ).ready() block.
$( document ).ready(function() {
  var type = "";
  $( "#select_tipo_cadastro option:selected" ).each(function() {
    type += $( this ).val();
  });
  switch (type) {
    case 'PF':
      $("#tipo_cadastro_pj").hide();
      $("#tipo_pessoa_fisica").show();
      $('#tipo_pessoa_juridica :input').removeAttr("required");
      $("#tipo_pessoa_fisica :input").attr("required", "required");
    break;
    case 'PJ':
      $("#tipo_pessoa_fisica").hide();
      $("#tipo_cadastro_pj").show();
      $('#tipo_pessoa_fisica :input').removeAttr("required");
      $("#tipo_pessoa_juridica :input").attr("required", "required");

    break;
  }
  console.log(type);

});