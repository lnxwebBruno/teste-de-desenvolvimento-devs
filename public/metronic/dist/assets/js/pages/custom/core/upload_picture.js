//Dropzone.autoDiscover = false;
var meuDropzone = new Dropzone("#dropzone_box_product_image", {
				
    url: "../../../../../api/core/upload/logo", // Set the url for your upload script location
    autoProcessQueue: false,
    paramName: "upload", // The name that will be used to transfer the file
    maxFiles: 1,
    maxFilesize: 30, // MB
    parallelUploads: 1,
    uploadMultiple: true,
    addRemoveLinks: true,
    acceptedFiles: "image/*",
    dictDefaultMessage: "Arraste ou Clique para selecionar a imagem.",
    dictFileTooBig: "O arquivo que você esta tentando enviar é muito grande, por favor entre em contato o administrador do sistema.",
    dictInvalidFileType: "O tipo de arquivo é inválido",
    dictCancelUpload: "Remover Arquivo",
    dictUploadCanceled: "O Arquivo foi removido",
    dictRemoveFile:"Remover Arquivo",
    timeout: 9999999999,
/*     init: function() {
        this.hiddenFileInput.setAttribute("webkitdirectory", true);
    }, */
    accept: function(file, done) {
    //	if (file.name == "justinbieber.jpg") {
    //		done("Naha, you don't.");
    //	} else {
            //Processa a fila de upload
           // console.log(file);
           // console.log(done);
		    //result = JSON.parse(file.trim());
           // console.log(result);

            //meuDropzone.processQueue();
            done();
    //	}
    }

});



meuDropzone.on("success", function(file, response) {
    /* Maybe display some more file information on your page */
   // console.log('Sucesso!');
    //response = JSON.parse(response);
    console.log(response);
    KTApp.unblockPage();

    var html = "";
    html +=  response['status-message'];
    html +=  '<a href="../../../../../empresas" class="btn btn-primary btn-lg">Listar Empresas</a>';
    confirmButtonText = 'Ok';
    Swal.fire({
        icon: response['status'],
        title: "Opa!",
        text: response['status-message'],
        html: html,
        confirmButtonText:confirmButtonText
      }).then((value) => {
            var uri = window.location.pathname;
            var uri_splited =  uri.split('/')[3];
            if(uri_splited == 'edit'){
                location.reload();
            }
    })

});

meuDropzone.on("addedfiles", function(file) {
    /* Maybe display some more file information on your page */
    if( meuDropzone.getQueuedFiles().length > 0){
	    //$('#upload_button').html('Processar Arquivos <Ok class="icon-2x flaticon2-arrow-up"></Ok>');
	    //$('#upload_button').attr('onclick','meuDropzone.processQueue()');
	    //$('#upload_button').attr('disabled', false);
    }else{
	    //$('#upload_button').attr('onclick','');
	    //$('#upload_button').attr('disabled', true);
	    //$('#upload_button').attr('onclick','finalizaCadastroFormandoColacao()');
	    //$('#upload_button').html('Realizar Upload de Arquivos Depois <i class="icon-2x flaticon2-checkmark"></i>');
    }
});


meuDropzone.on("removedfile", function(file) {
    /* Maybe display some more file information on your page */
    if( meuDropzone.getQueuedFiles().length > 0){
	    //$('#upload_button').html('Processar Arquivos <i class="icon-2x flaticon2-arrow-up"></i>');
	   // $('#upload_button').attr('onclick','meuDropzone.processQueue()');
	   // $('#upload_button').attr('disabled', false);
    }else{
	   // $('#upload_button').attr('onclick','');
	   // $('#upload_button').attr('disabled', true);
	    //$('#upload_button').attr('onclick','finalizaCadastroFormandoColacao()');
	    //$('#upload_button').html('Realizar Upload de Arquivos Depois <i class="icon-2x flaticon2-checkmark"></i>');
    }
});

meuDropzone.on('sending', function(file, xhr, formData){
    formData.append('empresas_id', $('#empresas_id').val());
    formData.append('account_id', getCookie('account_id'));
});
