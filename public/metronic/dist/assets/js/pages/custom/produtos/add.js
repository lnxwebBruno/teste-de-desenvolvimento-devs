var form = $("#add.form");
form.submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    
	$.ajax({
		url : post_url,
		type: request_method,
        data : form_data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            console.log(response);

            //Swal.fire("Opa!", response['status-message'], response['status']);
            var html = "";
            var confirmButtonText = "Ok";
            if(response['status'] == 'success'){
                html +=  response['status-message'];
                html +=  '<a href="../../../../../produtos" class="btn btn-primary btn-lg">Listar Produtos</a>';
                confirmButtonText = '<i class="fas fa-cart-plus"></i> Adicionar outro Produto';
                if( meuDropzone.getQueuedFiles().length > 0){
                    $('#produtos_id').val(response['produtos_id']); 
                    meuDropzone.processQueue();
                }else{

                    KTApp.unblockPage();
                    Swal.fire({
                        icon: response['status'],
                        title: "Opa!",
                        text: response['status-message'],
                        html: html,
                        confirmButtonText:confirmButtonText
                    }).then((value) => {
                    })
                }
                //form[0].reset();
            }else{
                KTApp.unblockPage();
                Swal.fire({
                    icon: response['status'],
                    title: "Opa!",
                    text: response['status-message'],
                    html: html,
                    confirmButtonText:confirmButtonText
                  }).then((value) => {
                })
            }
            $('#produtos_id').val('');
        }
    });
});