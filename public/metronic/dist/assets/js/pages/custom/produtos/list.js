
	var table = $('#kt_datatable');

	// begin first table
	var datatable = table.DataTable({
		"language": {
            "url": "../../../../../metronic/dist/assets/js/datatable/pt-br.json"
        },
		sDom: 'lrtip',
		"columnDefs": [
	
			{
				"targets": [5],
				"visible": false
			},
			{
				"targets": [ 0, 1, 2 ,3, 4, 6 , 7],
				"visible": true
			},
		
		],
		responsive: true,
/* 			responsive: {
			details: {
				display: $.fn.dataTable.Responsive.display.modal( {
					header: function ( row ) {
						var data = row.data();
						return 'Detalhes de '+data['nome_fantasia'];
					}
				} ),
				renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
					tableClass: 'table'
				} )
			}
		}, */
		"paging": true,
		searching: true,
		"ordering": false,
		"lengthChange": false,
		"pageLength": 15,
		processing: true,
		serverSide: true,
		ajax: {
			url: '../../../../../api/produtos/list?v=' + new Date().getTime(),
			type: 'POST',
/* 				data: {
				// parameters for custom backend script demo
				columnsDef: [
					'OrderID', 'Country',
					'ShipAddress', 'CompanyName', 'ShipDate',
					'Status', 'Type', 'Actions'],
			}, */
		},
		"columns": [
			{ "data": "produtos_id" },
			{ "data": "cod_sku" },
			{ "data": "descricao" },
			{ "data": "barcode" },
			{ "data": "empresa" },
			{ "data": "empresas_id" },
			{data: "produtos_id" , render : function ( data, type, row, meta ) {
				return type === 'display'  ?
				'<button onclick="deleta('+data+', true)" class="btn btn-sm btn-icon btn-danger"><i class="far fa-trash-alt"></i></button>' :
					data;
			}},
			{data: "produtos_id" , render : function ( data, type, row, meta ) {
				//console.log(meta);
				return type === 'display'  ?
				'<a href="../../../../../produtos/'+ data + '/edit" class="btn btn-sm btn-icon btn-primary"><i class="fas fa-edit"></i></a>':
					data;
				}},
		]
	});
	
	function searchQueryDt(){
		//datatable.ajax.reload();
		var cod_sku_q = $("#cod_sku_q").val();
		var descricao_q = $("#descricao_q").val();
		var barcode_q = $("#barcode_q").val();
		var empresas_id_q = $("#empresas_id_q").val();


		datatable.column(1).search(cod_sku_q).column(2).search(descricao_q).column(3).search(barcode_q).column(5).search(empresas_id_q).draw();
	}
	// On click bt search
	$('#search_dt').on( 'click', function () {
		searchQueryDt();
	} );
	// On enter
	$('.serch_input').on('keypress', function (e) {
		if(e.which === 13){
			searchQueryDt();
		}
  	});