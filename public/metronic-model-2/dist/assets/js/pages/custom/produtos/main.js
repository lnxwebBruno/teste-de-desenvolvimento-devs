function deleta(produtos_id, list = false) {
  Swal.fire({
      icon: 'error',
      title: "Tem certeza? <br> Esta ação será irreversivel!",
     // html: html,
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      focusConfirm: false,
      allowEscapeKey: true,
      allowOutsideClick: true
    }).then((value) => {
         if(value['isConfirmed'] == true){
              $.ajax({
                  type:'post',
                  url:'../../../../../api/produtos/disable',
                  dataType:'json',
                  data:{
                    produtos_id:parseInt(produtos_id),
                  },
                  beforeSend: function(){
                      KTApp.blockPage({
                        opacity: 0.2,
                        overlayColor: '#3699FF',
                        state: 'danger',
                        message: 'Enviando...'
                      });
                    },
                    success:function(response) {
                      KTApp.unblockPage();
                      Swal.fire({
                          icon: response['status'],
                          title: response['status-message'],
                          //html: html,
                          showCancelButton: false,
                          showConfirmButton: true,
                          showCloseButton: true,
                          focusConfirm: true,
                          allowEscapeKey: true,
                          allowOutsideClick: true
                        }).then((value) => {
                          if(list){
                            searchQueryDt();
                          }else{
                            location.href = '../../../../../produtos';
                          }
                        })
                  }
              });
        }
  });
}


/*   $( "#select_tipo_cliente" ).change(function () {
    var type = "";
    $( "#select_tipo_cliente option:selected" ).each(function() {
      type += $( this ).val();
    });
    switch (type) {
      case 'PF':
        $("#tipo_cliente_pj").hide();
        $("#tipo_pessoa_fisica").show();
      break;
      case 'PJ':
        $("#tipo_pessoa_fisica").hide();
        $("#tipo_cliente_pj").show();
      break;
    }
    console.log(type);
  }); */
        // loading remote data

  //Busca Pelas Empresas Cadastradas
  function formatRepo(repo) {
      if (repo.loading) return repo.text;
      var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
      if (repo.description) {
          markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
      }
      markup += "<div class='select2-result-repository__statistics'>" +
          "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
          "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
          "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
          "</div>" +
          "</div></div>";
      return markup;
  }

  function formatRepoSelection(repo) {
      return repo.full_name || repo.text;
  }
  $(".select_search").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/empresas/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term, // search term
                //page: params.page
            };
        },
        processResults: function(data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
                results: data.data
     /*            pagination: {
                    more: (params.page * 30) < data.total_count
                } */
            };
        },
        cache: true
    },
    //escapeMarkup: function(markup) {
    //    return markup;
    //}, // let our custom formatter work
    //minimumInputLength: 1,
    //templateResult: formatRepo, // omitted for brevity, see the source of this page
   // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});