function deleta(empresas_id, list = false) {
    Swal.fire({
        icon: 'error',
        title: "Tem certeza? <br> Esta ação será irreversivel!",
       // html: html,
        showCancelButton: true,
        showConfirmButton: true,
        showCloseButton: true,
        focusConfirm: false,
        allowEscapeKey: true,
        allowOutsideClick: true
      }).then((value) => {
           if(value['isConfirmed'] == true){
                $.ajax({
                    type:'post',
                    url:'../../../../../api/empresas/disable',
                    dataType:'json',
                    data:{
                      empresas_id:parseInt(empresas_id),
                    },
                    beforeSend: function(){
                        KTApp.blockPage({
                          opacity: 0.2,
                          overlayColor: '#3699FF',
                          state: 'danger',
                          message: 'Enviando...'
                        });
                      },
                      success:function(response) {
                        KTApp.unblockPage();
                        Swal.fire({
                            icon: response['status'],
                            title: response['status-message'],
                            //html: html,
                            showCancelButton: false,
                            showConfirmButton: true,
                            showCloseButton: true,
                            focusConfirm: true,
                            allowEscapeKey: true,
                            allowOutsideClick: true
                          }).then((value) => {
                            if(list){
                              searchQueryDt();
                            }else{
                              location.href = '../../../../../empresas';
                            }
                          })
                    }
                });
          }
    });
}


$("#marketplace_id").select2({
  placeholder: "Clique para buscar e selecionar",
  allowClear: true,
  ajax: {
      url: "../../../../../api/marketplaces/simplelist",
      dataType: 'json',
      delay: 250,
      data: function(params) {
          return {
              q: params.term, // search term
              //page: params.page
          };
      },
      processResults: function(data, params) {
          // parse the results into the format expected by Select2
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data, except to indicate that infinite
          // scrolling can be used
          params.page = params.page || 1;

          return {
              results: data.data
   /*            pagination: {
                  more: (params.page * 30) < data.total_count
              } */
          };
      },
      cache: true
  },
  //escapeMarkup: function(markup) {
  //    return markup;
  //}, // let our custom formatter work
  //minimumInputLength: 1,
  //templateResult: formatRepo, // omitted for brevity, see the source of this page
 // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});