$( ".add-mkt" ).click(function() {
    //$('input').val('');
    var marketplace_id = $(this).attr('data-marketplace-id');
    var marketplace_logopath = $(this).attr('data-marketplace-logopath');
    var marketplace_api = $(this).attr('data-marketplace-apikey');
    $('#marketplace_form_id').val(marketplace_id);
    if (marketplace_api == "1") {
        $('#display_api').show();
    } else {
        $('#display_api').hide();
    }
    $('#marketplace_modal').modal('show');
    $('#modal_logo').attr("src", marketplace_logopath);
});

var form = $(".marketplace_modal_form");
form.submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    
	$.ajax({
		url : post_url,
		type: request_method,
        data : form_data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            KTApp.unblockPage();
            console.log(response);

            //Swal.fire("Opa!", response['status-message'], response['status']);
            var html = "";
            var confirmButtonText = "Ok";
            if(response['status'] == 'success'){
                html += "<p>"+ response['status-message'] +"</p>"
                form[0].reset();
                var logo = $('#modal_logo').attr('src');
                add_loja(response['id_loja'], form_data, logo);
            }
            Swal.fire({
                icon: response['status'],
                title: "Sucesso!",
                text: response['status-message'],
                html: html,
                confirmButtonText:confirmButtonText
              }).then((value) => {
            })
        }
    });
});

function add_loja(id, form_data, logo) {
    tdata = String(form_data).split('&');
    data = {};
    for (var i = 0; i < tdata.length; i++) {
        temp = tdata[i].split("=");
        data[temp[0]] = temp[1];
    }
    var zona = $("#novas_lojas");
    var new_form = 
    '<form action="../../../../../api/' + controller + '/' + id + '/update_marketplace" method="POST" class="marketplace_modal_form">' +
    '<input type="hidden" name="empresas_id" id="empresas_id" value="' + data['empresas_id'] + '">' +
    '<div class="row">' + 
        '<div class="form-group col-xl-5">' +
            '<label>Nome da Loja</label>' +
            '<input required type="text" class="form-control form-control-lg form-control-solid" name="nome_loja" value="' + data['nome_loja'] + '">' +
        '</div>' +
        '<div class="col-xl-2"></div>' +
        '<div class="col-xl-3 my-auto">' +
            '<img src="../../../../../' + logo + '" style="width: 100%;height: auto;">' +
        '</div>' +
    '</div>' +
    '<div class="row">' +
        '<div class="form-group col-xl-5">' +
            '<label>E-mail</label>' +
            '<input required type="text" class="form-control form-control-lg form-control-solid" name="email_loja" value="' + data['email_loja'] + '">' +
        '</div>' + 
    '</div>' +
    '<div class="row">' +
        '<div class="form-group col-xl-5">' +
            '<label>Senha</label>' +
            '<input required type="password" class="form-control form-control-lg form-control-solid" name="senha_loja" value="' + data['senha_loja'] + '">' +
        '</div>' + 
    '</div>';
    if (data['api_key'] == "") {
        new_form += 
        '<div class="row">' +
            '<div class="form-group col-xl-5">' +
                '<label>URL da Loja</label>' +
                '<input required type="text" class="form-control form-control-lg form-control-solid" name="url_loja" value="' + data['url_loja'] + '">' +
            '</div>' +
            '<div class="col-xl-2"></div>' +
            '<div class="form-group col-xl-3 my-auto" style="text-align: center;">' +
                '<br>' +
                '<input type="submit" class="btn btn-primary" value="Atualizar">' +
            '</div>' +
        '</div>';
    } else {
    new_form += 
    '<div class="row">' +
        '<div class="form-group col-xl-5">' +
            '<label>URL da Loja</label>' +
            '<input required type="text" class="form-control form-control-lg form-control-solid" name="url_loja" value="' + data['url_loja'] + '">' +
        '</div>' +
    '</div>' +
    '<div class="row">' +
        '<div class="form-group col-xl-5">' +
            '<label>Chave de API</label>' +
            '<input type="text" class="form-control form-control-lg form-control-solid" name="api_key" value="' + data['api_key'] + '">' +
        '</div>' +
        '<div class="col-xl-2"></div>' +

        '<div class="form-group col-xl-3 my-auto" style="text-align: center;">' +
            '<br>' +
            '<input type="submit" class="btn btn-primary" value="Atualizar">' +
        '</div>' +
    '</div>';
    }
    new_form += '</form>';
    zona.append(new_form);
    var form = $(".marketplace_modal_form");
    form.submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission
        
        $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            dataType: "json",
            beforeSend: function( xhr ) {
                //Desativa o Form
                KTApp.blockPage({
                    opacity: 0.2,
                    overlayColor: '#3699FF',
                    state: 'danger',
                    message: 'Enviando...'
                });
            },
            success:function(response) {
                KTApp.unblockPage();
                console.log(response);

                //Swal.fire("Opa!", response['status-message'], response['status']);
                var html = "";
                var confirmButtonText = "Ok";
                if(response['status'] == 'success'){
                    html += "<p>"+ response['status-message'] +"</p>"
                    form[0].reset();
                    var logo = $('#modal_logo').attr('src');
                    add_loja(response['id_loja'], form_data, logo);
                }
                Swal.fire({
                    icon: response['status'],
                    title: "Sucesso!",
                    text: response['status-message'],
                    html: html,
                    confirmButtonText:confirmButtonText
                }).then((value) => {
                })
            }
        });
    });
}