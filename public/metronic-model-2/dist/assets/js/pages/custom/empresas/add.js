//Removendo campos não utilizados do cadastro genérico
$("#nome_responsavel").parent().parent().parent().remove()

function cepSearchMultiField(fid, f_suffix){ 
    var cep = $(fid).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "" && cep.length == 8)  {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            $("#endereco"+f_suffix).val("...");
            $("#endereco_bairro"+f_suffix).val("...");
            $("#cidade"+f_suffix).val("...");
            $("#estado"+f_suffix).val("...");
            //$("#ibge").val("...");

            //Consulta o webservice viacep.com.br/
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#endereco"+f_suffix).val(dados.logradouro);
                    $("#endereco_bairro"+f_suffix).val(dados.bairro);
                    $("#cidade"+f_suffix).val(dados.localidade);
                    $("#uf"+f_suffix).val(dados.uf);
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    //limpa_formulário_cep();
                    $.notify({
                        // options
                        message: 'CEP não encontrado.',
                    },{
                        // settings
                        type: 'danger'
                    });
                    //alert("CEP não encontrado.");
                }
            });
        } //end if.
        else {
            //cep é inválido.
            //limpa_formulário_cep();
            //alert("Formato de CEP inválido.");
            $.notify({
                // options
                message: 'Formato de CEP inválido.',
            },{
                // settings
                type: 'danger'
            });
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        //limpa_formulário_cep();
    }
}

var form = $("#add.form");
form.submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    
	$.ajax({
		url : post_url,
		type: request_method,
        data : form_data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            KTApp.unblockPage();
            console.log(response);

            //Swal.fire("Opa!", response['status-message'], response['status']);
            var html = "";
            var confirmButtonText = "Ok";
            if(response['status'] == 'success'){
                html +=  '<a href="../../../../../empresas" class="btn btn-primary btn-lg">Listar Empresas</a>';
                confirmButtonText = '<i class="menu-icon far fa-building"></i> Adicionar outra Empresa';
                form[0].reset();
            }
            Swal.fire({
                icon: response['status'],
                title: "Opa!",
                text: response['status-message'],
                html: html,
                confirmButtonText:confirmButtonText
              }).then((value) => {
            })
        }
    });
});