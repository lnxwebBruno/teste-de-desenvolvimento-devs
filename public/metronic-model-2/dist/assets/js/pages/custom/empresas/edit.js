$("#nome_responsavel").parent().parent().parent().remove()

$("#edit.form").submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serializeArray(); //Encode form elements for submission
    var data = {};

    if( $("#service_enabled").is(":checked") == true ){
        data['service_enabled'] = 1;
    }else{
        data['service_enabled'] = 0;
    }

/*     if( $("#meli_integracao").is(":checked") == true ){
      data['meli_integracao'] = 1;
    }else{
        data['meli_integracao'] = 0;
    } */
    $(form_data).each(function(index, obj){
        data[obj.name] = obj.value;
    });
    console.log(data);
	$.ajax({
		url : post_url,
		type: request_method,
        data : data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            KTApp.unblockPage();
            console.log(response);

            Swal.fire("Opa!", response['status-message'], response['status']);
        }
    }).done(function(response){ //
        //Debug
        //console.log(response);
	});
});
//Gerar Link de Integração Meli
/*  function gerarMeliLink(empresas_id = false){
    if(empresas_id){
      $.ajax({
        url : '../../../../../auth/meli/generate/' + empresas_id,
        type: 'get',
            dataType: "json",
            beforeSend: function( xhr ) {
                //Desativa o Form
                KTApp.blockPage({
                    opacity: 0.2,
                    overlayColor: '#3699FF',
                    state: 'danger',
                    message: 'Enviando...'
                  });
            },
            success:function(response) {
                KTApp.unblockPage();
                console.log(response);
    
                Swal.fire("Opa!", response['status-message'], response['status']);
                if(response['url_meli_integracao']){
                  $('#url_meli_integracao').html(response['url_meli_integracao']);
                  $('#url_meli_integracao').attr('href', response['url_meli_integracao']);
                }
            }
        }).done(function(response){ //
            //Debug
            //console.log(response);
      });
  }
 }
 */


//Tabelas de Lojas
	var table = $('#kt_datatable');
  var empresas_id = table.attr('data-empresas-id');
	// begin first table
	var datatable = table.DataTable({
		"language": {
            "url": "../../../../../metronic/dist/assets/js/datatable/pt-br.json"
        },
 
		//responsive: true,
			responsive: {
			details: {
				display: $.fn.dataTable.Responsive.display.modal( {
					header: function ( row ) {
						var data = row.data();
						return 'Detalhes de '+data['nome_fantasia'];
					}
				} ),
				renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
					tableClass: 'table'
				} )
			}
		}, 
		sDom: 'lrtip',
		searching: true,
		"lengthChange": false,
		"pageLength": 15,
    "ordering": false,
    fixedHeader: true,
    "scrollX": true,
    paging:         false,
		processing: true,
		serverSide: true,
		ajax: {
			url: '../../../../..//api/erp/lojas/bling/' + empresas_id + '?v=' + new Date().getTime(),
			type: 'POST',
 				data: {
				// parameters for custom backend script demo
				columnsDef: [
					'empresas_id', empresas_id
                ]}, 
		},
		"columns": [
			{ "data": "loja_id" },
			{ "data": "nome_marketplace" },
			{ "data": "numero_bling" },
			{ "data": "situacao_importar_text" },
 
			{data: "loja_id" , render : function ( data, type, row, meta ) {
				return type === 'display'  ?
				'<button type="button"  onclick="deletaLojaBling('+data+', true)" class="btn btn-sm btn-icon btn-danger"><i class="far fa-trash-alt"></i></button>' :
					data;
			}},
			{data: "loja_id" , render : function ( data, type, row, meta ) {
				console.log(meta);
				return type === 'display'  ?
				'<button type="button"  onclick="editLoja('+data+', true)" class="btn btn-sm btn-icon btn-primary"><i class="fas fa-edit"></i></button>' :
        data;
				}},
		]
	});
 
function editLoja(loja_id){
  $.ajax({
    type:'get',
    url:'../../../../../api/erp/bling/loja/' + loja_id,
    dataType:'json',
    beforeSend: function(){
  /*         KTApp.blockPage({
          opacity: 0.2,
          overlayColor: '#3699FF',
          state: 'danger',
          message: 'Carregando...'
        }); */
      },
      success:function(response) {
        console.log(response);
        if(response){
          //Carrega as configurações e infos no formulário
          $('#form-loja').attr('action', "../../../../../api/erp/bling/update/loja/" + loja_id);
          $("#numero_bling").val(response['numero_bling']);
          $("#marketplace_id").val(response['marketplace_id']);
          $("#situacao_importar").val(response['situacao_importar']);
          $("#situacao_apos_importar").val(response['situacao_apos_importar']);
          $("#situacao_atualizar_processo").val(response['situacao_atualizar_processo']);
          $("#situacao_verificado").val(response['situacao_verificado']);
          $("#situacao_apos_finalizar").val(response['situacao_apos_finalizar']);
          $("#situacao_cancelar").val(response['situacao_cancelar']);
          $("#importar").val(response['importar']);
          //Abre Modal
          $('#modal-loja').modal('show');
        }
    }
  });
}


function criarLoja(empresas_id){
 
          //Reset nas configurações e infos no formulário
          $('#form-loja').attr('action', "../../../../../api/erp/bling/add/loja/" + empresas_id);
          $("#numero_bling").val('');
          $("#marketplace_id").empty();
          $("#situacao_importar").val('9');
/*           $("#situacao_apos_importar").val('');
          $("#situacao_atualizar_processo").val('');
          $("#situacao_verificado").val('');
          $("#situacao_apos_finalizar").val('');
          $("#situacao_cancelar").val(''); */
          $("#importar").val('1');
          //Abre Modal
          $('#modal-loja').modal('show');
}

$('#modal-loja').on('shown.bs.modal', function () {

  $("#marketplace_id").select2({
    placeholder: "Clique para buscar e selecionar",
    allowClear: true,
    ajax: {
        url: "../../../../../api/marketplaces/simplelist",
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term, // search term
                //page: params.page
            };
        },
        processResults: function(data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;
  
            return {
                results: data.data
     /*            pagination: {
                    more: (params.page * 30) < data.total_count
                } */
            };
        },
        cache: true
    },
    //escapeMarkup: function(markup) {
    //    return markup;
    //}, // let our custom formatter work
    //minimumInputLength: 1,
    //templateResult: formatRepo, // omitted for brevity, see the source of this page
   // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
  });
})

function deletaLojaBling(loja_id) {
    Swal.fire({
        icon: 'error',
        title: "Tem certeza? <br> Esta ação será irreversivel!",
        // html: html,
        showCancelButton: true,
        showConfirmButton: true,
        showCloseButton: true,
        focusConfirm: false,
        allowEscapeKey: true,
        allowOutsideClick: true
      }).then((value) => {
            if(value['isConfirmed'] == true){
                 $.ajax({
                    type:'post',
                    url:'../../../../../api/erp/bling/disable/loja',
                    dataType:'json',
                    data:{
                      loja_id:parseInt(loja_id),
                    },
                    beforeSend: function(){
                        KTApp.blockPage({
                          opacity: 0.2,
                          overlayColor: '#3699FF',
                          state: 'danger',
                          message: 'Enviando...'
                        });
                      },
                      success:function(response) {
                        KTApp.unblockPage();
                        Swal.fire({
                            icon: response['status'],
                            title: response['status-message'],
                            //html: html,
                            showCancelButton: false,
                            showConfirmButton: true,
                            showCloseButton: true,
                            focusConfirm: true,
                            allowEscapeKey: true,
                            allowOutsideClick: true
                          }).then((value) => {
                              datatable.draw();
                          })
                    }
                });
          }
    });
}


$("#form-loja").submit(function(event){
	event.preventDefault(); //prevent default action 
  if(!validateSituacoesID()){
 
    $.notify({
      // options
      message: 'Existem situações repetidas no formulário',
      title: "Ooops!",
    },{
      // settings
      type: 'danger',
      placement: {
        from: 'bottom',
        align: 'right'
      },
      z_index: 99999999,
    });

    return false;
  }

	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
  var form_data = $(this).serializeArray(); //Encode form elements for submission
  var data = {};

    $(form_data).each(function(index, obj){
        data[obj.name] = obj.value;
    });
    console.log(data);
	$.ajax({
		url : post_url,
		type: request_method,
        data : data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            KTApp.unblockPage();
            console.log(response);

            if(response['status'] == 'success'){
		          datatable.draw();
              $('#modal-loja').modal('hide');
            }

            Swal.fire("Opa!", response['status-message'], response['status']);
        }
    }).done(function(response){ //
        //Debug
        //console.log(response);
	});
});

 
//Validação para não permitir valores repetidos
function validateSituacoesID(){

  situacao_id = [];
  $.each($('select.situacao'),function(){
   // console.log($(this).val());
    var i = 0;
    situacao_id.push($(this).val());
    $('select.situacao option[value=' + $(this).val() + ']:selected').parent().removeClass( "border border-danger border-2 animate__animated animate__tada" );

    i++;
  });

  console.log(situacao_id);
  var uniq = situacao_id
  .map((name) => {
    return {
      count: 1,
      name: name
    }
  })
  .reduce((a, b) => {
    a[b.name] = (a[b.name] || 0) + b.count
    return a
  }, {})

  var duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1)

  console.log(duplicates) // [ 'situacao_id' ]
  if(duplicates.length > 0){
    
    for (let index = 0; index < duplicates.length; index++) {
      setTimeout(() => { $('select.situacao option[value=' + duplicates[index] + ']:selected').parent().addClass( "border border-danger border-2 animate__animated animate__tada" );}, 200);
    }
    return false;
  }

  return true;

 }

function deleta_loja(loja_id, elemen) {
  Swal.fire({
      icon: 'error',
      title: "Tem certeza? <br> Esta ação será irreversivel!",
      // html: html,
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      focusConfirm: false,
      allowEscapeKey: true,
      allowOutsideClick: true
    }).then((value) => {
          if(value['isConfirmed'] == true){
               $.ajax({
                  type:'post',
                  url:'../../../../../api/empresas/disable_marketplace',
                  dataType:'json',
                  data:{
                    loja_id:parseInt(loja_id),
                  },
                  beforeSend: function(){
                      KTApp.blockPage({
                        opacity: 0.2,
                        overlayColor: '#3699FF',
                        state: 'danger',
                        message: 'Enviando...'
                      });
                    },
                    success:function(response) {
                      KTApp.unblockPage();
                      Swal.fire({
                          icon: response['status'],
                          title: response['status-message'],
                          //html: html,
                          showCancelButton: false,
                          showConfirmButton: true,
                          showCloseButton: true,
                          focusConfirm: true,
                          allowEscapeKey: true,
                          allowOutsideClick: true
                        }).then((value) => {
                            datatable.draw();
                        })
                  }
              });
        }
  });
  $(elemen).parent().parent().parent().remove();
}

function buscar_marketplaces(elemen) {
  results = [];
  $("#cardzone").empty();
  $.ajax({
		url : "../../../../../api/marketplaces/simplelist",
		type: "get",
    data : "q=" + $(elemen).val(),
    dataType: "json",
    success:function(response) {
        //r = JSON.parse(response['data']);
        r = response['data'];
        var to_append="";
        for (var i = 0; i < r.length; i++) {
          if (i == 0 || i%4 == 0) {
            to_append += "<div class='row'>";
          }
          to_append += "<div class='col-xl-3'>" + '<div class="card card-custom card-stretch card-border" id="' + r[i]['marketplace_id'] + '">' +
          '<div class="card-body"><div class="row"><div class="col-xl-6 my-auto">';
          if (r[i]['logo_path'] == "") {
            to_append += r[i][nome];
          } else {
            to_append += '<img src="../../../../../' + r[i]['logo_path'] + '" style="width: 100%;height: auto;">';
          }
          to_append += '</div><div class="col-xl-6">' + 
          '<button data-marketplace-id="' + r[i]['marketplace_id'] + '" data-marketplace-apikey="'+ r[i]['allow_api_key'] + '" data-marketplace-logopath="../../../../../' + r[i]['logo_path'] + '" class="add-mkt btn btn-icon btn-outline-success"><i class="fa fa-plus" aria-hidden="true"></i></button>' + 
          '</div></div></div></div></div>';

          if (((i-3) % 4 == 0 && i > 0) || i == r.length - 1) {
            to_append += "</div>";
          }
        }
        $("#cardzone").append(to_append);
    }
    }).done(function(response){ //
        //Debug
	});
}