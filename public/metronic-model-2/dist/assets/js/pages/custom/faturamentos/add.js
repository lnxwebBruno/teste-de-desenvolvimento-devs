var form = document.getElementById("add");
$("#add").submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    var data = {};

    if( $("#cons_final").is(":checked") == true ){
        data['cons_final'] = 1;
    }else{
        data['cons_final'] = 0;
    }
    var form_data = $(this).serialize() + "&cons_final=" +  data['cons_final']; //Encode form elements for submission

    /* $(form_data).each(function(index, obj){
        data[obj.name] = obj.value;
    }); */
    //return;
	$.ajax({
		url : post_url,
		type: request_method,
        data : form_data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            KTApp.unblockPage();
            console.log(response);

            //Swal.fire("Opa!", response['status-message'], response['status']);
            var html = "";
            var confirmButtonText = "Ok";
            if(response['status'] == 'success'){
                html +=  response['status-message'];
                html +=  '<a href="../../../../../faturamentos" class="btn btn-primary btn-lg">Listar Faturamentos</a>';
                confirmButtonText = '<i class="fas fa-user-friends"></i> Adicionar outra operação';
                html += '<a href="../../../../../nfe/novo" class="btn btn-primary btn-lg">Emitir nota fiscal</a>'
                form.reset();
            }
            Swal.fire({
                icon: response['status'],
                title: "Opa!",
                text: response['status-message'],
                html: html,
                confirmButtonText:confirmButtonText
              }).then((value) => {
                  //salvarVenda();
              })
        }
    });
});   

$( document ).ready(function() {
    var $select_search_produtos = $(".select_search_produtos");

    $select_search_produtos.select2({
        placeholder: "Clique para buscar e selecionar",
        allowClear: true,

        ajax: {
            url: "../../../../../api/produtos/simplelist",
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    q: params.term, // search term
                // produtos_id: $("#c_prod").val()
                    //page: params.page
                };
            },
            processResults: function(data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;
                //console.log(data);
                return {
                    results: data.data

                };
            },
            cache: true
        },

    }).on("select2:select", function (e) { 
        var response_produto = $(this).select2('data')[0];
        console.log(response_produto);
        var cpreco = $(this).parent().parent().parent().find('.cpreco');
        cpreco.val(parseFloat(response_produto['preco_venda']).toFixed(2).replace('.', ','));
    });
});


$('#kt_repeater_1').repeater({
    initEmpty: false,
    isFirstItemUndeletable: true,
    show: function () {

        $(this).slideDown();
        //$('#kt_repeater_1 > div  > div > div > div> div > span.select2-container').remove();
        


        
        $(".select_search_produtos").select2({
            placeholder: "Clique para buscar e selecionar",
            allowClear: true,
            ajax: {
                url: "../../../../../api/produtos/simplelist",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        //produtos_id: $("#c_prod").val()
                        //page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
        
                    return {
                        results: data.data

                    };
                },
                cache: false
            },
        }).on("select2:select", function (e) { 

            var response_produto = $(this).select2('data')[0];
            console.log(response_produto);
            var cpreco = $(this).parent().parent().parent().find('.cpreco');
            cpreco.val(parseFloat(response_produto['preco_venda']).toFixed(2).replace('.', ','));
    /*         $("#gv_descricao").val(response_grupo[0]['descricao']);
            $("#gv_cfop").val(response_grupo[0]['cfop']);
            $("#gv_icms").val(response_grupo[0]['icms']);
            $("#gv_ipi").val(response_grupo[0]['ipi']);
            $("#gv_pis").val(response_grupo[0]['aliq_pis']);
            $("#gv_cofins").val(response_grupo[0]['aliq_cofins']); */
        });

        $('#kt_repeater_1 > div div > div > div > div> div > span.select2-container').css('width','100%');

        $(".money").maskMoney({allowNegative: true, thousands:'.', decimal:',', affixesStay: false, allowZero : true});
        console.log('add');

        //Evento para mudança de tipo_servico_select
        $(".tipo_servico_select").off('change').on('change', (function(){
            //cria o nome da class para ser "focada" quando finalizar a checagem
            var class_input = new Date().getTime() + "_input";

            //verifica se mudou para produto
            if( $(this).val() == 0){
                //captura o name do input e salva na variavel
                var name = $(this).parent().parent().children( "input.c_prod" ).attr('name');
                //remove o input
                $(this).parent().parent().children( "input.c_prod" ).remove(); 
                //cria um select no lugar do input utiliza a classe e name definidos anteriormente
                $(this).parent().parent().before().append( '<select class="form-control form-control-solid form-control-md select2 select_search_produtos '+class_input+'" name="'+name+'"  ><option label="label"></option></select>' ); 
                //Instancia o novo Select2
                $("." + class_input).select2({
                    placeholder: "Clique para buscar e selecionar",
                    allowClear: true,
                    ajax: {
                        url: "../../../../../api/produtos/simplelist",
                        dataType: 'json',
                        delay: 250,
                        data: function(params) {
                            return {
                                q: params.term, // search term
                                //produtos_id: $("#c_prod").val()
                                //page: params.page
                            };
                        },
                        processResults: function(data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;
                
                            return {
                                results: data.data
        
                            };
                        },
                        cache: false
                    },
                }).on("select2:select", function (e) { 
                    var response_produto = $(this).select2('data')[0];
                    console.log(response_produto);
                    var cpreco = $(this).parent().parent().parent().find('.cpreco');
                    cpreco.val(parseFloat(response_produto['preco_venda']).toFixed(2).replace('.', ','));
                });
                //ajusta o width
                $('#kt_repeater_1 > div div > div > div > div> div > span.select2-container').css('width','100%');
                //foca no novo input
                $('.' + class_input).focus();
        
            }else{
                //Verifica se mudou para produto
                if($(this).attr('data-previous-value') == 0){
                    //captura o name do input e salva na variavel
                    var name = $(this).parent().parent().children( "select.select_search_produtos").attr('name');
                    //cria um input no lugar do input utiliza a classe e name definidos anteriormente
                    $(this).parent().parent().before().append( '<input type="text" class="form-control form-control-solid form-control-md '+class_input+' c_prod" name="'+name+'" value="" />' ); 
                    //foca no input
                    $('.' + class_input).focus();
                    //destroi o select2
                    $(this).parent().parent().children( "select.select_search_produtos" ).select2('destroy'); 
                    $(this).parent().parent().children( "select.select_search_produtos" ).remove(); 
                }
            }
            //salva o value no atributo
            $(this).attr('data-previous-value', $(this).val());
        }));


    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    }
});