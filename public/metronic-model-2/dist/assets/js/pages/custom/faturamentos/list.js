var table = $('#kt_datatable');

	// begin first table
	var datatable = table.DataTable({
		"language": {
            "url": "../../../../../metronic/dist/assets/js/datatable/pt-br.json"
        },
		sDom: 'lrtip',
		"columnDefs": [
	
			{
				"targets": [0, 8],
				"visible": false
			},
			{
				"targets": [],//[ 0, 1, 2 ,3, 8, 10, 11],
				"visible": true
			},
		
		],
		responsive: true,
/* 			responsive: {
			details: {
				display: $.fn.dataTable.Responsive.display.modal( {
					header: function ( row ) {
						var data = row.data();
						return 'Detalhes de '+data['nome_fantasia'];
					}
				} ),
				renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
					tableClass: 'table'
				} )
			}
		}, */
		"paging": true,
		searching: true,
		"ordering": false,
		"lengthChange": false,
		"pageLength": 15,
		processing: true,
		serverSide: true,
		ajax: {
			url: '../../../../../api/faturamentos/list?v=' + new Date().getTime(),
			type: 'POST',
/* 				data: {
				// parameters for custom backend script demo
				columnsDef: [
					'OrderID', 'Country',
					'ShipAddress', 'CompanyName', 'ShipDate',
					'Status', 'Type', 'Actions'],
			}, */
		},
		"columns": [
			{ "data": "faturamentos_id" }, // 0 aqui você vai precisar renderizar um check_box para seleção, vc pode ocultar  a coluna e deixar para depois
			{ "data": "faturamentos_id" }, // 1
			{ "data": "nome_cliente" }, // 2
			{ "data": "empresa_nome_fantasia" }, //3
			{ "data": "nome_martkeplace" }, //4
			{ "data": "data_cadastro" }, //4
			{data: "total", render : function ( data, type, row, meta) {
				return type === 'display' ?
				"R$ " + data.toFixed(2) : data;
			}}, //6 // Aqui você muda para o valor total
			{data: "faturamentos_id" , render : function ( data, type, row, meta ) {
				console.log(meta);
				return type === 'display'  ?
				'<a href="../../../../../faturamentos/'+ data + '/edit" class="btn btn-sm btn-icon btn-primary"><i class="fas fa-edit"></i></a>':
					data;
			}}, //7
			{ "data": "data_cadastro"} //8
		]
	});
	
	function searchQueryDt(){
		var nome_cliente_q = $("#nome_cliente_q").val();
		var empresas_id = $("#empresas_id").val();
		var marketplace_id = $("#marketplace_id").val();
		var data_inicio = $("#input_start_date").val();
		var data_fim = $("#input_end_date").val();
		datatable.column(2).search(nome_cliente_q)
		.column(3).search(marketplace_id)
		.column(4).search(empresas_id)
		.column(7).search(data_inicio)
		.column(8).search(data_fim).draw();
	}
	// On click bt search
	$('#search_dt').on( 'click', function () {
		searchQueryDt();
	} );
	// On enter
	$('.search_input').on('keypress', function (e) {
		if(e.which === 13){
			searchQueryDt();
		}
  	});