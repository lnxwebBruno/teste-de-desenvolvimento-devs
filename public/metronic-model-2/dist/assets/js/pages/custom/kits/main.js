function deleta(kits_id, list = false) {
    Swal.fire({
        icon: 'error',
        title: "Tem certeza? <br> Esta ação será irreversivel!",
       // html: html,
        showCancelButton: true,
        showConfirmButton: true,
        showCloseButton: true,
        focusConfirm: false,
        allowEscapeKey: true,
        allowOutsideClick: true
      }).then((value) => {
           if(value['isConfirmed'] == true){
                $.ajax({
                    type:'post',
                    url:'../../../../../api/kits/delete',
                    dataType:'json',
                    data:{
                        kits_id:parseInt(kits_id),
                    },
                    beforeSend: function(){
                        KTApp.blockPage({
                          opacity: 0.2,
                          overlayColor: '#3699FF',
                          state: 'danger',
                          message: 'Enviando...'
                        });
                      },
                      success:function(response) {
                        KTApp.unblockPage();
                        Swal.fire({
                            icon: response['status'],
                            title: response['status-message'],
                            //html: html,
                            showCancelButton: false,
                            showConfirmButton: true,
                            showCloseButton: true,
                            focusConfirm: true,
                            allowEscapeKey: true,
                            allowOutsideClick: true
                          }).then((value) => {
                            if(list){
                              searchQueryDt();
                            }else{
                              location.href = '../../../../../kits';
                            }
                          })
                    }
                });
          }
    });
  }
  
      //Desabilita
      function enableKit(kits_id, list = false){
    
        if ($('input#enable_kit_id_' + kits_id).is(':checked')) {

            //$('#bt_text_auth').html('Autorizar'); 
            var enabled = 1;
        }else{
            var enabled = 0;
        }

        Swal.fire({
            icon: 'error',
            title: "Tem certeza?",
            // html: html,
            showCancelButton: true,
            showConfirmButton: true,
            showCloseButton: true,
            focusConfirm: false,
            allowEscapeKey: true,
            allowOutsideClick: true
            }).then((value) => {
                if(value['isConfirmed'] == true){
                    $.ajax({
                        type:'post',
                        url:'../../../../../api/kits/disable',
                        dataType:'json',
                        data:{
                            kits_id:parseInt(kits_id),
                            enabled:enabled,
                        },
                        beforeSend: function(){
                            KTApp.blockPage({
                                opacity: 0.2,
                                overlayColor: '#3699FF',
                                state: 'danger',
                                message: 'Enviando...'
                            });
                            },
                            success:function(response) {
                                console.log(response);
                            KTApp.unblockPage();
                            Swal.fire({
                                icon: response['status'],
                                title: response['status-message'],
                                //html: html,
                                showCancelButton: false,
                                showConfirmButton: true,
                                showCloseButton: true,
                                focusConfirm: true,
                                allowEscapeKey: true,
                                allowOutsideClick: true
                                }).then((value) => {
                                if(list){
                                    //searchQueryDt();
                                }else{
                                    //location.href = '../../../../../kits';
                                }
                                })
                        }
                    });
                }
        });
    }
    
    //loading remote data
    //Select Remote Search Empresas 
    
    $(".select_search_empresas").select2({
        placeholder: "Clique para buscar e selecionar",
        allowClear: true,
        ajax: {
            url: "../../../../../api/empresas/simplelist?v="+ new Date().getTime(),
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    q: params.term, // search term
                    //page: params.page
                };
            },
            processResults: function(data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;
    
                return {
                    results: data.data

                };
            },
            cache: true
        },

    });

    $( document ).ready(function() {
        //Select Remote Search Empresas 
        $(".select_search_produtos").select2({
            placeholder: "Clique para buscar e selecionar",
            allowClear: true,
            ajax: {
                url: "../../../../../api/produtos/simplelist?v="+ new Date().getTime(),
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        empresas_id: $("#empresas_id").val()
                //page: params.page
                    };
                },
                beforeSend : function()    {           
                    if($("#empresas_id").val() == "") {
                        toastr.error("Selecione um empresa antes de selecionar os produtos");
                        //$('#empresas_id').select2('open');
                        return false;
                    }
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
        
                    return {
                        results: data.data

                    };
                },
                cache: true
            },

        });
    });

