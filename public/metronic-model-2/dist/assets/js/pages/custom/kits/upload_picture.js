//Dropzone.autoDiscover = false;
var meuDropzone = new Dropzone("#dropzone_box_product_image", {
				
    url: "../../../../../action/importCSV.php", // Set the url for your upload script location
    autoProcessQueue: false,
    paramName: "upload", // The name that will be used to transfer the file
    maxFiles: 1,
    maxFilesize: 30, // MB
    parallelUploads: 1,
    uploadMultiple: true,
    addRemoveLinks: true,
    acceptedFiles: "image/*",
    dictDefaultMessage: "Arraste ou Clique para selecionar a imagem.",
    dictFileTooBig: "O arquivo que você esta tentando enviar é muito grande, por favor entre em contato o administrador do sistema.",
    dictInvalidFileType: "O tipo de arquivo é inválido",
    dictCancelUpload: "Remover Arquivo",
    dictUploadCanceled: "O Arquivo foi removido",
    dictRemoveFile:"Remover Arquivo",
    timeout: 9999999999,
/*     init: function() {
        this.hiddenFileInput.setAttribute("webkitdirectory", true);
    }, */
    accept: function(file, done) {
    //	if (file.name == "justinbieber.jpg") {
    //		done("Naha, you don't.");
    //	} else {
            //Processa a fila de upload
           // console.log(file);
           // console.log(done);
		    //result = JSON.parse(file.trim());
           // console.log(result);

            //meuDropzone.processQueue();
            done();
    //	}
    }

});



meuDropzone.on("success", function(file, response) {
    /* Maybe display some more file information on your page */
   // console.log('Sucesso!');
    result = JSON.parse(response.trim());
    console.log(result);
    status_results = [];
    for (index = 0; index < result.length; index++) {
        status_results.push(result[index]['status_upload']);
    }
    var error_status = searchStringInArray ('error', status_results);
    var success_status = searchStringInArray ('success', status_results);
    var custom_html_response = '';
    if ( (error_status !== false &&  success_status !== false) || (error_status !== false) ){
            custom_html_response += '';
            custom_html_response += '<table class="table">';
            custom_html_response +=     '<thead>';
            custom_html_response +=         '<tr>';
            custom_html_response +=             '<th>Arquivo</th>';
            custom_html_response +=             '<th>Aviso</th>';
            custom_html_response +=             '<th>Status</th>';
            custom_html_response +=        '</tr>';
            custom_html_response +=     '</thead>';
            custom_html_response +=     '<tbody>';
            for (index = 0; index < result.length; index++) {

                if (result[index]['status'] == 'success'){ result[index]['icon'] = '<i class="far fa-check-circle text-success"></i>'; }
                if (result[index]['status'] == 'error'){ result[index]['icon'] = '<i class="far fa-times-circle text-danger"></i>'; }
                custom_html_response +=         '<tr>';
                custom_html_response +=             '<td>'+ result[index]['file_name'] +'</td>';
                custom_html_response +=             '<td>'+ result[index]['text_status'] +'</td>';
                custom_html_response +=             '<td>'+ result[index]['icon'] +'</td>';
                custom_html_response +=         '</tr>';
            }
            custom_html_response +=     '</tbody>';
            custom_html_response += '</table>';

        //finalizaCadastroFormandoColacao(custom_html_response, 'error', 'Ooops aconteceu algo durante o upload. <br> Por favor tente novamente removendo e adicionando os arquivos.');
    }else if(error_status == false &&  success_status !== false ){

		Swal.fire({
            icon: 'success',
            title: "Importado com sucesso!",
            //html: "<a href='../../../../../empresas' class='btn btn-primary font-weight-bolder'>Ver Resultados </a>",
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true,
            focusConfirm: false,
            allowEscapeKey: true,
            allowOutsideClick: true
          }).then((value) => {
            //$('#modalImportCSV').modal('hide');

        });
        //finalizaCadastroFormandoColacao();

    }



   // console.log(searchStringInArray ('success', status_results));
   // console.log(status_results);

});

meuDropzone.on("addedfiles", function(file) {
    /* Maybe display some more file information on your page */
    if( meuDropzone.getQueuedFiles().length > 0){
	    //$('#upload_button').html('Processar Arquivos <i class="icon-2x flaticon2-arrow-up"></i>');
	    $('#upload_button').attr('onclick','meuDropzone.processQueue()');
	    $('#upload_button').attr('disabled', false);
    }else{
	    $('#upload_button').attr('onclick','');
	    $('#upload_button').attr('disabled', true);
	    //$('#upload_button').attr('onclick','finalizaCadastroFormandoColacao()');
	    //$('#upload_button').html('Realizar Upload de Arquivos Depois <i class="icon-2x flaticon2-checkmark"></i>');
    }
});


meuDropzone.on("removedfile", function(file) {
    /* Maybe display some more file information on your page */
    if( meuDropzone.getQueuedFiles().length > 0){
	    //$('#upload_button').html('Processar Arquivos <i class="icon-2x flaticon2-arrow-up"></i>');
	    $('#upload_button').attr('onclick','meuDropzone.processQueue()');
	    $('#upload_button').attr('disabled', false);
    }else{
	    $('#upload_button').attr('onclick','');
	    $('#upload_button').attr('disabled', true);
	    //$('#upload_button').attr('onclick','finalizaCadastroFormandoColacao()');
	    //$('#upload_button').html('Realizar Upload de Arquivos Depois <i class="icon-2x flaticon2-checkmark"></i>');
    }
});

meuDropzone.on('sending', function(file, xhr, formData){
    //formData.append('id_user', $('#id_user').val());
    formData.append('id_admin', getCookie('id_admin'));
});
