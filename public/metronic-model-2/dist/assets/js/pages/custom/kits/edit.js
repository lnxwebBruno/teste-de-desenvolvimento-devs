var form = $("#edit.form");
form.submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    
	$.ajax({
		url : post_url,
		type: request_method,
        data : form_data,
        dataType: "json",
        beforeSend: function( xhr ) {
            //Desativa o Form
            KTApp.blockPage({
                opacity: 0.2,
                overlayColor: '#3699FF',
                state: 'danger',
                message: 'Enviando...'
              });
        },
        success:function(response) {
            console.log(response);
            KTApp.unblockPage();

            //Swal.fire("Opa!", response['status-message'], response['status']);
            var html = "";
            var confirmButtonText = "Ok";
            if(response['status'] == 'success'){
                html +=  response['status-message'];
                //html +=  '<br><a href="../../../../../kits" class="btn btn-primary btn-lg">Listar Kits</a>';
                confirmButtonText = 'OK';
                    KTApp.unblockPage();
                    Swal.fire({
                        icon: response['status'],
                        title: "Opa!",
                        text: response['status-message'],
                        html: html,
                        confirmButtonText:confirmButtonText
                    }).then((value) => {
                 /*        form[0].reset(); */
                 /*        $(".select_search_produtos").val('').trigger('change'); */
                 /*        $(".select_search_empresas").val('').trigger('change'); */
                    })
            }else{
                KTApp.unblockPage();
                Swal.fire({
                    icon: response['status'],
                    title: "Opa!",
                    text: response['status-message'],
                    html: html,
                    confirmButtonText:confirmButtonText
                  }).then((value) => {
                })
            }
        }
    });
});

var $repeater = $('#kt_repeater_1').repeater({
    initEmpty: false,
    isFirstItemUndeletable: true,
    show: function () {
        $(this).slideDown();
        $('#kt_repeater_1 > div div > div > div> span.select2-container').remove();
        $(".select_search_produtos").select2({
            placeholder: "Clique para buscar e selecionar",
            allowClear: true,
            ajax: {
                url: "../../../../../api/produtos/simplelist",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        empresas_id: $("#empresas_id").val()
                        //page: params.page
                    };
                },
                beforeSend : function()    {           
                    if($("#empresas_id").val() == "") {
                        toastr.error("Selecione um empresa antes de selecionar os produtos");
                        //$('#empresas_id').select2('open');
                        return false;
                    }
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
        
                    return {
                        results: data.data

                    };
                },
                cache: true
            },

        });
        $('#kt_repeater_1 > div div > div > div> span.select2-container').css('width','100%');
    },

    hide: function(deleteElement) {                 
        if(confirm('Tem certeza que deseja apagar este item?')) {
            $(this).slideUp(deleteElement);
        }                                
    }    
});

/* 	
$repeater.setList([
    { produtos_data: [{produtos_id: "12", produtos_qtd: "5"}]}
 ]); */

