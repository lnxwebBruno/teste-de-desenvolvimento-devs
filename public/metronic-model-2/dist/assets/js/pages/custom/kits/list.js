
	var table = $('#kt_datatable');

	// begin first table
	var datatable = table.DataTable({
		"language": {
            "url": "../../../../../metronic/dist/assets/js/datatable/pt-br.json"
        },
		sDom: 'lrtip',
		"columnDefs": [
	
			{
				"targets": [4, 5],
				"visible": false
			},
	  		{
				"targets": [ 0, 1, 2 ,3, 6, 7, 8],
				"visible": true
			},
		
		],
		responsive: true,
/* 			responsive: {
			details: {
				display: $.fn.dataTable.Responsive.display.modal( {
					header: function ( row ) {
						var data = row.data();
						return 'Detalhes de '+data['nome_fantasia'];
					}
				} ),
				renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
					tableClass: 'table'
				} )
			}
		}, */
		"paging": true,
		searching: true,
		"ordering": false,
		"lengthChange": false,
		"pageLength": 15,
		processing: true,
		serverSide: true,
		ajax: {
			url: '../../../../../api/kits/list?v=' + new Date().getTime(),
			type: 'POST',
/* 				data: {
				// parameters for custom backend script demo
				columnsDef: [
					'OrderID', 'Country',
					'ShipAddress', 'CompanyName', 'ShipDate',
					'Status', 'Type', 'Actions'],
			}, */
		},
		"columns": [
			{ "data": "kits_id" },
			{ "data": "cod_sku" },
			{ "data": "empresa" },
			{ "data": "descricao" },
            { "data": "empresas_id" },
            { "data": "status" },
            {data: "kits_id" , render : function ( data, type, row, meta ) {
                //console.log(row);
                var checked = "";
                if(row.status == 1){
                     checked = 'checked="checked"';
                }
                return '<span class="switch switch-primary"><label><input id="enable_kit_id_'+row.kits_id+'" onchange="enableKit('+row.kits_id+', 1)" type="checkbox" '+checked+' name="select"><span></span></label>';
			}},
			{data: "kits_id" , render : function ( data, type, row, meta ) {
				return type === 'display'  ?
				'<button onclick="deleta('+data+', true)" class="btn btn-sm btn-icon btn-danger"><i class="far fa-trash-alt"></i></button>' :
					data;
			}},
			{data: "kits_id" , render : function ( data, type, row, meta ) {
				//console.log(meta);
				return type === 'display'  ?
				'<a href="../../../../../kits/'+ data + '/edit" class="btn btn-sm btn-icon btn-primary"><i class="fas fa-edit"></i></a>':
					data;
				}},
		]
	});
	
	function searchQueryDt(){
		//datatable.ajax.reload();
		var cod_sku_q = $("#cod_sku_q").val();
		var descricao_q = $("#descricao_q").val();
		var enabled_q = $("#enabled_q").val();
		var empresas_id_q = $("#empresas_id_q").val();


		datatable.column(1).search(cod_sku_q).column(3).search(descricao_q).column(5).search(enabled_q).column(2).search(empresas_id_q).draw();
	}
	// On click bt search
	$('#search_dt').on( 'click', function () {
		searchQueryDt();
	} );
	// On enter
	$('.serch_input').on('keypress', function (e) {
		if(e.which === 13){
			searchQueryDt();
		}
  	});