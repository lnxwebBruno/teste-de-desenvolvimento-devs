
	const table = $('#cliente-faturamento-datatable');
	const clienteId = table.data('cliente-id');

	// begin first table
	const datatable = table.DataTable({
		"language": {
            "url": "../../../../../metronic/dist/assets/js/datatable/pt-br.json"
        },
		sDom: 'lrtip',
		responsive: true,
		"paging": true,
		searching: true,
		"ordering": false,
		"lengthChange": false,
		"pageLength": 15,
		processing: true,
		serverSide: true,
		ajax: {
			url: `../../../../../api/clientes/${clienteId}/faturamentos?v=${new Date().getTime()}`,
			type: 'POST',
		},
		"columns": [
			{ "data": "faturamentos_id" },
			{ "data": "empresa_nome_fantasia" },
			{ "data": "data_cadastro" },
			{ "data": "total", render: (data, type, row, meta) =>  {
				return type === 'display' ? 	data.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'}) : data;
			}},
			{data: "clientes_id" , render : function ( data, type, row, meta ) {
				return type === 'display'  ?
				'<div class="text-center"><a href="../../../../../faturamentos/' + row.faturamentos_id + '/edit"  target="_blank" class="btn btn-sm btn-icon btn-primary">Ver</a></div>':
					data;
			}},
		]
	});