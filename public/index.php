<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
// server should keep session data for AT LEAST 1 hour
$time_session = 3600 * 48;
ini_set('session.gc_maxlifetime', $time_session);

// each client should remember their session id for EXACTLY 1 hour
session_set_cookie_params($time_session);
session_start();

date_default_timezone_set("America/Sao_Paulo");
setlocale(LC_ALL, NULL);
setlocale(LC_ALL, 'pt_BR');
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');


require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes

//Login
$router->add('login', ['controller' => 'Login', 'action' => 'index']);
$router->add('auth', ['controller' => 'Login', 'action' => 'auth']);
$router->add('signup', ['controller' => 'Login', 'action' => 'signUp']);


// Home or Dashboard
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('dashboard', ['controller' => 'Home', 'action' => 'index']);
$router->add('home', ['controller' => 'Home', 'action' => 'index']);
$router->add('index', ['controller' => 'Home', 'action' => 'index']);



//Empresas
$router->add('core', ['controller' => 'Core', 'action' => 'index']);
//Api
$router->add('api/core/list', ['controller' => 'Core', 'action' => 'list']);
$router->add('api/core/simplelist', ['controller' => 'Core', 'action' => 'Simplelist']);
$router->add('api/core/add', ['controller' => 'Core', 'action' => 'add']);
$router->add('api/core/add', ['controller' => 'Core', 'action' => 'add']);
$router->add('api/core/disable', ['controller' => 'Core', 'action' => 'disable']);
$router->add('api/core/{id:\d+}/update', ['controller' => 'Core', 'action' => 'update']);
$router->add('api/core/{id:\d+}/add_marketplace', ['controller' => 'Core', 'action' => 'add_marketplace']);
$router->add('api/core/disable_marketplace', ['controller' => 'Core', 'action' => 'disable_marketplace']);
$router->add('api/core/{id:\d+}/update_marketplace', ['controller' => 'Core', 'action' => 'update_marketplace']);
$router->add('api/core/upload/photo', ['controller' => 'Core', 'action' => 'uploadPhoto']);

//Transportadoras
$router->add('transportadoras', ['controller' => 'Transportadoras', 'action' => 'index']);
//Api
$router->add('api/transportadoras/list', ['controller' => 'Transportadoras', 'action' => 'list']);
$router->add('api/transportadoras/simplelist', ['controller' => 'Transportadoras', 'action' => 'Simplelist']);
$router->add('api/transportadoras/add', ['controller' => 'Transportadoras', 'action' => 'add']);
$router->add('api/transportadoras/disable', ['controller' => 'Transportadoras', 'action' => 'disable']);
$router->add('api/transportadoras/{id:\d+}/update', ['controller' => 'Transportadoras', 'action' => 'update']);

//Fornecedores
$router->add('fornecedores', ['controller' => 'Fornecedores', 'action' => 'index']);
//Api
$router->add('api/fornecedores/list', ['controller' => 'Fornecedores', 'action' => 'list']);
$router->add('api/fornecedores/simplelist', ['controller' => 'Fornecedores', 'action' => 'Simplelist']);
$router->add('api/fornecedores/add', ['controller' => 'Fornecedores', 'action' => 'add']);
$router->add('api/fornecedores/disable', ['controller' => 'Fornecedores', 'action' => 'disable']);
$router->add('api/fornecedores/{id:\d+}/update', ['controller' => 'Fornecedores', 'action' => 'update']);

//Clientes
$router->add('clientes', ['controller' => 'Clientes', 'action' => 'index']);
$router->add('api/clientes/list', ['controller' => 'Clientes', 'action' => 'list']);
$router->add('api/clientes/simplelist', ['controller' => 'Clientes', 'action' => 'Simplelist']);
$router->add('api/clientes/add', ['controller' => 'Clientes', 'action' => 'add']);
$router->add('api/clientes/disable', ['controller' => 'Clientes', 'action' => 'disable']);
$router->add('api/clientes/{id:\d+}/update', ['controller' => 'Clientes', 'action' => 'update']);

//Produtos
$router->add('produtos', ['controller' => 'Produtos', 'action' => 'index']);
$router->add('api/produtos/list', ['controller' => 'Produtos', 'action' => 'list']);
$router->add('api/produtos/simplelist', ['controller' => 'Produtos', 'action' => 'Simplelist']);
$router->add('api/produtos/add', ['controller' => 'Produtos', 'action' => 'add']);
$router->add('api/produtos/disable', ['controller' => 'Produtos', 'action' => 'disable']);
$router->add('api/produtos/{id:\d+}/update', ['controller' => 'Produtos', 'action' => 'update']);
$router->add('api/produtos/upload/photo', ['controller' => 'Produtos', 'action' => 'uploadPhoto']);

//Kits
$router->add('kits', ['controller' => 'Kits', 'action' => 'index']);
$router->add('api/kits/list', ['controller' => 'Kits', 'action' => 'list']);
$router->add('api/kits/add', ['controller' => 'Kits', 'action' => 'add']);
$router->add('api/kits/disable', ['controller' => 'Kits', 'action' => 'disable']);
$router->add('api/kits/delete', ['controller' => 'Kits', 'action' => 'delete']);
$router->add('api/kits/{id:\d+}/update', ['controller' => 'Kits', 'action' => 'update']);

//Admin
$router->add('admin/users',  ['controller' => 'Users', 'namespace' => 'Admin',  'action' => 'index']);
$router->add('admin/user/{id:\d+}',  ['controller' => 'Users', 'namespace' => 'Admin',  'action' => 'edit']);
$router->add('admin/users/new', ['controller' => 'Users', 'namespace' => 'Admin', 'action' => 'new']);
$router->add('admin/logs', ['controller' => 'Logs', 'namespace' => 'Admin', 'action' => 'index']);

//User Actions
$router->add('api/users/add', ['controller' => 'AccountActions', 'action' => 'addUser']);
$router->add('api/users/edit/pass', ['controller' => 'AccountActions', 'action' => 'editPassAccount']);
$router->add('api/users/disable',  ['controller' => 'Users', 'namespace' => 'Admin',  'action' => 'disable']);
$router->add('api/users/{id:\d+}/update',  ['controller' => 'Users', 'namespace' => 'Admin',  'action' => 'update']);

$router->add('me', ['controller' => 'Me', 'action' => 'index']);
$router->add('me/pass', ['controller' => 'Me', 'action' => 'index']);

//Logout
$router->add('logout', ['controller' => 'Me', 'action' => 'logout']);
$router->add('sair', ['controller' => 'Me', 'action' => 'logout']);


//Static Pages
$router->add('guia', ['controller' => 'Pages', 'action' => 'guia']);


$router->add('cron', ['controller' => 'Cron', 'action' => 'importarPedidos']);

//Faturamentos
$router->add('faturamentos', ['controller' => 'Faturamentos', 'action' => 'index']);
$router->add('api/faturamentos/list', ['controller' => 'Faturamentos', 'action' => 'list']);
$router->add('api/faturamentos/simplelist', ['controller' => 'Faturamentos', 'action' => 'simplelist']);
$router->add('api/faturamentos/add', ['controller' => 'Faturamentos', 'action' => 'add']);
$router->add('api/faturamentos/disable', ['controller' => 'Faturamentos', 'action' => 'disable']);
$router->add('api/faturamentos/{id:\d+}/update', ['controller' => 'Faturamentos', 'action' => 'update']);
$router->add('api/clientes/{id:\d+}/faturamentos', ['controller' => 'Faturamentos', 'action' => 'getFaturamentosByClienteId']);

// CRUD Lojas Bling

//read by loja_id
$router->add('api/erp/bling/loja/{id:\d+}', ['controller' => 'ERP', 'action' => 'lojaBling']);
//update
$router->add('api/erp/bling/update/loja/{id:\d+}', ['controller' => 'ERP', 'action' => 'updateLojaBling']);
//add
$router->add('api/erp/bling/add/loja/{id:\d+}', ['controller' => 'ERP', 'action' => 'addLojaBling']);
//delete
$router->add('api/erp/bling/disable/loja', ['controller' => 'ERP', 'action' => 'disableLojaBling']);

//List Lojas Bling
//by empresas_id
$router->add('api/erp/lojas/bling/{id:\d+}', ['controller' => 'ERP', 'action' => 'listLojasBling']);
//all lojas
$router->add('api/erp/lojas/bling', ['controller' => 'ERP', 'action' => 'listLojasBling']);

$router->add('bling/pedidos/{id:\d+}', ['controller' => 'Bling', 'action' => 'pedidosByEmpresa']);

//Marketplaces
$router->add('api/marketplaces/simplelist', ['controller' => 'Marketplaces', 'action' => 'simplelist']);

//Rotas Padrão
$router->add('{controller}/{action}');
$router->add('{controller}/{id:\d+}/{action}');

//Exemplo Simples
///$router->add('empresas/{id:\d+}', ['controller' => 'Empresas', 'action' => 'edit']);
//var_dump($router->getParams());

$router->dispatch($_SERVER['QUERY_STRING']);
//var_dump($router->getRoutes());
