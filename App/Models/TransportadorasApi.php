<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class TransportadorasApi extends Cadastro\PessoaJuridica
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $nome_fantasia = "", string $cnpj = "", int $offset = 0, int $limit = 15)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'transportadoras_id DESC';
        //Collums
        $conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        $conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('transportadoras', $conditions);
        return $response;
    }
    public function simpleList(string $query = "", int $offset = 0, int $limit = 999)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'transportadoras_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, transportadoras_id AS id,  UPPER(nome_fantasia) AS text, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $conditions['custom_where_query'] = "WHERE (nome_fantasia LIKE '%$query%') OR (cnpj LIKE '%$query%') AND enabled = '1'";
        $response =  $this->getRows('transportadoras', $conditions);
        return $response;
    }
    
    public function getTransportadoraById(int $transportadoras_id)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['transportadoras_id'] = $transportadoras_id;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('transportadoras', $conditions);
        return $response;
    }

    public function getTransportadoraByCNPJ(string $cnpj)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['cnpj'] = $cnpj;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('transportadoras', $conditions);
        return $response;
    }


    public function add(array $data)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);


        if (!$this->isRazaoSocialValid($razao_social)) {
            $response['status-message'] = 'Preencha a razão social da transportadora.';
            return $response;
        }

        if (!$this->isNomeFantasiaValid($nome_fantasia)) {
            $response['status-message'] = 'Preencha o nome fantasia da transportadora.';
            return $response;
        }


        if (!$this->isCNPJValidFilter($cnpj)['valid'])
		{
            $response['status-message'] = 'Digite um CNPJ válido.';
            return $response;
        }else{
            $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
        }

        if ($this->isTransportadoraCNPJExist($cnpj))
		{
            $response['status-message'] = 'Este CNPJ já está cadastrado';
            return $response;
        }

        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }
        if (!$this->isCepValid($cep))
		{
            $response['status-message'] = 'Digite um cep válido.';
            return $response;
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }

        $insert =  $this->insert('transportadoras', $data);

        if($insert){
            $response['status'] = 'success';
            $response['status-message'] = 'A transportadora foi registrada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function edit(array $data, int $transportadoras_id)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);


        if (!$this->isRazaoSocialValid($razao_social)) {
            $response['status-message'] = 'Preencha a razão social da transportadora.';
            return $response;
        }

        if (!$this->isNomeFantasiaValid($nome_fantasia)) {
            $response['status-message'] = 'Preencha o nome fantasia da transportadora.';
            return $response;
        }


        if (!$this->isCNPJValidFilter($cnpj)['valid'])
		{
            $response['status-message'] = 'Digite um CNPJ válido.';
            return $response;
        }else{
            $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
        }

        
       // var_dump($this->isTransportadoraCNPJExist($cnpj));
        if($cnpj !== $this->getTransportadoraById($transportadoras_id)['data'][0]['cnpj']){

            if ($this->isTransportadoraCNPJExist($cnpj))
            {
                $response['status-message'] = 'Este novo CNPJ já está cadastrado';
                return $response;
            }
        }

        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }

        if (!$this->isCEPValidFilter($cep)['valid'])
		{
            $response['status-message'] = 'Digite um CEP válido.';
            return $response;
        }else{
            $cep = $data['cep'] = $this->isCEPValidFilter($cep)['cep'];
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }

        $where['transportadoras_id'] = $transportadoras_id;
        $update =  $this->update('transportadoras', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A transportadora foi atualizada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }


    public function disable(int $transportadoras_id)
    {
        $response['status'] = 'error';

        if (!$this->isTransportadoraIdValid($transportadoras_id))
		{
            $response['status-message'] = 'A transportadora não existe, ou já foi deletada';
            return $response;
        }
        
        $where['transportadoras_id'] = $transportadoras_id;
        $data['enabled'] = '0';
        $update =  $this->update('transportadoras', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A transportadora foi deletada com sucesso!';
            return $response;
        }
    }

    /* Verifica se a transportadora existe */
    public function isTransportadoraIdValid(int $transportadoras_id): bool
    {
        $conditions['where']['transportadoras_id'] = $transportadoras_id;
        $conditions['where']['enabled'] = '1';
        $response =  $this->getRows('transportadoras', $conditions);
        return $response['gotData'];
    }

    /* Verifica se a transportadora pelo CNPJ */
    public function isTransportadoraCNPJExist(string $cnpj):bool
    {
        $conditions['where']['cnpj'] = $cnpj;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('transportadoras', $conditions);
        return $response['gotData'];
    }

}
