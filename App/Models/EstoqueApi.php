<?php

namespace App\Models;

use PDO;
use App\Models\EmpresasApi;
use App\Models\ProdutosApi;
/**
 * Example user model
 *
 * PHP version 7.0
 */
class EstoqueApi extends \App\Models\CrudInit
{
    /**
     * Get all the users as an associative array
     *e
     * @return array
     */
    public function list($cod_sku = "", $descricao = "", $barcode = "", $nome_fantasia = "", $qtd_total = "", $qtd_reservado = "", $qtd_disponivel = "", int $offset = 0, int $limit = 15)
    {

        //return func_get_args();
        $produtosApi = new ProdutosApi();
        $empresasApi = new EmpresasApi();
        $produtos_response  = array();
       // $cod_sku = "RCF0132";
        //$descricao = "Travesseiro";
        //$barcode = "7896806212265";
        //$nome_fantasia = "LNX";

        $produtos_list_id_search = array();
        if(!empty($nome_fantasia)){
        //Pesquisa pelo ID das Empresas e seus produtos
            $empresas_data = $empresasApi->simpleList($nome_fantasia,  $offset = 0,  $limit = 9999);
            if($empresas_data['gotData']){
                for ($i=0; $i < sizeof($empresas_data['data']); $i++) { 
                    $empresas_id = $empresas_data['data'][$i]['empresas_id'];
                    $produtos_list_search = $produtosApi->list($empresas_id, $cod_sku, $barcode, $descricao,$offset = 0,  $limit = 999999999);
                    if($produtos_list_search['gotData']){
                        //var_dump($produtos_list_search['data']);
                        for ($produtos_list_c=0; $produtos_list_c < sizeof($produtos_list_search['data']); $produtos_list_c++) { 
                            $produtos_list_id_search[] =  $produtos_list_search['data'][$produtos_list_c]['produtos_id'];
                        }
                    }
                }
            }else{
                return $produtos_response;

            }
        }else{
            //Pesquisa pelo produtos atraves dos campos de busca
            if( $cod_sku !== "" || $descricao !== "" || $barcode !== "" || $nome_fantasia !== ""){
                $produtos_list_search = $produtosApi->list(0, $cod_sku, $barcode, $descricao,$offset = 0,  $limit = 999999999);
                if($produtos_list_search['gotData']){
                    //var_dump($produtos_list_search['data']);
                    for ($produtos_list_c=0; $produtos_list_c < sizeof($produtos_list_search['data']); $produtos_list_c++) { 
                        $produtos_list_id_search[] =  $produtos_list_search['data'][$produtos_list_c]['produtos_id'];
                    }

                }else{
                    return $produtos_response;
                }
            } 

        }
        //Pesquisa pelo produtos no estoque pelos ids recuperados anteriormente se teve campo de busca preenchido e obteve resultados
        //Monta Query de forma dinamica
        //var_dump($produtos_list_id_search);
        if(!empty($produtos_list_id_search)){
            $q = 0;
            $sql_produtos = "";
            foreach($produtos_list_id_search as $key => $value){
                $pre = ($q > 0)?' OR ':'';
                $sql_produtos .= $pre.'produto_id'." = '".$value."'";
                $q++;
            }
            //Monta a query de Busca
            $conditions['return_type'] = 'all';
            //$conditions['limit'] = 999999;
            //$conditions['offset'] = $offset;
            $conditions['order_by'] = 'created DESC';
            $conditions['custom_where_query'] = "WHERE $sql_produtos AND unavailable = '0'";
        }else{
            //Se não teve nenhum campo preenchido //Default Search
            //Config
            $conditions['return_type'] = 'all';
            //$conditions['limit'] = 999999;
            //$conditions['offset'] = $offset;
            //$conditions['where']['enabled'] = 1;
            $conditions['order_by'] = 'created DESC';
            $conditions['where']['unavailable'] = "0";
            //Mount Query
            $conditions['select'] = 'produto_id, status';
        }

        $data_estoque =  $this->getRows('estoque', $conditions);
        $produtos_id = array();
        if($data_estoque['gotData']){
           for ($i=0; $i < sizeof($data_estoque['data']) ; $i++) { 
                $produtos_id[] =  $data_estoque['data'][$i]['produto_id'];
           }

            $produtos_id = array_values(array_unique($produtos_id));
            //var_dump($produtos_id);
            for ($p=0; $p < sizeof($produtos_id); $p++) { 
                $produtos_response[$p] = array();
                $produto_data = $produtosApi->getProdutoById($produtos_id[$p])['data'][0]; 
                $produtos_response[$p]['produto_id'] = $produtos_id[$p];
                $produtos_response[$p]['descricao'] = $produto_data['descricao'];
                $produtos_response[$p]['empresa'] = $produto_data['empresa'];
                $produtos_response[$p]['cod_sku'] = $produto_data['cod_sku'];
                $produtos_response[$p]['barcode'] = $produto_data['barcode'];
                $produtos_response[$p]['disponivel'] = 0;
                $produtos_response[$p]['reservado'] = 0;
            }

        
            for ($p=0; $p < sizeof($produtos_id); $p++) { 

                for ($i=0; $i < sizeof($data_estoque['data']) ; $i++) { 
                    if($produtos_id[$p] ==  $data_estoque['data'][$i]['produto_id']){
                        if($data_estoque['data'][$i]['status'] == 'disponivel'){
                            $produtos_response[$p]['disponivel']++;
                        }
                        if($data_estoque['data'][$i]['status'] == 'reservado'){
                            $produtos_response[$p]['reservado']++;
                        }
                    }
                }
                $produtos_response[$p]['total'] = $produtos_response[$p]['disponivel'] + $produtos_response[$p]['reservado'];
            }


            function searchInResponse(array $array, array $pairs)
            {
                foreach ($pairs as $pKey => $pVal) {
                    if($pVal == ""){
                        unset($pairs[$pKey]);
                    }
                }
                //var_dump($pairs);
                $found = array();
                foreach ($array as $aKey => $aVal) {
                    $coincidences = 0;
                    foreach ($pairs as $pKey => $pVal) {
                        if (array_key_exists($pKey, $aVal) && $aVal[$pKey] == intval($pVal)) {
                            $coincidences++;
                        }
                    }
                    if ($coincidences == count($pairs)) {
                        $found[$aKey] = $aVal;
                    }
                }
        
                return array_values($found);
            }
           /*  $qtd_total = "1"; 
            $qtd_disponivel = "1"; 
            $qtd_reservado = "0"; */
            //Filtro de Quantidade
            if($qtd_total !== "" || $qtd_reservado !== "" || $qtd_disponivel !== ""){
                $filtred_response = searchInResponse($produtos_response, array('total' => $qtd_total, 'disponivel' => $qtd_disponivel,  'reservado' => $qtd_reservado));
                return $filtred_response;
            }

            return $produtos_response;
        }
    }
    //Select2 Method GET
    public function simpleList(string $query = "", int $offset = 0, int $limit = 999)
    {
        //Config
       /*  $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'empresas_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, empresas_id AS id,  UPPER(nome_fantasia) AS text, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $conditions['custom_where_query'] = "WHERE (nome_fantasia LIKE '%$query%') OR (cnpj LIKE '%$query%') AND enabled = '1'";
        $response =  $this->getRows('empresas', $conditions);
        return $response; */
    }

    public function verifyProduct($produtos_id){
        $conditions['where']['unavailable'] = "0";
        $conditions['where']['separacao'] = "0";
        $conditions['where']['status'] = "disponivel";
        $conditions['where']['produto_id'] = $produtos_id;
        //Mount Query
        $data_estoque =  $this->getRows('estoque', $conditions);
        return $data_estoque;
    }


  


    public function add(array $data)
    {

     /*    $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        return $response; */
    }

    public function edit(array $data, int $empresas_id)
    {

    }


    public function disable(int $empresas_id)
    {
/*         $response['status'] = 'error';

        if (!$this->isEmpresaIdValid($empresas_id))
		{
            $response['status-message'] = 'A empresa não existe, ou já foi deletada';
            return $response;
        }
        
        $where['empresas_id'] = $empresas_id;
        $data['enabled'] = '0';
        $update =  $this->update('empresas', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A empresa foi deletada com sucesso!';
            return $response;
        } */
    }

    public function getNearlyAdressByProductID($produtos_id){

        $response['endereco'] = null;
        $produtosApi = new ProdutosApi();
        $produto_data = $produtosApi->getProdutoById($produtos_id)['data'][0]; 

        $response['cod_sku'] = $produto_data['cod_sku'];
        $response['barcode'] = $produto_data['barcode'];
        $conditions['where']['produto_id'] = $produtos_id;
        $conditions['where']['separacao'] = '0';
        $conditions['where']['status'] = 'reservado';
        $conditions['limit'] = 1;
        $data_estoque =  $this->getRows('estoque', $conditions);
        //var_dump($data_estoque);
        if($data_estoque['gotData']){
            //$response['endereco'] = $data_estoque['data'][0]['endereco'];
            $response['endereco'] = $data_estoque['data'][0]['endereco'];
            $estoque_id = $data_estoque['data'][0]['estoque_id'];
            $conditions_up['estoque_id'] = $estoque_id;
            $data_up['separacao'] = 1; 
            $update_estoque =  $this->update('estoque', $data_up, $conditions_up);
            
        }
        return $response;
    }

    public function isValidEndereco($endereco_estoque):bool
    {
        $conditions['where']['posicao'] = $endereco_estoque;
        $data_estoque =  $this->getRows('estoque_enderecamento', $conditions);
        return $data_estoque['gotData'];
    }
}
