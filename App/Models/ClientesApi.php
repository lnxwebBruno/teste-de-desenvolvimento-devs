<?php

namespace App\Models;

use PDO;
//use App\Models\Cadastro\PessoaFisica;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientesApi extends Cadastro\PessoaJuridica
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $nome_fantasia = "", string $cnpj = null, int $offset = 0, int $limit = 15)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['order_by'] = 'clientes_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['custom_where_query'] = "WHERE (nome LIKE '%$nome_fantasia%' OR  nome_fantasia LIKE '%$nome_fantasia%') AND (cpf LIKE '%$cnpj%' OR cnpj LIKE '%$cnpj%') AND enabled = '1'";
        $conditions['select'] = '*, Coalesce(nome_fantasia,nome) as nome_fantasia, Coalesce(cpf,cnpj) as cnpj, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('clientes', $conditions);
        return $response;
    }

    public function simpleList(string $query = "", int $offset = 0, int $limit = 999)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'clientes_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, clientes_id AS id, Coalesce(nome_fantasia,nome) as text, Coalesce(cpf,cnpj) as cnpj, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $conditions['custom_where_query'] = "WHERE (nome LIKE '%$query%' OR  nome_fantasia LIKE '%$query%') AND (cpf LIKE '%$query%' OR cnpj LIKE '%$query%') AND enabled = '1'";
        $response =  $this->getRows('clientes', $conditions);
        return $response;
    }
    public function getClienteById(int $clientes_id)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['clientes_id'] = $clientes_id;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome) AS nome, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('clientes', $conditions);
        return $response;
    }

    public function getClienteByCnpjOrCpf(string $cnpjOrCpf, $isCnpj = true)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['custom_where_query'] = 'AND  '. ($isCnpj ? 'cnpj' : 'cpf') . ' = ' . $cnpjOrCpf;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        return $this->getRows('clientes', $conditions);
    }


    public function add(array $data)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        if($tipo_cadastro == "PF"){

            $data['cnpj'] = NULL;
            $data['razao_social'] = NULL;
            $data['nome_fantasia'] = NULL;
            if(!Cadastro\PessoaFisica::isCPFValidFilter($cpf)){
                $response['status-message'] = 'Digite um CPF válido.';
                return $response;
            }else{
                $data['cpf'] = Cadastro\PessoaFisica::isCPFValidFilter($cpf);
            }

            if ($this->isClienteCPFExist($cpf))
            {
                $response['status-message'] = 'Este CPF já está cadastrado';
                return $response;
            }
        }

        if($tipo_cadastro == "PJ"){
           $data['cpf'] = NULL;
           $data['nome'] = NULL;
            if (!$this->isRazaoSocialValid($razao_social)) {
                $response['status-message'] = 'Preencha a razão social do cliente.';
                return $response;
            }
    
            if (!$this->isNomeFantasiaValid($nome_fantasia)) {
                $response['status-message'] = 'Preencha o nome fantasia do cliente.';
                return $response;
            }
    
    
            if (!$this->isCNPJValidFilter($cnpj)['valid'])
            {
                $response['status-message'] = 'Digite um CNPJ válido.';
                return $response;
            }else{
                $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
            }
    
            if ($this->isClienteCNPJExist($cnpj))
            {
                $response['status-message'] = 'Este CNPJ já está cadastrado';
                return $response;
            }
        }




        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }
        if (!$this->isCepValid($cep))
		{
            $response['status-message'] = 'Digite um cep válido.';
            return $response;
        }

        $insert =  $this->insert('clientes', $data);

        if($insert){
            $response['status'] = 'success';
            $response['status-message'] = 'O cliente foi registrado com sucesso!';
            $response['cliente_id'] = $insert;
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function edit(array $data, int $clientes_id)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        if($tipo_cadastro == "PF"){

            $data['cnpj'] = NULL;
            $data['razao_social'] = NULL;
            $data['nome_fantasia'] = NULL;
            if(!Cadastro\PessoaFisica::isCPFValidFilter($cpf)){
                $response['status-message'] = 'Digite um CPF válido.';
                return $response;
            }else{
                $data['cpf'] = Cadastro\PessoaFisica::isCPFValidFilter($cpf);
            }
            $cpf = preg_replace('/[^0-9]/', '', (string) $cpf); 
            if($cpf !== $this->getClienteById($clientes_id)['data'][0]['cpf']){
    
                if ($this->isClienteCPFExist($cpf))
                {
                    $response['status-message'] = 'Este CPF já está cadastrado';
                    return $response;
                }
            }
        }

        if($tipo_cadastro == "PJ"){
            $data['cpf'] = NULL;
            $data['nome'] = NULL;
            if (!$this->isRazaoSocialValid($razao_social)) {
                $response['status-message'] = 'Preencha a razão social do cliente.';
                return $response;
            }
    
            if (!$this->isNomeFantasiaValid($nome_fantasia)) {
                $response['status-message'] = 'Preencha o nome fantasia do cliente.';
                return $response;
            }
    
    
            if (!$this->isCNPJValidFilter($cnpj)['valid'])
            {
                $response['status-message'] = 'Digite um CNPJ válido.';
                return $response;
            }else{
                $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
            }
    
            
           // var_dump($this->isTransportadoraCNPJExist($cnpj));
            if($cnpj !== $this->getClienteById($clientes_id)['data'][0]['cnpj']){
    
                if ($this->isClienteCNPJExist($cnpj))
                {
                    $response['status-message'] = 'Este CNPJ já está cadastrado';
                    return $response;
                }
            }
    


        }




        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }
        if (!$this->isCepValid($cep))
		{
            $response['status-message'] = 'Digite um cep válido.';
            return $response;
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }


 


        $where['clientes_id'] = $clientes_id;
        $update =  $this->update('clientes', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O cliente foi atualizado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }


    public function disable(int $clientes_id)
    {
        $response['status'] = 'error';

        if (!$this->isClienteIdValid($clientes_id))
		{
            $response['status-message'] = 'O cliente não existe, ou já foi deletado';
            return $response;
        }
        
        $where['clientes_id'] = $clientes_id;
        $data['enabled'] = '0';
        $update =  $this->update('clientes', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O cliente foi deletado com sucesso!';
            return $response;
        }
    }

    /* Verifica se o cliente existe pelo id */
    public function isClienteIdValid(int $clientes_id): bool
    {
        $conditions['where']['clientes_id'] = $clientes_id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('clientes', $conditions);
        return $response['gotData'];
    }

    /* Verifica se o cliente existe pelo CNPJ */
    public function isClienteCNPJExist(string $cnpj):bool
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        $conditions['where']['cnpj'] = $cnpj;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('clientes', $conditions);
        return $response['gotData'];
    }

    /* Verifica se o cliente existe pelo CPF */
    public function isClienteCPFExist(string $cpf):bool
    {
        $cpf = preg_replace('/[^0-9]/', '', (string) $cpf);
        $conditions['where']['cpf'] = $cpf;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('clientes', $conditions);
        return $response['gotData'];
    }

}
