<?php

namespace App\Models\Integracoes;

use App\Models\Cadastro\PessoaFisica;
use App\Models\CrudInit;
use App\Models\MarketplacesApi;
use PDO;
use App\Models\Integracoes\BlingApi;
use App\Models\Integracoes\CallbackApi;
use App\Models\FaturamentosApi;
use App\Models\ClientesApi;
use App\Models\EmpresasApi;
use App\Models\ProdutosApi;
/* use App\Models\Cadastro\PessoaJuridica;
use App\Models\Cadastro\PessoaJuridica; */
 
/**
 * Example user model
 *
 * PHP version 7.0
 */
class ERPApi extends \App\Models\CrudInit
{
    /**
     * Get all the users as an associative array
     *e
     * @return array
     */
    //Lista os ERP disponiveis para integração
    public function list(){

        $conditions['where']['enabled'] = 1;
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('erp_integracao', $conditions);
        if($response['gotData']){
            return $response['data'];
        }else{
            return false;
        }
        return $response;
    }

    public function listLojasBling($empresas_id  = false){
        $blingApi = new BlingApi();
        $wempresas = "";
        if($empresas_id){
            $wempresas = " AND lojas_bling.empresas_id = '$empresas_id'";
        }
        //Mount Query
        $conditions['custom_where_query'] = "INNER JOIN (marketplaces) ON lojas_bling.marketplace_id = marketplaces.marketplace_id WHERE lojas_bling.enabled = '1' $wempresas";
        $conditions['select'] = 'lojas_bling.*,  marketplaces.nome as nome_marketplace, DATE_FORMAT(lojas_bling.created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(lojas_bling.modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        
        $response =  $this->getRows('lojas_bling', $conditions);
        if($response['gotData']){
            for ($i=0; $i < sizeof($response['data']); $i++) { 
                $response['data'][$i]['situacao_importar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_importar']);
                $response['data'][$i]['situacao_apos_importar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_apos_importar']);
                $response['data'][$i]['situacao_atualizar_processo_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_atualizar_processo']);
                $response['data'][$i]['situacao_verificado_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_verificado']);
                $response['data'][$i]['situacao_apos_finalizar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_apos_finalizar']);
                $response['data'][$i]['situacao_cancelar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_cancelar']);
            }
        } 
        return $response;
    }

    public function getLojaBlingByID(int $loja_id){
        $blingApi = new BlingApi();
        $conditions['where']['loja_id'] = $loja_id;
        $conditions['where']['enabled'] = 1;
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('lojas_bling', $conditions);
        if($response['gotData']){
            for ($i=0; $i < sizeof($response['data']); $i++) { 
                $response['data'][$i]['situacao_importar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_importar']);
                $response['data'][$i]['situacao_apos_importar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_apos_importar']);
                $response['data'][$i]['situacao_atualizar_processo_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_atualizar_processo']);
                $response['data'][$i]['situacao_verificado_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_verificado']);
                $response['data'][$i]['situacao_apos_finalizar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_apos_finalizar']);
                $response['data'][$i]['situacao_cancelar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_cancelar']);
            }
            return $response['data'][0];
        }
        return $response['gotData'];
    }

    public function editLojaBling(array $data, int $loja_id){

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        //Acrescentar Validação de Situação repetida aqui

        $where['loja_id'] = $loja_id;
        $update =  $this->update('lojas_bling', $data, $where);
        //return $update;
        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A loja foi atualizada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;

    }

    public function addLojaBling(array $data, int $empresas_id){

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);
        
        //Acrescentar Validação de Situação repetida aqui

        $data['empresas_id'] = $empresas_id;
        $update =  $this->insert('lojas_bling', $data);
        //return $update;
        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A loja foi atualizada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;

    }

    public function disableLojaBling(int $loja_id)
    {
        $response['status'] = 'error';

        if (!$this->getLojaBlingByID($loja_id))
        {
            $response['status-message'] = 'A empresa loja não existe, ou já foi deletada';
            return $response;
        }
        
        $where['loja_id'] = $loja_id;
        $data['enabled'] = '0';
        $update =  $this->update('lojas_bling', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A loja foi deletada com sucesso!';
            return $response;
        }
    }

    public function getLojaByNumeroEmpresa($numero_bling, $empresas_id = false){

        $blingApi = new BlingApi();

        $conditions['where']['numero_bling'] = $numero_bling;
        if($empresas_id){
            $conditions['where']['empresas_id'] = $empresas_id;
        }
        $conditions['where']['enabled'] = 1;
        $conditions['where']['importar'] = 1;
        $conditions['limit'] = 1;
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('lojas_bling', $conditions);
        if($response['gotData']){
            for ($i=0; $i < sizeof($response['data']); $i++) { 
                $response['data'][$i]['situacao_importar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_importar']);
                $response['data'][$i]['situacao_apos_importar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_apos_importar']);
                $response['data'][$i]['situacao_atualizar_processo_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_atualizar_processo']);
                $response['data'][$i]['situacao_verificado_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_verificado']);
                $response['data'][$i]['situacao_apos_finalizar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_apos_finalizar']);
                $response['data'][$i]['situacao_cancelar_text'] = $blingApi->situacaoIdToText( $response['data'][$i]['situacao_cancelar']);
            }
            return $response['data'][0];
        }
        return $response['gotData'];


    }
    // Importa Pedidos dos ERPs : Bling e (futuro erp aqui)
    public function importarPedidos()
    {
        $blingApi = new BlingApi();
        $empresasApi = new EmpresasApi();
        //$cargasApi = new CargasApi();
        $clientesApi = new ClientesApi();
        $produtosApi = new ProdutosApi();
        $faturamentosApi = new FaturamentosApi();

        $empresas_data = $empresasApi->list('', '' , 0, 999999);
        $cargas = array();
        //Pedidos do Bling
        //Listas as Empresas
        $data_response = array();
        $filters = array();
        if($empresas_data['gotData']) {
            for ($i = 0; $i < sizeof($empresas_data['data']); $i++) {

                $empresas_id[$i] = $empresas_data['data'][$i]['empresas_id'];
                //Captura pedidos do Bling
                //Apenas empresas "Ativadas"
                if ($empresas_data['data'][$i]['erp_integracao'] == 'bling' && $empresas_data['data'][$i]['service_enabled'] == 1) {
                    //var_dump($empresas_data['data'][$i]);
                    if (!empty($empresas_data['data'][$i]['erp_integracao_chave_api'])) {

                        $blingApi = new BlingApi(); //Instancia Classe do Bling
                        //Defina a API KEY
                        $blingApi->strApiKey = $empresas_data['data'][$i]['erp_integracao_chave_api'];
                        $date_inicio = date("d/m/Y", strtotime("-1 days"));
                        $date_fim = date("d/m/Y");
                        //Filtro por Data
                        $filters['dataEmissao'] = " $date_inicio TO $date_fim";

                        /*
                        Pedidos de Venda:
                            6 = Em Aberto
                            9 = Atendido
                            12 = Cancelado
                            15 = Em andamento
                            18 = Venda Agenciada
                            21 = Em digitação
                            24 = Verificado 
                        */
                        $data_response = $blingApi->getOrders($filters);
                        if (isset($data_response['pedidos'])) {
                            //Pedidos
                            $pedidos = $data_response['pedidos'];
                            for ($p = 0; $p < sizeof($pedidos); $p++) {
                                $pedidos[$p]["pedido"]['situacao'];
                                //Verifica a chave loja do pedido e configura as padrões de importação
                                if (isset($pedidos[$p]["pedido"]['loja'])) {
                                    //Busca a loja selecionada pelo id da loja que vem no pedido
                                    $loja_import_config = $this->getLojaByNumeroEmpresa($pedidos[$p]["pedido"]['loja'], $empresas_id[$i]);
                                } else {
                                    //Caso não exista a a chave "loja" ela carrega uma configuração padrão
                                    //Loja Padrão Config
                                    $loja_import_config = $this->getLojaByNumeroEmpresa('0', $empresas_id[$i]);
                                }

                                if ($loja_import_config) {
                                    if ($pedidos[$p]["pedido"]['situacao'] == $loja_import_config['situacao_importar_text']) {
                                        //Verifica se faturamento existe
                                        $pedidoIdBling = $pedidos[$p]['pedido']['numero'];
                                        $faturamentoExist = $faturamentosApi->hasFaturamento($pedidoIdBling, $loja_import_config['marketplace_id'], $empresas_id[$i]);
                                        if (!$faturamentoExist) {
                                            //cria cliente
                                            $clientes_id = $this->createClienteIfNecessary($pedidos[$p]['pedido']['cliente']);
                                            $registerFaturamento = true;

                                            //Aqui temos comentando uma checagem de não cadastrar o faturamento se o produto não existir,
                                            //Neste caso se o produto não existir vamos cadastra-lo no sistema
                                            $itens = $pedidos[$p]["pedido"]['itens'];
                                            if (!isset($itens) || count($itens) === 0) $registerFaturamento = false;
                                            if ($registerFaturamento) {
                                                //Cria Pedido
                                                $this->createFaturamentoIfNecessary($empresas_id[$i], $loja_import_config['marketplace_id'], $clientes_id, $pedidos[$p]['pedido']);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $cargas;
        }
    }

    private function createClienteIfNecessary($cliente): int
    {
        $clientesApi = new ClientesApi();
        //Para o bling cpnj e cpf é a mesma coisa
        $cnpj = trim($cliente['cnpj']);
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        $clientes_id = 0;
        //Se o cliente não existe cria um cadastro
        if((!$clientesApi->isClienteCNPJExist($cnpj) && !$clientesApi->isClienteCPFExist($cnpj)) || empty($cnpj) ) {
            //Cria Cliente e captura cliente id
            $data = [];
            if(!empty($cnpj)) {
                if(PessoaFisica::isCPFValidFilter($cnpj)) {
                    $data['tipo_cadastro'] = "PF";
                    $data['cpf'] = $cnpj;
                    $data['nome'] = $cliente['nome'];

                }else if($clientesApi->isCNPJValidFilter($cnpj)['valid']){
                    $data['tipo_cadastro'] = "PJ";
                    $data['cnpj'] = $cnpj;
                    $data['nome_fantasia'] = $cliente['nome'];
                    $data['razao_social'] = $cliente['nome'];
                }
            }else{
                $data['nome_fantasia'] = $cliente['nome'];
                $data['nome'] = $cliente['nome'];
                $data['tipo_cadastro'] = "PF";
                $data['cpf'] = $cnpj;
            }

            $data['endereco'] = $cliente['endereco'];
            $data['endereco_n'] = $cliente['numero'];
            $data['endereco_complemento'] = $cliente['complemento'];
            $data['endereco_bairro'] = $cliente['bairro'];
            $data['cep'] = $cliente['cep'];
            $data['cidade'] = $cliente['cidade'];
            $data['uf'] = $cliente['uf'];
            $data['nome_responsavel'] = $cliente['nome'];
            $data['email'] = $cliente['email'];
            $data['celular'] =$cliente['celular'];
            $data['telefone'] = $cliente['fone'];
            //Adiciona o Cliente
            $response = $clientesApi->add($data);
            if($clientes_id['status'] == 'success') {
                //Id do cliente cadastrado
                $clientes_id = $response['clientes_id'];
            }
        } else {
            //Captura o clientes_id pelo CNPJ
            $cnpj_filtered = $clientesApi->isCNPJValidFilter($cnpj);
            if($cnpj_filtered){
                if($clientesApi->getClienteByCnpjOrCpf($cnpj_filtered['cnpj'])['gotData']){
                    $clientes_id =  $clientesApi->getClienteByCnpjOrCpf($cnpj)['data'][0]['clientes_id'];
                }
            } else {
                //Se não for um CNPJ ele captura o clientes_id pelo CPF
                $cpf = PessoaFisica::isCPFValidFilter($cnpj);
                $response = $clientesApi->getClienteByCnpjOrCpf($cpf, false);
                if($response['gotData']){
                    $clientes_id =  $response['data'][0]['clientes_id'];
                }
            }
        }

        return $clientes_id;
    }

    private function createFaturamentoIfNecessary(int $empresaId, int $marketplaceId, int $clienteId, array $pedido)
    {
        //Aqui Transformamos o pedido em um faturamento no sistema
        $faturamentosApi = new FaturamentosApi();

        $data = [];
        $data['empresas_id'] = $empresaId ;
        $data['pedido_id_bling'] = $pedido['numero'];
        $data['situacao'] = $pedido['situacao'];
        $data['clientes_id'] = $clienteId;
        $data['data_cadastro'] = $pedido['data'];
        $data['observacoes'] = $pedido['observacoes'];
        $data['valor_frete'] = floatval($pedido['valorfrete']);
        $data['total_produtos'] = floatval($pedido['totalprodutos']);
        $data['total_venda'] = floatval($pedido['totalvenda']);
        /*if(isset($pedido['tipoIntegracao'])){
            $data['tipoIntegracao'] = $pedido['tipoIntegracao'];
        }
        if(isset($pedido['numeroPedidoLoja'])){
            $data['numeroPedidoLoja'] = $pedido['numeroPedidoLoja'];
        }*/
        $data['marketplace_id'] =  $marketplaceId;
        //$data['itens_json'] = json_encode($pedido['itens']);
        //$data['origem'] =  'bling';

        if (isset($pedido['parcelas'])) {
            for ($i = 0; $i < count($pedido['parcelas']); $i++) {
                $parcela = $pedido['parcelas'][$i]['parcela'];
                $data['financeiro'][$i]['p_valor'] = $parcela['valor'];
                $data['financeiro'][$i]['data_vencimento'] = date ('Y-m-d H:i:s', strtotime(str_replace('-','/', $parcela['dataVencimento'])));
                $data['financeiro'][$i]['p_data'] = date("Y-m-d H:i:s");
                $data['financeiro'][$i]['p_recebido'] = '';
                $data['financeiro'][$i]['parcela'] = '';
                $data['financeiro'][$i]['p_formapag'] = $parcela['forma_pagamento']['descricao'];
            }
        } else {
            $data['financeiro'] = [];
        }

        $produtosApi = new ProdutosApi();
        $dataProduto = [];
        for ($i = 0; $i < count($pedido['itens']); $i++) {
            $pedidoItem = $pedido['itens'][$i]['item'];
            $produtoCheck = $produtosApi->existProdutoForEmpresaId( $pedidoItem['codigo'], $empresaId);
            if (!$produtoCheck) {
                $dataProduto['descricao'] = $pedidoItem['descricao'];
                $dataProduto['peso_kg'] = $pedidoItem['pesoBruto'];
                $dataProduto['peso_kg'] = $pedidoItem['pesoBruto'];
                $dataProduto['altura_cm'] = $pedidoItem['altura'];
                $dataProduto['largura_cm'] = $pedidoItem['largura'];
                $dataProduto['largura_cm'] = $pedidoItem['largura'];
                $dataProduto['empresas_id'] = $empresaId;
                $dataProduto['enabled'] = 1;
                $dataProduto['cod_sku'] = $pedidoItem['codigo'];
                $dataProduto['barcode'] = $pedidoItem['gtin'];
                $produtoId = $produtosApi->add($dataProduto)['produtos_id'];
            } else {
                $produtoId = $produtoCheck;
            }

            $data['carrinho'][$i]['produto_id'] = $produtoId;
            $data['carrinho'][$i]['c_preco'] = $pedidoItem['valorunidade'];
            $data['carrinho'][$i]['c_qtd'] = $pedidoItem['quantidade'];
            $data['carrinho'][$i]['c_desc'] = $pedidoItem['descontoItem'];
            $data['carrinho'][$i]['c_prod'] = $pedidoItem['descricao'];
            $data['carrinho'][$i]['pos'] = 1;
            $data['carrinho'][$i]['c_tot'] = 0;
        }
        $faturamentosApi->add($data);
    }

    public function importarNotasFiscais(){

        $blingApi = new BlingApi();
        $empresasApi = new EmpresasApi();
        $cargasApi = new CargasApi();
        $clientesApi = new ClientesApi();
        $produtosApi = new ProdutosApi();
        $response = array();
        $empresas_data = $empresasApi->list('', '' , 0, 999999);
        $cargas = array();
        //Pedidos do Bling
        //Listas as Empresas
        $data_response = array();
        $filters = array();
        if($empresas_data['gotData']){
            for ($i=0; $i < sizeof($empresas_data['data']) ; $i++) {

                $empresas_id[$i] = $empresas_data['data'][$i]['empresas_id'];
                //Captura pedidos do Bling
                //Apenas empresas "Ativadas"
                if ($empresas_data['data'][$i]['erp_integracao'] == 'bling' && $empresas_data['data'][$i]['service_enabled'] == 1) {
                    //var_dump($empresas_data['data'][$i]);
                    if(!empty($empresas_data['data'][$i]['erp_integracao_chave_api'])){
    
                        $cargas_data = $cargasApi->list('',  'saida',  $empresas_id[$i], 'conferido',  0, 999999);
                        
                        if($cargas_data['gotData']){

                            for ($c=0; $c < sizeof($cargas_data['data']) ; $c++) { 
                                if($cargas_data['data'][$c]['origem'] == 'bling'){
                                    //Instancia Classe do Bling
                                    $blingApi = new BlingApi();
                                    //Defina a API KEY
                                    $blingApi->strApiKey = $empresas_data['data'][$i]['erp_integracao_chave_api'];

                                    $data_response = $blingApi->getOrder($cargas_data['data'][$c]['num_pedido']);

                                    if(isset($data_response['pedidos'])){
                                        //Pedidos0
                                        $pedidos = $data_response['pedidos'];
                                        for ($p=0; $p < sizeof($pedidos); $p++) { 
                                        // $pedidos[$p]["pedido"]['situacao'];
                                            //Verifica a chave loja do pedido e configura as padrões de importação
                                            if(isset( $pedidos[$p]["pedido"]['loja'])){
                                                //Busca a loja selecionada pelo id da loja que vem no pedido
                                                $loja_import_config = $this->getLojaByNumeroEmpresa( $pedidos[$p]["pedido"]['loja'],  $empresas_id[$i]);
                                            }else{
                                                //Caso não exista a a chave "loja" ela carrega uma configuração padrão
                                                //Loja Padrão Config
                                                $loja_import_config = $this->getLojaByNumeroEmpresa('0', $empresas_id[$i]);
                                            }   
                                            if($loja_import_config){
                                                //if($pedidos[$p]["pedido"]['situacao'] == $loja_import_config['situacao_apos_finalizar_text']){
                                                    if(isset($pedidos[$p]["pedido"]['nota'])){
                                                        if($pedidos[$p]["pedido"]['nota']['situacao'] == '6' || $pedidos[$p]["pedido"]['nota']['situacao'] == '7'){
                                                            //var_dump($pedidos[$p]["pedido"]);
                                                            $notas_fiscais = $blingApi->getNf($pedidos[$p]["pedido"]['nota']['numero'],$pedidos[$p]["pedido"]['nota']['serie']);
                                                            //var_dump($notas_fiscais['retorno']['notasfiscais']);
                                                            if(isset($notas_fiscais['retorno']['notasfiscais'])){
                                                                for ($n=0; $n < sizeof($notas_fiscais['retorno']['notasfiscais']); $n++) { 
                                                                    $response[] = $insert_nf = $cargasApi->insertNFCargas($notas_fiscais['retorno']['notasfiscais'][$n]['notafiscal'], $cargas_data['data'][$c]['cargas_id']);
                                                                }
                                                            }
                                                        }
                                                    }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }

    public function cancelarPedidos(){
        $callbackApi = new CallbackApi();
        $cargasApi = new CargasApi();
        $pedidos = $callbackApi->listarCallbacks($tipo = 'pedido', $origem = 'bling');
        $response = array();
        //return $pedidos;

            //Pedidos
            for ($p=0; $p < sizeof($pedidos); $p++) { 
                if($pedidos[$p]['situacao'] == 'Cancelado'){
                    //return $pedidos[$p];

                    if(isset($pedidos[$p]['loja'])){
                        //Busca a loja selecionada pelo id da loja que vem no pedido
                        $loja_import_config = $this->getLojaByNumeroEmpresa( $pedidos[$p]['loja'],  $pedidos[$p]['empresas_id']);
                    }else{
                        //Caso não exista a a chave "loja" ela carrega uma configuração padrão
                        //Loja Padrão Config
                        $loja_import_config = $this->getLojaByNumeroEmpresa('0', $pedidos[$p]['empresas_id']);
                    } 
                    //return $loja_import_config;

                    if($loja_import_config){

                      //Verifica se a carga não existe
                      $carga_exist = $cargasApi->getCargaByNumeroBling($pedidos[$p]['numero'], $loja_import_config['loja_id'], $pedidos[$p]['empresas_id']);
                        if( $carga_exist == true && $carga_exist[0]['situacao'] !== 'cancelado'){
                            //Muda a situação da Carga
                            $carga_update = $cargasApi->editSituacao('cancelado', $carga_exist[0]['cargas_id']);
                            //Disponibiliza os itens da carga
                            $cargasApi->disponilizaCargaItensEstoque($carga_exist[0]['cargas_id']);
                            
                            if($carga_update['status'] == 'success'){
                                $response[] =  $callbackApi->callbackVerify($pedidos[$p]['callback_id']);
                            }
                        }
                    }
                }
            }
        return $response;
    }

    // Importa Pedidos dos ERPs : Bling e (futuro erp aqui)
    public function pedidosByEmpresa($empresa_id)
    {
        $blingApi = new BlingApi();
        $empresasApi = new EmpresasApi();
        $clientesApi = new ClientesApi();
        $produtosApi = new ProdutosApi();

        $empresas_data = $empresasApi->getEmpresaById($empresa_id);
        $cargas = array();
        //Pedidos do Bling
        //Listas as Empresas
        $data_response = array();
        $filters = array();
        if($empresas_data['gotData']){
            for ($i=0; $i < sizeof($empresas_data['data']) ; $i++) {

                $empresas_id[$i] = $empresas_data['data'][$i]['empresas_id'];
                //Captura pedidos do Bling
                //Apenas empresas "Ativadas"
                if ($empresas_data['data'][$i]['erp_integracao'] == 'bling') {
                    //var_dump($empresas_data['data'][$i]);
                    if(!empty($empresas_data['data'][$i]['erp_integracao_chave_api'])){
                        
                        $blingApi = new BlingApi(); //Instancia Classe do Bling
                        //Defina a API KEY
                        $blingApi->strApiKey = $empresas_data['data'][$i]['erp_integracao_chave_api'];
                        $date_inicio = date("d/m/Y",strtotime("-1 days"));
                        $date_fim = date("d/m/Y");
                        //Filtro por Data
                        $filters['dataEmissao'] = " $date_inicio TO $date_fim";

                        /*
                        Pedidos de Venda:
                            6 = Em Aberto
                            9 = Atendido
                            12 = Cancelado
                            15 = Em andamento
                            18 = Venda Agenciada
                            21 = Em digitação
                            24 = Verificado 
                        */

                        //$filters['idSituacao'] = "9";

                        return $data_response = $blingApi->getOrders($filters);
                                //break;
                    // $lojas_list = $this->getLojaByNumeroEmpresa();
                    }
                }
                // Listas as lojas cadastradas dentro da Integração
            }
        }
        return $cargas;
        //return $data_response;
    }

}   