<?php

namespace App\Models\Integracoes;

use PDO;
use App\Models\Integracoes\ERPApi;
use App\Models\CargasApi;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class CallbackApi extends \App\Models\CrudInit
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
 
    public function receberCallbackPedidos(array $data, string $origem, int $empresas_id)
    {
        $data_insert['data'] = json_encode($data, true);
        $data_insert['origem'] = $origem;
        $data_insert['empresas_id'] = $empresas_id;
        $data_insert['tipo'] = 'pedido';
        $insert =  $this->insert('callbacks', $data_insert);
        return $insert;
    }

    public function listarCallbacks($tipo = false, $origem = false, $empresas_id = false)
    {   
        $pedidos = array();
        $conditions = array();
        if($tipo){
            $conditions['where']['tipo'] = $tipo;
        }

        if($origem){
            $conditions['where']['origem'] = $origem;
        }

        if($empresas_id){
            $conditions['where']['empresas_id'] = $empresas_id;
        }
        $conditions['where']['verificado'] = '0';

        $response =  $this->getRows('callbacks', $conditions);
        if($response['gotData']){
            if($tipo == 'pedido'){
                for ($i=0; $i <sizeof($response['data']) ; $i++) { 
                    $data = json_decode($response['data'][$i]['data'], true)['POST'];
                    if(isset($data['data'])){
                        $pedido_raw = json_decode($data['data'], true)['retorno']['pedidos'][0]['pedido'];
                        $pedido_raw['empresas_id'] = $response['data'][$i]['empresas_id'];
                        $pedido_raw['callback_id'] = $response['data'][$i]['callback_id'];
                        //var_dump($pedido_raw);
                        $pedidos[] =  $pedido_raw;
                    }
                }
                return $pedidos; 
            }                
        }
        return $response['gotData'];
    }

    public function callbackVerify($callback_id){
            //return $data;
            $response['status'] = 'error';
            $data['verificado'] = 1;
            $where_carga['callback_id'] = "$callback_id";
            $insert_carga =  $this->update('callbacks', $data, $where_carga);
            if($insert_carga){
                $response['status'] = 'success';
                $response['callback_id'] =  $callback_id;
                $response['status-message'] = 'O callback foi verificado com sucesso!';
            }else{
                $response['status-message'] = 'Occoreu um erro na verificação, tente novamente!';
            }
            return $response;
    }
}
