<?php
namespace App\Models\Integracoes;

use GuzzleHttp\Client;
use App\Models\EmpresasApi;
use App\Models\CargasApi;

class MeliApi extends \App\Models\CrudInit
{
	private $client_id = "2723412627759163";
	private $client_secret = "wggBE0jCdogB3bQjpYiY6CL87ntM4Opb";
	private $base_uri = "https://api.mercadolibre.com/";
	private $redirect_uri = "https://fleischer.lnxweb.com.br/auth/meli/finish";
    

    public function verifyHashIntegracao(string $hash){
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['meli_auth_hash'] = $hash;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $empresa_data =  $this->getRows('empresas', $conditions);
        if($empresa_data['gotData']){
            $response = $empresa_data['data'][0];
            $_SESSION['empresas_id'] = $response['empresas_id'];
            $response['url_allow_meli_wms'] =  "http://auth.mercadolivre.com.br/authorization?response_type=code&client_id=".$this->client_id."&redirect_uri=". $this->redirect_uri;
            $response['data'] = $empresa_data['gotData'];
        }else{
            $response['data'] = $empresa_data['gotData'];
        }
        return $response;
    }

    public function finishIntegracao(string $code, int $empresas_id){

        $authToGetRefreshToken = $this->authToGetRefreshToken($code);

        $where_empresa['empresas_id'] = "$empresas_id";
        $data['codeauth_meli'] = $code;
        $data['refreshtoken_meli'] = $authToGetRefreshToken['refresh_token'];
        $data['access_token_meli'] = $authToGetRefreshToken['access_token'];
        $data['meli_integracao'] = 1;
        $update_empresa =  $this->update('empresas', $data, $where_empresa); 
        $response['data'] = $update_empresa;
        return $response;
    }

    public function generateURLAuth($empresas_id){

        $empresasapi = new EmpresasApi();
        $response['status'] = 'error';
        //Verifica se a empresa existe e já existe hash de integração
        $empresa_data = $empresasapi->getEmpresaById($empresas_id);
        if($empresa_data['gotData']){
            $response['status'] = 'success';
            if($empresa_data['data'][0]['meli_auth_hash'] !== NULL){
                $response['status-message'] = "A empresa já possui a url de integração e esta pronta para uso.";
                $response['meli_auth_hash'] = $empresa_data['data'][0]['meli_auth_hash'];
                $response['url_meli_integracao'] =  "https://" .  $_SERVER['HTTP_HOST'] . "/auth/meli?hash=" . $response['meli_auth_hash'];

            }else{
                $data['meli_auth_hash'] = sha1(uniqid( mt_rand(), true));
                $where_empresa['empresas_id'] = "$empresas_id";
                $update_empresa =  $this->update('empresas', $data, $where_empresa);
                $response['status-message'] = "A url foi gerada com sucesso e a esta pronta para uso.";
                $response['meli_auth_hash'] = $data['meli_auth_hash'];
                $response['url_meli_integracao'] =  "https://" .  $_SERVER['HTTP_HOST'] . "/auth/meli?hash=" .$data['meli_auth_hash'];

            }
            
        }else{

            $response['status-message'] = "Nenhum dado encontrado.";
        }
        return $response;
    }

    public function authToGetRefreshToken($SERVER_GENERATED_AUTHORIZATION_CODE){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_uri,
            // You can set any number of default request options.
            'timeout'  => 30,
        ]);

        $response = $client->request('POST', 'oauth/token', [
            'headers' => [
                'accept' => 'application/json',
                'content-type:' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'redirect_uri' => $this->redirect_uri,
                'code' => $SERVER_GENERATED_AUTHORIZATION_CODE,
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }


    public function getRefreshToken($refresh_token){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_uri,
            // You can set any number of default request options.
            'timeout'  => 30,
        ]);

        $response = $client->request('POST', 'oauth/token', [
            'headers' => [
                'accept' => 'application/json',
                'content-type:' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'grant_type' => 'refresh_token',
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'refresh_token' => $refresh_token,
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getOrder($order_id, $access_token){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_uri,
            // You can set any number of default request options.
            'timeout'  => 30,
        ]);

        $response = $client->request('GET', "orders/$order_id", [
            'headers' => [
                'accept' => 'application/json',
                'Authorization' => "Bearer $access_token"
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);

    }

    public function getShipmentStatus($shipping_id, $access_token){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_uri,
            // You can set any number of default request options.
            'timeout'  => 30,
        ]);

        $response = $client->request('GET', "shipments/$shipping_id", [
            'headers' => [
                'accept' => 'application/json',
                'Authorization' => "Bearer $access_token"
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);

    }

    public function atualizaAccessTokens(){
        $empresasApi = new EmpresasApi();
        $response = array();
        $empresas_data = $empresasApi->list('', '' , 0, 999999);
        //Pedidos do Bling
        //Listas as Empresas
        $data_response = array();
        $filters = array();
        if($empresas_data['gotData']){
            for ($i=0; $i < sizeof($empresas_data['data']) ; $i++) {
                $empresas_id[$i] = $empresas_data['data'][$i]['empresas_id'];
                
                //Apenas empresas "Ativadas" com integração com o Meli
                if ($empresas_data['data'][$i]['meli_integracao'] == 1 && $empresas_data['data'][$i]['refreshtoken_meli'] !== NULL) {
                    //Atualiza Access Token das Empresas com integração ao Meli
                    $getRefreshToken  = $this->getRefreshToken($empresas_data['data'][$i]['refreshtoken_meli']);
                    
                    $where_update['empresas_id'] = $empresas_data['data'][$i]['empresas_id'];
                    $data_update['access_token_meli'] = $getRefreshToken['access_token'];
                    //Response
                    $response[$i]['status'] = $this->update('empresas', $data_update, $where_update);
                    $response[$i]['empresas_id'] = $empresas_data['data'][$i]['empresas_id'];
                }
            }
        }
        return $response;
    }

    public function getEtiqueta($SHIPPING_ID, $access_token, $suffix_file = ''){
        $path = "uploads/etiqueta/etiqueta_ML_$SHIPPING_ID-$suffix_file.zip";
        $dir = "uploads/etiqueta/etiqueta_ML_$SHIPPING_ID-$suffix_file/";
        $zpl = $dir . "Etiqueta de envio.txt";


        if(!file_exists($zpl)){
            //Baixa Pacote Zip de Etiquetas do Meli em formato ZPL2 
            $pdf_path = fopen($path, 'w+');
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $this->base_uri,
                // You can set any number of default request options.
                'timeout'  => 30,
            ]);
            //$url = "users/me";
            $url = "shipment_labels?shipment_ids=$SHIPPING_ID&response_type=zpl2";
            $response = $client->request('GET',$url, [
                'headers' => [
                    'Authorization' => "Bearer $access_token"
                ],
                'sink' => $pdf_path,
            ]);

            // Descompacta o Zip e transforma o Arquivo ZPL2(TXT) em Pdf pelo API
            $zipArchive = new \ZipArchive();
            $result = $zipArchive->open($path);
            if ($result === TRUE) {
                $zipArchive ->extractTo($dir);
                $zipArchive ->close();
                // Do something else on success
                $zplToPdf = $this->zplToPdf($zpl, $dir);
                return $zplToPdf;
            }else{
                return false;
            }

        }else{
            return false;
        }

    }

    public function zplToPdf($zpl_path, $dir){
        //$zpl = "^xa^cfa,50^fo100,100^fdHello World^fs^xz";
        //$zpl_array= file_get_contents($zpl_path, true);
        //$zpl = "";
        $zpl_array = file($zpl_path);
        $zpl = '';
        foreach ($zpl_array as $line) {
            $zpl .= $line;
        }
        //var_dump($zpl);
        //return false;

        $curl = curl_init();
        // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
        curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/");
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $zpl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
        $result = curl_exec($curl);

        if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            $pdf_path = $dir . "etiqueta.pdf";
            $file = fopen($pdf_path, "w"); // change file name for PNG images
            fwrite($file, $result);
            fclose($file);
            $response =  $pdf_path;
        } else {
            print_r("Error: $result");
            $response =  false;
        }

        curl_close($curl);
        return $response;
    }
    
    public function importarEtiquetasMeli(){
        $empresasApi = new EmpresasApi();
        $cargasApi = new CargasApi();
        $response = array();
        $empresas_data = $empresasApi->list('', '' , 0, 999999);
        $cargas = array();
        //Listas as Empresas
        $data_response = array();
        $filters = array();
        if($empresas_data['gotData']){
            for ($i=0; $i < sizeof($empresas_data['data']) ; $i++) {

                $empresas_id[$i] = $empresas_data['data'][$i]['empresas_id'];
                //Apenas empresas "Ativadas" que tem integraçãom com o Meli
                if ($empresas_data['data'][$i]['meli_integracao'] == 1 && $empresas_data['data'][$i]['refreshtoken_meli'] !== NULL) {

                    //AccessToken Meli
                    $access_token = $empresas_data['data'][$i]['access_token_meli'];
                    
                    //Lista as Cargas
                    $cargas_data = $cargasApi->list('',  'saida',  $empresas_id[$i], 'conferido',  0, 999999);
                    if($cargas_data['gotData']){

                        for ($c=0; $c < sizeof($cargas_data['data']) ; $c++) { 
                            if($cargas_data['data'][$c]['tipoIntegracao'] == 'MercadoLivre'){

                                $order_id = $cargas_data['data'][$c]['numeroPedidoLoja'];
                                $cargas_id = $cargas_data['data'][$c]['cargas_id'];
                                //var_dump($cargas_id);
                                //Dados do Pedido Meli
                                $data_order  = $this->getOrder($order_id ,$access_token);
                                //var_dump($data_order);
                                $data_shipping = $data_order['shipping'];
                                //Lista os shipping_ids
                                //Salva as Etiquetas no Sistema
                                foreach ($data_shipping as $key => $shipping_id) {

                                    //Checar Status do Shipment
                                    $shipment_data = $this->getShipmentStatus($shipping_id, $access_token);
                                    //var_dump($shipment_status);

                                    if($shipment_data['status'] == "ready_to_ship"){
                                        if($shipment_data['substatus'] == "ready_to_print" || $shipment_data['substatus'] == "printed" ){
                                            ///Baixar e Salvar Etiqueta em PDF
                                            $response['cargas_ready_to_ship'][] = $cargas_id;

                                            $file_path = $this->getEtiqueta($shipping_id, $access_token, $cargas_id);
                                            if($file_path){
                                                $data_insert['file_path'] = $file_path;
                                                $data_insert['cargas_id'] = $cargas_id;
                                                $response[] = $this->insert('cargas_etiquetas', $data_insert);
                                                $response['cargas_etq'][] = $cargas_id;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $response;
    }
}
