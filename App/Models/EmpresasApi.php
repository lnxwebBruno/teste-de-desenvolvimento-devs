<?php

namespace App\Models;

use PDO;
use filters;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class EmpresasApi extends Cadastro\PessoaJuridica
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $nome_fantasia = "", string $cnpj = "", int $offset = 0, int $limit = 15)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'empresas_id DESC';
        //Collums
        $conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        $conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('empresas', $conditions);
        return $response;
    }
    //Select2 Method GET
    public function simpleList(string $query = "", int $offset = 0, int $limit = 999)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'empresas_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, empresas_id AS id,  UPPER(nome_fantasia) AS text, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $conditions['custom_where_query'] = "WHERE (nome_fantasia LIKE '%$query%' OR cnpj LIKE '%$query%') AND enabled = '1'";
        $response =  $this->getRows('empresas', $conditions);
        return $response;
    }


    public function getEmpresaById(int $empresas_id, $show_disabled = false)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        if($show_disabled){
            unset($conditions['where']['enabled']);
        }
        //Collums
        $conditions['where']['empresas_id'] = $empresas_id;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('empresas', $conditions);

        return $response;
    }

    public function getEmpresaByCNPJ(string $cnpj)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['cnpj'] = $cnpj;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('empresas', $conditions);
        return $response;
    }


    public function add(array $data)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }

        extract($data);
        
        if (!$this->isRazaoSocialValid($razao_social)) {
            $response['status-message'] = 'Preencha a razão social da empresa.';
            return $response;
        }

        if (!$this->isNomeFantasiaValid($nome_fantasia)) {
            $response['status-message'] = 'Preencha o nome fantasia da empresa.';
            return $response;
        }


        if (!$this->isCNPJValidFilter($cnpj)['valid'])
		{
            $response['status-message'] = 'Digite um CNPJ válido.';
            return $response;
        }else{
            $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
        }

        if ($this->isEmpresaCNPJExist($cnpj))
		{
            $response['status-message'] = 'Este CNPJ já está cadastrado';
            return $response;
        }

        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }
        if (!$this->isCepValid($cep))
		{
            $response['status-message'] = 'Digite um cep válido.';
            return $response;
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }
       

        $insert =  $this->insert('empresas', $data);

        if($insert){
            $response['status'] = 'success';
            $response['status-message'] = 'A empresa foi registrada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function edit(array $data, int $empresas_id)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }

 
        
        extract($data);


        if (!$this->isRazaoSocialValid($razao_social)) {
            $response['status-message'] = 'Preencha a razão social da empresa.';
            return $response;
        }

        if (!$this->isNomeFantasiaValid($nome_fantasia)) {
            $response['status-message'] = 'Preencha o nome fantasia da empresa.';
            return $response;
        }


        if (!$this->isCNPJValidFilter($cnpj)['valid'])
		{
            $response['status-message'] = 'Digite um CNPJ válido.';
            return $response;
        }else{
            $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
        }

        
       // var_dump($this->isEmpresaCNPJExist($cnpj));
        if($cnpj !== $this->getEmpresaById($empresas_id)['data'][0]['cnpj']){

            if ($this->isEmpresaCNPJExist($cnpj))
            {
                $response['status-message'] = 'Este novo CNPJ já está cadastrado';
                return $response;
            }
        }

        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }

        if (!$this->isCEPValidFilter($cep)['valid'])
		{
            $response['status-message'] = 'Digite um CEP válido.';
            return $response;
        }else{
            $cep = $data['cep'] = $this->isCEPValidFilter($cep)['cep'];
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }

        if (!$this->isEmailValid($email_relatorios))
		{
            $response['status-message'] = 'Digite um email válido para relatórios.';
            return $response;
        }
        
        $where['empresas_id'] = $empresas_id;
        $update =  $this->update('empresas', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A empresa foi atualizada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function add_marketplace(array $data, int $empresas_id) {
        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        if (isset($data['api_key']) and $data['api_key'] == "") {
            unset($data['api_key']);
        }
        extract($data);

        $insert =  $this->insert('empresas_lojas', $data);

        if($insert){
            $response['status'] = 'success';
            $response['id_loja'] = $insert;
            $response['status-message'] = 'A loja foi registrada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function edit_marketplace(array $data, int $empresas_lojas_id) {
        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        $where['empresas_lojas_id'] = $empresas_lojas_id;
        $insert =  $this->update('empresas_lojas', $data, $where);

        if($insert){
            $response['status'] = 'success';
            $response['status-message'] = 'A loja foi registrada com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function disable_marketplace(int $empresas_lojas_id)
    {
        $response['status'] = 'error';

        if (!$this->isEmpresaLojaIdValid($empresas_lojas_id))
		{
            $response['status-message'] = 'A loja não existe, ou já foi deletada';
            return $response;
        }
        
        $where['empresas_lojas_id'] = $empresas_lojas_id;
        $data['enabled'] = '0';
        $update =  $this->update('empresas_lojas', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A loja foi deletada com sucesso!';
            return $response;
        }
    }

    public function disable(int $empresas_id)
    {
        $response['status'] = 'error';

        if (!$this->isEmpresaIdValid($empresas_id))
		{
            $response['status-message'] = 'A empresa não existe, ou já foi deletada';
            return $response;
        }
        
        $where['empresas_id'] = $empresas_id;
        $data['enabled'] = '0';
        $update =  $this->update('empresas', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A empresa foi deletada com sucesso!';
            return $response;
        }
    }

    public function uploadPhoto(int $empresas_lohjas_id, $file){

        if(!is_dir("uploads/lojas/$empresas_id")){

            if(mkdir("uploads/lojas/$empresas_id")){
                //echo "created;";
            } 
        }

        $target_path = "uploads/lojas/$empresas_lojas_id/" . md5(uniqid(rand(), true)) . '_' . $file['name'][0];
        $data['image_path'] = $target_path;
        //$status['md5'] = md5_file($file_tmp_name);
        if(move_uploaded_file($file['tmp_name'][0], $target_path)){
            $where['empresas_lojas_id'] = $empresas_lojas_id;
            $update = $this->update('empresas', $data, $where);


            $response['status'] = 'success';
            $response['status-message'] = 'Os dados foram inseridos com sucesso';

        }else{
            $response['status'] = 'error';
            $response['status-message'] = 'Occoreu um erro, entre em contato com o administrador.';
        }
        return $response;
    }

    /* Verifica se a empresa existe */
    public function isEmpresaIdValid(int $empresas_id): bool
    {
        $conditions['where']['empresas_id'] = $empresas_id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('empresas', $conditions);
        return $response['gotData'];
    }
    
    public function isEmpresaLojaIdValid(int $empresas_id): bool
    {
        $conditions['where']['empresas_lojas_id'] = $empresas_id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('empresas_lojas', $conditions);
        return $response['gotData'];
    }

    /* Verifica se a empresa pelo CNPJ */
    public function isEmpresaCNPJExist(string $cnpj):bool
    {
        $conditions['where']['cnpj'] = $cnpj;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('empresas', $conditions);
        return $response['gotData'];
    }

    public function getMarketplaces() {
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response = $this->getRows('marketplaces', $conditions);
        return $response['data'];
    }

}
