<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class FornecedoresApi extends Cadastro\PessoaJuridica
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $nome_fantasia = "", string $cnpj = "", int $offset = 0, int $limit = 15)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'fornecedores_id DESC';
        //Collums
        $conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        $conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('fornecedores', $conditions);
        return $response;
    }
    public function simpleList(string $query = "", int $offset = 0, int $limit = 999)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'fornecedores_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, fornecedores_id AS id,  UPPER(nome_fantasia) AS text, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $conditions['custom_where_query'] = "WHERE (nome_fantasia LIKE '%$query%') OR (cnpj LIKE '%$query%') AND enabled = '1'";
        $response =  $this->getRows('fornecedores', $conditions);
        return $response;
    }

    public function getFornecedorById(int $fornecedores_id)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['fornecedores_id'] = $fornecedores_id;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('fornecedores', $conditions);
        return $response;
    }

    public function getFornecedorByCNPJ(string $cnpj)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['cnpj'] = $cnpj;
        //Mount Query
        $conditions['select'] = '*, UPPER(nome_fantasia) AS nome_fantasia, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('fornecedores', $conditions);
        return $response;
    }


    public function add(array $data)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);


        if (!$this->isRazaoSocialValid($razao_social)) {
            $response['status-message'] = 'Preencha a razão social do fornecedor.';
            return $response;
        }

        if (!$this->isNomeFantasiaValid($nome_fantasia)) {
            $response['status-message'] = 'Preencha o nome fantasia do fornecedor.';
            return $response;
        }


        if (!$this->isCNPJValidFilter($cnpj)['valid'])
		{
            $response['status-message'] = 'Digite um CNPJ válido.';
            return $response;
        }else{
            $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
        }

        if ($this->isFornecedorCNPJExist($cnpj))
		{
            $response['status-message'] = 'Este CNPJ já está cadastrado';
            return $response;
        }

        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }
        if (!$this->isCepValid($cep))
		{
            $response['status-message'] = 'Digite um cep válido.';
            return $response;
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }

        $insert =  $this->insert('fornecedores', $data);

        if($insert){
            $response['status'] = 'success';
            $response['status-message'] = 'O fornecedor foi registrado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function edit(array $data, int $fornecedores_id)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);


        if (!$this->isRazaoSocialValid($razao_social)) {
            $response['status-message'] = 'Preencha a razão social do fornecedor.';
            return $response;
        }

        if (!$this->isNomeFantasiaValid($nome_fantasia)) {
            $response['status-message'] = 'Preencha o nome fantasia do fornecedor.';
            return $response;
        }


        if (!$this->isCNPJValidFilter($cnpj)['valid'])
		{
            $response['status-message'] = 'Digite um CNPJ válido.';
            return $response;
        }else{
            $cnpj = $data['cnpj'] = $this->isCNPJValidFilter($cnpj)['cnpj'];
        }

        
       // var_dump($this->isFornecedorCNPJExist($cnpj));
        if($cnpj !== $this->getFornecedorById($fornecedores_id)['data'][0]['cnpj']){

            if ($this->isFornecedorCNPJExist($cnpj))
            {
                $response['status-message'] = 'Este novo CNPJ já está cadastrado';
                return $response;
            }
        }

        if (!$this->isEnderecoValid($endereco))
		{
            $response['status-message'] = 'Digite um endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoNumeroValid($endereco_n))
		{
            $response['status-message'] = 'Digite um número de endereço válido.';
            return $response;
        }

        if (!$this->isEnderecoComplementoValid($endereco_complemento))
		{
            $response['status-message'] = 'Digite um complemento válido.';
            return $response;
        }

        if (!$this->isEnderecoBairroValid($endereco_bairro))
		{
            $response['status-message'] = 'Digite um bairro válido.';
            return $response;
        }

        if (!$this->isCidadeValid($cidade))
		{
            $response['status-message'] = 'Digite um cidade válido.';
            return $response;
        }

        if (!$this->isUFValid($uf))
		{
            $response['status-message'] = 'Digite um uf válido.';
            return $response;
        }

        if (!$this->isCEPValidFilter($cep)['valid'])
		{
            $response['status-message'] = 'Digite um CEP válido.';
            return $response;
        }else{
            $cep = $data['cep'] = $this->isCEPValidFilter($cep)['cep'];
        }

        if (!$this->isTelefoneValidFilter($telefone)['valid'])
		{
            $response['status-message'] = 'Digite um telefone válido.';
            return $response;
        }else{
            $telefone = $data['telefone'] = $this->isTelefoneValidFilter($telefone)['telefone'];
        }

        if (!$this->isCelularValidFilter($celular)['valid'])
		{
            $response['status-message'] = 'Digite um celular válido.';
            return $response;
        }else{
            $celular = $data['celular'] = $this->isCelularValidFilter($celular)['celular'];
        }

        if (!$this->isEmailValid($email))
		{
            $response['status-message'] = 'Digite um email válido.';
            return $response;
        }

        $where['fornecedores_id'] = $fornecedores_id;
        $update =  $this->update('fornecedores', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O fornecedor foi atualizado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }


    public function disable(int $fornecedores_id)
    {
        $response['status'] = 'error';

        if (!$this->isFornecedorIdValid($fornecedores_id))
		{
            $response['status-message'] = 'O fornecedor não existe, ou já foi deletado';
            return $response;
        }
        
        $where['fornecedores_id'] = $fornecedores_id;
        $data['enabled'] = '0';
        $update =  $this->update('fornecedores', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O fornecedor foi deletado com sucesso!';
            return $response;
        }
    }

    /* Verifica se o fornecedor existe */
    public function isFornecedorIdValid(int $fornecedores_id): bool
    {
        $conditions['where']['fornecedores_id'] = $fornecedores_id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('fornecedores', $conditions);
        return $response['gotData'];
    }

    /* Verifica se O fornecedor pelo CNPJ */
    public function isFornecedorCNPJExist(string $cnpj):bool
    {
        $conditions['where']['cnpj'] = $cnpj;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('fornecedores', $conditions);
        return $response['gotData'];
    }

}
