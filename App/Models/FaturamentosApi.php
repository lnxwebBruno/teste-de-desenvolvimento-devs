<?php

namespace App\Models;

use PDO;
use App\Models\ClientesApi;
use App\Models\VendedoresApi;
use App\Models\TransportadorasApi;
use App\Models\ProdutosApi;
//use App\Models\Cadastro\PessoaFisica;
include_once("filters.php");
/**
 * Example user model
 *
 * PHP version 7.0
 */
class FaturamentosApi extends CrudInit
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $nome_cliente = "", string $martketplace_id = "", string $empresas_id = "", string $data_inicio = "", string $data_fim = "", int $offset = 0, int $limit = 15) {
        //Config
        $faturamentos_id = null;
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['order_by'] = 'faturamentos_id DESC';
        //Collums
        //if($faturamentos_id){
        //$conditions['where']['faturamentos_id'] = "%$faturamentos_id%";
        //}
        //$conditions['where']['tipo'] = "%$tipo%";
        //$conditions['where']['enabled'] = "1";

        //Mount Query
        $wempresas = $empresas_id != "" && !is_null($empresas_id) ? " AND faturamentos.empresas_id='$empresas_id' " : "";
        $wcliente = $nome_cliente != "" && !is_null($nome_cliente) ? " AND (clientes.nome LIKE '%$nome_cliente%' OR clientes.nome_fantasia LIKE '%$nome_cliente%')" : "";
        //$wvendedor = $nome_vendedor != "" && !is_null($nome_vendedor) ? " AND vendedores.nome LIKE '%$nome_vendedor%'" : "";
        $wmarketplace = $martketplace_id != "" && !is_null($martketplace_id) ? " AND marketplaces.marketplace_id = '$martketplace_id'" : "";
        $wid = $faturamentos_id != "" && !is_null($faturamentos_id) ? " AND faturamentos.faturamentos_id='$faturamentos_id' " : "";
        $wdata = "";
        if (!is_null($data_inicio) && !is_null($data_fim) && $data_inicio != "" && $data_fim != "") {
            $wdata = " AND faturamentos.data_cadastro BETWEEN '$data_inicio' AND '$data_fim' ";
        }
        $conditions['custom_where_query'] = "INNER JOIN (clientes) ON faturamentos.clientes_id = clientes.clientes_id INNER JOIN (empresas) ON faturamentos.empresas_id = empresas.empresas_id  INNER JOIN (marketplaces) ON faturamentos.marketplace_id = marketplaces.marketplace_id WHERE faturamentos.enabled = '1'" . $wid. $wcliente . $wmarketplace . $wempresas . $wdata;
        $conditions['select'] = 'faturamentos.*,  UPPER(empresas.nome_fantasia) AS empresa_nome_fantasia, COALESCE(clientes.nome, clientes.nome_fantasia) as nome_cliente, marketplaces.nome as nome_martkeplace, DATE_FORMAT(faturamentos.data_cadastro,"%d/%m/%Y") AS data_cadastro, DATE_FORMAT(faturamentos.created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(faturamentos.modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        //$conditions['select'] = 'faturamentos.*, COALESCE(clientes.nome, clientes.nome_fantasia) as nome_cliente, vendedores.nome as nome_vendedor, DATE_FORMAT(faturamentos.data_cadastro,"%d/%m/%Y") AS data_cadastro, DATE_FORMAT(faturamentos.created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(faturamentos.modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('faturamentos', $conditions);
        for ($i = 0; $i < count($response['data']); $i++) {
            $total = $this->getOrderTotal($response['data'][$i]['faturamentos_id']);
            $response['data'][$i]['total'] = $total;
        }
        return $response;
    }


    public function getFaturamentosByClienteId(int $clientes_id, int $offset = 0, int $limit = 15) {
        //Config
        $faturamentos_id = null;
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['order_by'] = 'faturamentos_id DESC';
        //Collums
        //if($faturamentos_id){
        //$conditions['where']['faturamentos_id'] = "%$faturamentos_id%";
        //}
        //$conditions['where']['tipo'] = "%$tipo%";
        //$conditions['where']['enabled'] = "1";

        //Mount Query
        $conditions['custom_where_query'] = "INNER JOIN (clientes) ON faturamentos.clientes_id = clientes.clientes_id INNER JOIN (empresas) ON faturamentos.empresas_id = empresas.empresas_id  INNER JOIN (marketplaces) ON faturamentos.marketplace_id = marketplaces.marketplace_id WHERE faturamentos.enabled = '1' AND faturamentos.clientes_id=$clientes_id";
        $conditions['select'] = 'faturamentos.*,  UPPER(empresas.nome_fantasia) AS empresa_nome_fantasia, COALESCE(clientes.nome, clientes.nome_fantasia) as nome_cliente, marketplaces.nome as nome_martkeplace, DATE_FORMAT(faturamentos.data_cadastro,"%d/%m/%Y") AS data_cadastro, DATE_FORMAT(faturamentos.created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(faturamentos.modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        //$conditions['select'] = 'faturamentos.*, COALESCE(clientes.nome, clientes.nome_fantasia) as nome_cliente, vendedores.nome as nome_vendedor, DATE_FORMAT(faturamentos.data_cadastro,"%d/%m/%Y") AS data_cadastro, DATE_FORMAT(faturamentos.created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(faturamentos.modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('faturamentos', $conditions);
        for ($i = 0; $i < count($response['data']); $i++) {
            $total = $this->getOrderTotal($response['data'][$i]['faturamentos_id']);
            $response['data'][$i]['total'] = $total;
        }
        return $response;
    }

    public function getOrderTotal($faturamentos_id) {
        $conditions['return_type'] = 'all';
        $conditions['where']['faturamentos_id'] = "%$faturamentos_id%";
        $conditions['where']['enabled'] = "1";
        $conditions['select'] = 'c_tot';
        $response =  $this->getRows('faturamentos_carrinho', $conditions);
        $total = 0.0;
        for ($i = 0; $i < count($response['data']); $i++){
            $total += floatval($response['data'][$i]['c_tot']);
        }
        return $total;
    }

    public function simpleList(string $query = "", int $offset = 0, int $limit = 999) {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'faturamentos_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";

        //Mount Query
        $conditions['select'] = '*, faturamentos_id AS id, UPPER(descricao) AS text, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        //$conditions['custom_where_query'] = "WHERE (nome LIKE '%$query$%') AND (cpf LIKE '%$query%') AND enabled = '1'";
        $response =  $this->getRows('faturamentos', $conditions);
        return $response;
    }

    public function add(array $data) {
        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }

        extract($data);

        unset($data['carrinho']);
        unset($data['financeiro']);

        for ($i = 0; $i < count($carrinho); $i++) {
            $carrinho[$i]['c_preco'] = floatval($carrinho[$i]['c_preco']);
            $carrinho[$i]['c_desc'] = $carrinho[$i]['c_desc'] == "" ? 0 : floatval($carrinho[$i]['c_desc']);
            $carrinho[$i]['c_tot'] = (floatval($carrinho[$i]['c_preco']) * floatval($carrinho[$i]['c_qtd']));
            //$carrinho[$i]['c_tot'] = (floatval($carrinho[$i]['c_preco']) * floatval($carrinho[$i]['c_qtd'])) - floatval($carrinho[$i]['c_desc']);
        }

        for ($i = 0; $i < count($financeiro); $i++) {
            $financeiro[$i]['p_valor'] = floatval($financeiro[$i]['p_valor']);
            $financeiro[$i]['p_recebido'] = $financeiro[$i]['p_recebido'] == "" ? 0 : floatval($financeiro[$i]['p_recebido']);
            $financeiro[$i]['parcela'] = strval($i+1) . "/" . strval(count($financeiro));
        }

        $insert =  $this->insert('faturamentos', $data);
        //echo "<pre>", var_dump($carrinho), "</pre>"; die;

        if($insert){
            $response1 = $this->addCarrinho($carrinho, $insert);
            $response2 = $this->addFinanceiro($financeiro, $insert);
            if ($response1 && $response2) {
                $response['status'] = 'success';
                $response['status-message'] = 'O faturamento foi registrado com sucesso!';
            }
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function addCarrinho(array $data, $id) {
        

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['faturamentos_id'] = $id;
            $insert =  $this->insert('faturamentos_carrinho', $data[$i]);
            if($insert){
                $response['status'] = 'success';
                $response['status-message'] = 'O faturamento foi registrado com sucesso!';
            }else{
                $response['status'] = 'error';
                $response['status-message'] = 'Occoreu um erro na inserção (carrinho), tente novamente.!';
                return $response;
            }
        }
        
        return $response;
    }

    public function getCarrinho(int $faturamentos_id=NULL) {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        if (!is_null($faturamentos_id)) {
            $conditions['where']['faturamentos_id'] = $faturamentos_id;
        }
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('faturamentos_carrinho', $conditions);
        return $response;
    }

    public function addFinanceiro(array $data, $id) {
        

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['faturamentos_id'] = $id;
            $insert =  $this->insert('faturamentos_financeiro', $data[$i]);
            if($insert){
                $response['status'] = 'success';
                $response['status-message'] = 'O faturamento foi registrado com sucesso!';
            }else{
                $response['status'] = 'error';
                $response['status-message'] = 'Occoreu um erro na inserção (financeiro), tente novamente.!';
                return $response;
            }
        }
        
        return $response;
    }

    public function getFinanceiro(int $faturamentos_id=NULL) {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        if (!is_null($faturamentos_id)) {
            $conditions['where']['faturamentos_id'] = $faturamentos_id;
        }
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified, DATE_FORMAT(p_data,"%d/%m/%Y") AS data_vencimento';
        $response =  $this->getRows('faturamentos_financeiro', $conditions);
        return $response;
    }

    public function edit(array $data, int $faturamentos_id) {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        if (isset($data['cons_final']) and $data['cons_final'] == "on") {
            $data["cons_final"] = "1";
        } else {
            $data["cons_final"] = "0";
        }

        //var_dump($_POST, $data);

        $carrinho = isset($data['carrinho']) ? $data['carrinho'] : array();
        $financeiro = isset($data['financeiro']) ? $data['financeiro'] : array();
        unset($data['carrinho']);
        unset($data['financeiro']);

        $where['faturamentos_id'] = $faturamentos_id;
        $update =  $this->update('faturamentos', $data, $where);
        
        if($update){
            $this->updateCarrinho($carrinho, $faturamentos_id);
            $this->updateFinanceiro($financeiro, $faturamentos_id);
            $response['status'] = 'success';
            $response['status-message'] = 'O faturamento foi atualizado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na atualização, tente novamente.!';
        }
        return $response;
    }

    public function updateCarrinho(array $data, $id) {
        $old_data = $this->getCarrinho($id)['data'];

        $old_ids = array();
        $new_ids = array();

        for ($i = 0; $i < count($old_data); $i++) {
            array_push($old_ids, $old_data[$i]['faturamentos_carrinho_id']);
        }
        for ($i = 0; $i < count($data); $i++) {
            array_push($new_ids, $data[$i]['faturamentos_carrinho_id']);
        }

        $remover = array_diff($old_ids, $new_ids);
        $atualizar = array_diff($old_ids, $remover);

        // |---------------------|
        // | Loop de atualização |
        // |---------------------|
        for ($i = 0; $i < count($data); $i++) {
            //Ajuste de valores
            $data[$i]['c_preco'] = toEnglishDecimal($data[$i]['c_preco']);
            $data[$i]['c_desc'] = toEnglishDecimal($data[$i]['c_desc']);
            $data[$i]['c_tot'] = (floatval($data[$i]['c_preco']) * floatval($data[$i]['c_qtd'])) - floatval($data[$i]['c_desc']);

            //Condição 1: se o registro já existir
            if (isset($data[$i]['faturamentos_carrinho_id']) && $data[$i]['faturamentos_carrinho_id'] != "") {
                $where['faturamentos_carrinho_id'] = $data[$i]['faturamentos_carrinho_id'];
                $update = $this->update('faturamentos_carrinho', $data[$i], $where);
                if (!$update) {
                    $response['status'] = 'error';
                    $response['status-message'] = "Ocorreu um erro ao cadastrar";
                    return $response;
                }
            //Condição 2: se o registro não existir
            } else {
                unset($data[$i]['faturamentos_carrinho_id']);
                $d = array(0 => $data[$i]);
                $update = $this->addCarrinho($d, $id);
                if (!$update) {
                    $response['status'] = 'error';
                    $response['status-message'] = "Ocorreu um erro ao cadastrar";
                    return $response;
                }
            }
        }

        // |---------------------|
        // |   Loop de remoção   |
        // |---------------------|
        for ($i = 0; $i < count($old_data); $i++) {
            if (in_array($old_data[$i]['faturamentos_carrinho_id'], $remover)) {
                $update = $this->disableCarrinho($old_data[$i]['faturamentos_carrinho_id']);
                if (!$update) {
                    $response['status'] = 'error';
                    $response['status-message'] = "Ocorreu um erro ao excluir a parcela";
                    return $response;
                }
            }
        }
        
        $response['status'] = 'success';
        $response['status-message'] = 'O faturamento foi atualizado com sucesso!';

        return $response;
    }

    public function updateFinanceiro(array $data, $id) {
        $old_data = $this->getFinanceiro($id)['data'];

        $old_ids = array();
        $new_ids = array();

        for ($i = 0; $i < count($old_data); $i++) {
            array_push($old_ids, $old_data[$i]['faturamentos_financeiro_id']);
        }
        for ($i = 0; $i < count($data); $i++) {
            array_push($new_ids, $data[$i]['faturamentos_financeiro_id']);
        }

        $remover = array_diff($old_ids, $new_ids);
        $atualizar = array_diff($old_ids, $remover);

        // |---------------------|
        // | Loop de atualização |
        // |---------------------|
        for ($i = 0; $i < count($data); $i++) {
            //Ajuste de valores
            $data[$i]['p_valor'] = toEnglishDecimal($data[$i]['p_valor']);
            $data[$i]['p_recebido'] = toEnglishDecimal($data[$i]['p_recebido']);
            $data[$i]['parcela'] = strval($i+1) . "/" . strval(count($data));

            //Condição 1: se o registro já existir
            if (isset($data[$i]['faturamentos_financeiro_id']) && $data[$i]['faturamentos_financeiro_id'] != "") {
                $where['faturamentos_financeiro_id'] = $data[$i]['faturamentos_financeiro_id'];
                $update = $this->update('faturamentos_financeiro', $data[$i], $where);
                if (!$update) {
                    $response['status'] = 'error';
                    $response['status-message'] = "Ocorreu um erro ao cadastrar";
                    return $response;
                }
            //Condição 2: se o registro não existir
            } else {
                unset($data[$i]['faturamentos_financeiro_id']);
                $d = array(0 => $data[$i]);
                $update = $this->addFinanceiro($d, $id);
                if (!$update) {
                    $response['status'] = 'error';
                    $response['status-message'] = "Ocorreu um erro ao cadastrar";
                    return $response;
                }
            }
        }
        // |---------------------|
        // |   Loop de remoção   |
        // |---------------------|
        for ($i = 0; $i < count($old_data); $i++) {
            if (in_array($old_data[$i]['faturamentos_financeiro_id'], $remover)) {
                $update = $this->disableFinanceiro($old_data[$i]['faturamentos_financeiro_id']);
                if (!$update) {
                    $response['status'] = 'error';
                    $response['status-message'] = "Ocorreu um erro ao excluir a parcela";
                    return $response;
                }
            }
        }
        $response['status'] = 'success';
        $response['status-message'] = 'O faturamento foi atualizado com sucesso!';

        return $response;
    }

    public function disable(int $faturamentos_id) {
        $response['status'] = 'error';

        if (!$this->isFaturamentoIdValid($faturamentos_id))
		{
            $response['status-message'] = 'A operação não existe ou já foi deletada';
            return $response;
        }
        
        $where['faturamentos_id'] = $faturamentos_id;
        $data['enabled'] = '0';
        $update =  $this->update('faturamentos', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A operação foi deletado com sucesso!';
            return $response;
        }
    }

    public function disableFinanceiro(int $id) {
        $response['status'] = 'error';

        if (!$this->isFinanceiroIdValid($id))
		{
            $response['status-message'] = 'A operação não existe ou já foi deletada';
            return $response;
        }
        
        $where['faturamentos_financeiro_id'] = $id;
        $data['enabled'] = '0';
        $update =  $this->update('faturamentos_financeiro', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A operação foi deletado com sucesso!';
            return $response;
        }
    }

    public function disableCarrinho(int $id) {
        $response['status'] = 'error';

        if (!$this->isCarrinhoIdValid($id))
		{
            $response['status-message'] = 'A operação não existe ou já foi deletada';
            return $response;
        }
        
        $where['faturamentos_carrinho_id'] = $id;
        $data['enabled'] = '0';
        $update =  $this->update('faturamentos_carrinho', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'A operação foi deletado com sucesso!';
            return $response;
        }
    }

    public function getFaturamentoById(int $faturamentos_id) {
        $faturamento = false;
        //Config
        //$conditions['where']['enabled'] = 1;
        //Collums
        //$conditions['where']['faturamentos_id'] = $faturamentos_id;
        //Mount Query
        $conditions['custom_where_query'] = "INNER JOIN (clientes) ON faturamentos.clientes_id = clientes.clientes_id INNER JOIN (empresas) ON faturamentos.empresas_id = empresas.empresas_id  INNER JOIN (marketplaces) ON faturamentos.marketplace_id = marketplaces.marketplace_id WHERE faturamentos.enabled = '1' AND faturamentos.faturamentos_id = '$faturamentos_id'";
        $conditions['select'] = 'faturamentos.*,  UPPER(empresas.nome_fantasia) AS empresa_nome_fantasia, COALESCE(clientes.nome, clientes.nome_fantasia) as nome_cliente, marketplaces.nome as nome_martkeplace, DATE_FORMAT(faturamentos.data_cadastro,"%d/%m/%Y") AS data_cadastro, DATE_FORMAT(faturamentos.created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(faturamentos.modified,"%d/%m/%Y ás %H:%i:%s") AS modified';


        //$conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $data =  $this->getRows('faturamentos', $conditions);
        if($data['gotData']){

            $faturamento = $data['data'][0];
            
            $carrinho = $this->getCarrinho($faturamento['faturamentos_id']);
            $carrinho['carrinho'] = $carrinho['data'];
            unset($carrinho['data']);

            $financeiro = $this->getFinanceiro($faturamento['faturamentos_id']);
            $financeiro['financeiro'] = $financeiro['data'];
            unset($financeiro['data']);

         /*    $clientesApi = new ClientesApi();
            $cliente = $clientesApi->getClienteById($faturamento['clientes_id']);
            if (is_null($cliente['data'][0]['nome_fantasia'])) {
                $cliente = $cliente['data'][0]['nome'];
            } else {
                $cliente = $cliente['data'][0]['nome_fantasia'];
            }
            $faturamento['clientes'] = $cliente; */

            /* $vendedoresApi = new VendedoresApi();
            $vendedores = $vendedoresApi->getVendedorById($faturamento['vendedores_id']);
            $vendedores = $vendedores['data'][0]['nome'];
            $faturamento['vendedores'] = $vendedores;
    */
    /*       $transportadorasApi = new TransportadorasApi();
            $trans = $transportadorasApi->getTransportadoraById($faturamento['transportadoras_id']);
            if (is_null($trans['data'][0]['nome_fantasia'])) {
                $trans = $trans['data'][0]['nome'];
            } else {
                $trans = $trans['data'][0]['nome_fantasia'];
            } */
        // $faturamento['transportadoras'] = $trans;
            //var_dump($carrinho);
            $produtosApi = new ProdutosApi();
            for ($i = 0; $i < count($carrinho['carrinho']); $i++) {
                if ($carrinho['carrinho'][$i]['pos'] == 0) {
                    $carrinho['carrinho'][$i]['prodname'] = $produtosApi->getProdutoById($carrinho['carrinho'][$i]['c_prod'])['data'][0]['descricao'];
                } else {
                    $carrinho['carrinho'][$i]['prodname'] = $carrinho['carrinho'][$i]['c_prod'];
                }
            }
            //die("<br><br>" . var_dump($carrinho['carrinho'][0]));

            $faturamento = array_merge($faturamento, $carrinho);
            $faturamento = array_merge($faturamento, $financeiro);
        }

        //var_dump($faturamento);
        return $faturamento;
}

    public function isFaturamentoIdValid(int $faturamentos_id): bool {
        $conditions['where']['faturamentos_id'] = $faturamentos_id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('faturamentos', $conditions);
        return $response['gotData'];
    }

    public function isFinanceiroIdValid(int $id): bool {
        $conditions['where']['faturamentos_financeiro_id'] = $id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('faturamentos_financeiro', $conditions);
        return $response['gotData'];
    }

    public function isCarrinhoIdValid(int $id): bool {
        $conditions['where']['faturamentos_carrinho_id'] = $id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('faturamentos_carrinho', $conditions);
        return $response['gotData'];
    }

    public function hasFaturamento(int $pedidoIdBling, int $marketplaceId, int $empresaId): bool {
        $conditions['select'] = 'count(*)';
        $conditions['custom_where_query']= 'WHERE pedido_id_bling = ' . $pedidoIdBling . ' AND  marketplace_id = '. $marketplaceId  . ' AND  empresas_id = ' .  $empresaId;
        $response =  $this->getRows('faturamentos', $conditions);
        return (bool) $response['recordsTotal'];
    }
}
