<?php

namespace App\Models;

use PDO;
use App\Models\EmpresasApi;
use App\Models\ProdutosApi;
/**
 * Example user model
 *
 * PHP version 7.0
 */
class KitsApi extends \App\Models\CrudInit
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $empresas_id = "", string $cod_sku = "", string $descricao = "",  string $enabled = "", int $offset = 0, int $limit = 15)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['order_by'] = 'kits_id DESC';
        //Collums
        if (is_int(intval($empresas_id)) && intval($empresas_id) > 0){
            $conditions['where']['empresas_id'] = "%$empresas_id%";
        }
        $conditions['where']['cod_sku'] = "%$cod_sku%";
        $conditions['where']['descricao'] = "%$descricao%";
        $conditions['where']['enabled'] =  "%$enabled%";
        $conditions['where']['deleted'] =  "0";

        //Mount Query
        $conditions['select'] = 'kits_id, empresas_id, cod_sku, descricao, produtos_data, enabled as status';
        $response =  $this->getRows('produtos_kits', $conditions);
        if($response['gotData']){

            for ($i=0; $i < sizeof($response['data']); $i++) { 

                $empresas_id = $response['data'][$i]['empresas_id'];
                $empresasApi = new EmpresasApi();
                $empresa_data = $empresasApi->getEmpresaById($empresas_id);
                if($empresa_data['gotData']){
                    $response['data'][$i]['empresa'] = $empresa_data['data'][0]['nome_fantasia'];
                }else{
                    $response['data'][$i]['empresa'] = 'N/A';
                }
            }
        }
        return $response;
    }

        //Select2 Method GET
        public function simpleList(string $query = "", $empresas_id = null, int $offset = 0, int $limit = 999)
        {
            //Config
            $conditions['return_type'] = 'all';
            $conditions['limit'] = 999;
            $conditions['offset'] = $offset;
            //$conditions['where']['enabled'] = 1;
            $conditions['order_by'] = 'kits_id DESC';
            //Collums
            if($empresas_id){
                $empresas_query = "AND empresas_id = '$empresas_id'";
            }
            //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
            //$conditions['where']['cnpj'] = "%$cnpj%";
            //Mount Query
            $conditions['select'] = '*, kits_id AS id, CONCAT( descricao ," - ", cod_sku ) AS text , DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
            $conditions['custom_where_query'] = "WHERE (cod_sku LIKE '%$query%'  $empresas_query  AND enabled = '1') OR (descricao LIKE '%$query%'  $empresas_query  AND enabled = '1')";
            $response =  $this->getRows('produtos_kits', $conditions);
            return $response;
        }

    public function getKitById(int $kits_id)
    {
        //Config
        $conditions['where']['deleted'] = '0';
        //Collums
        $conditions['where']['kits_id'] = $kits_id;
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('produtos_kits', $conditions);
        //var_dump($response);
        $empresas_id = $response['data'][0]['empresas_id'];
        $empresasApi = new EmpresasApi();
        $empresa_data = $empresasApi->getEmpresaById($empresas_id);
        if($empresa_data['gotData']){
            $response['data'][0]['empresa'] = $empresa_data['data'][0]['nome_fantasia'];
        }else{
            $response['data'][0]['empresa'] = 'N/A';
        }
        return $response;
    }

    public function getKitBySKU_EAN(string $code)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['cnpj'] = $cnpj;
        //Mount Query
        $conditions['select'] = '*, UPPER(descricao) AS descricao, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('produtos_kits', $conditions);
        return $response;
    }


    public function add(array $data)
    {
        //return $data;
        $response['status'] = 'error';

        $produtosApi = new ProdutosApi();

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }

        if(array_key_exists("produtos_data",$data)){
            for ($i_prod_data=0; $i_prod_data < sizeof($data['produtos_data']); $i_prod_data++) { 
                ;

                if(!$produtosApi->getProdutoById($data['produtos_data'][$i_prod_data]['produtos_id']))
                {
                    $response['status-message'] = 'O ID '.$data['produtos_data'][$i_prod_data]['produtos_id'].' do produto não existe.';
                    return $response;
                }
        
            }

            $data['produtos_data'] = json_encode($data['produtos_data']);
        }else{
            $response['status-message'] = 'Selecione produtos para o kit';
            return $response;
        }


        extract($data);
        //if($cnpj !== $this->getKitById($kits_id)['data'][0]['kits_id']){

            if ($this->isKitCodeSkuExist($cod_sku))
            {
                $response['status-message'] = 'Este SKU do Kit já está cadastrado';
                return $response;
            }

            if ($produtosApi->isProdutoCodeSkuExist($cod_sku))
            {
                $response['status-message'] = 'Este SKU é referente a um produto já cadastrado';
                return $response;
            }
       
        //}


        $insert =  $this->insert('produtos_kits', $data);

        if($insert){
            $response['kits_id'] = $insert;
            $response['status'] = 'success';
            $response['status-message'] = 'O kit foi registrado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }



    public function edit(array $data, int $kits_id)
    {

        $response['status'] = 'error';

        $produtosApi = new ProdutosApi();

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }

        if(array_key_exists("produtos_data",$data)){
            for ($i_prod_data=0; $i_prod_data < sizeof($data['produtos_data']); $i_prod_data++) { 
                ;

                if(!$produtosApi->getProdutoById(intval($data['produtos_data'][$i_prod_data]['produtos_id'])))
                {
                    $response['status-message'] = 'O ID '.$data['produtos_data'][$i_prod_data]['produtos_id'].' do produto não existe.';
                    return $response;
                }
        
            }
            $data['produtos_data'] = json_encode($data['produtos_data']);

        }else{
            $response['status-message'] = 'Selecione produtos para o kit';
            return $response;
        }


        extract($data);
        if($cod_sku !== $this->getKitById($kits_id)['data'][0]['cod_sku']){

            if ($this->isKitCodeSkuExist($cod_sku))
            {
                $response['status-message'] = 'Este SKU do Kit já está cadastrado';
                return $response;
            }
        }

            if ($produtosApi->isProdutoCodeSkuExist($cod_sku))
            {
                $response['status-message'] = 'Este SKU é referente a um produto já cadastrado';
                return $response;
            }

        $where['kits_id'] = $kits_id;
        $update =  $this->update('produtos_kits', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O kit foi atualizado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }


    public function deleteKit(int $kits_id)
    {
        $response['status'] = 'error';

        if (!$this->isKitIdValid($kits_id))
		{
            $response['status-message'] = 'O kit não existe, ou já foi deletado';
            return $response;
        }
        
        $where['kits_id'] = $kits_id;
        $data['deleted'] = '1';
        $update =  $this->update('produtos_kits', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O produto foi deletado com sucesso!';
            return $response;
        }
    }

    public function disable(int $kits_id, int $enabled)
    {
        $response['status'] = 'error';

        if (!$this->isKitIdValid($kits_id))
		{
            $response['status-message'] = 'O kit não existe, ou foi deletado';
            return $response;
        }
        
        $where['kits_id'] = $kits_id;
        $data['enabled'] = "$enabled";
        $update =  $this->update('produtos_kits', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O status do Kit foi alterado com sucesso!';
            return $response;
        }
    }


    /* Verifica se a empresa existe */
    public function isKitIdValid(int $kits_id): bool
    {
        $conditions['where']['kits_id'] = $kits_id;
        $conditions['where']['deleted'] = 0;
        $response =  $this->getRows('produtos_kits', $conditions);
        return $response['gotData'];
    }

    /* Verifica se a empresa pelo CNPJ */
    public function isKitCodeSkuExist(string $cod_sku):bool
    {
        $conditions['where']['cod_sku'] = $cod_sku;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('produtos_kits', $conditions);
        return $response['gotData'];
    }


}
