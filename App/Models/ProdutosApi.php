<?php

namespace App\Models;

use PDO;
use App\Models\EmpresasApi;
/**
 * Example user model
 *
 * PHP version 7.0
 */
class ProdutosApi extends \App\Models\CrudInit
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function list(string $empresas_id = "", string $cod_sku = "", string $barcode = "", string $descricao = "", int $offset = 0, int $limit = 15)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = $limit;
        $conditions['offset'] = $offset;
        $conditions['order_by'] = 'produtos_id DESC';
        //Collums
        if (is_int(intval($empresas_id)) && intval($empresas_id) > 0){
            $conditions['where']['empresas_id'] = "$empresas_id";
        }
        $conditions['where']['cod_sku'] = "%$cod_sku%";
        $conditions['where']['barcode'] = "%$barcode%";
        $conditions['where']['descricao'] = "%$descricao%";
        $conditions['where']['enabled'] = 1;

        //Mount Query
        $conditions['select'] = 'produtos_id, empresas_id, cod_sku, barcode, descricao';
        $response =  $this->getRows('produtos', $conditions);
        if($response['gotData']){

            for ($i=0; $i < sizeof($response['data']); $i++) { 

                $empresas_id = $response['data'][$i]['empresas_id'];
                $empresasApi = new EmpresasApi();
                $empresa_data = $empresasApi->getEmpresaById($empresas_id);
                if($empresa_data['gotData']){
                    $response['data'][$i]['empresa'] = $empresa_data['data'][0]['nome_fantasia'];
                }else{
                    $response['data'][$i]['empresa'] = 'N/A';
                }
            }
        }
        return $response;
    }
    //Select2 Method GET
    public function simpleList(string $query = "", $empresas_id = null, int $offset = 0, int $limit = 999)
    {
        //Config
        $conditions['return_type'] = 'all';
        $conditions['limit'] = 999;
        $conditions['offset'] = $offset;
        //$conditions['where']['enabled'] = 1;
        $conditions['order_by'] = 'produtos_id DESC';
        //Collums
        //$conditions['where']['nome_fantasia'] = "%$nome_fantasia%";
        //$conditions['where']['cnpj'] = "%$cnpj%";
        //Mount Query
        if($empresas_id){
            $empresas_query = "AND empresas_id = '$empresas_id'";
        }

        $conditions['select'] = '*, produtos_id AS id, CONCAT( descricao ," - ", cod_sku ) AS text , DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $conditions['custom_where_query'] = "WHERE (cod_sku LIKE '%$query%'  $empresas_query  AND enabled = '1') OR (barcode LIKE '%$query%'  $empresas_query  AND enabled = '1') OR (descricao LIKE '%$query%'  $empresas_query  AND enabled = '1')";
        $response =  $this->getRows('produtos', $conditions);
        return $response;
    }

    public function getProdutoById($produtos_id)
    {
        //Config
        $conditions['where']['enabled'] = 1;
        //Collums
        $conditions['where']['produtos_id'] = $produtos_id;
        //Mount Query
        $conditions['select'] = '*, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('produtos', $conditions);
        if($response['gotData']){
            $empresas_id = $response['data'][0]['empresas_id'];
            $empresasApi = new EmpresasApi();
            $empresa_data = $empresasApi->getEmpresaById($empresas_id);
            if($empresa_data['gotData']){
                $response['data'][0]['empresa'] = $empresa_data['data'][0]['nome_fantasia'];
            }else{
                $response['data'][0]['empresa'] = 'N/A';
            }
        }else{
            $response =  $response['gotData'];
        }

        return $response;
    }

    public function getProdutoBySKU_EAN(string $code)
    {
        //Config
        //Collums
        $conditions['limit'] = 1;

        $conditions['custom_where_query'] = "WHERE (cod_sku LIKE '$code' OR barcode LIKE '$code') AND enabled = 1";
        //Mount Query
        $conditions['select'] = '*, UPPER(descricao) AS descricao, DATE_FORMAT(created,"%d/%m/%Y ás %H:%i:%s") AS created, DATE_FORMAT(modified,"%d/%m/%Y ás %H:%i:%s") AS modified';
        $response =  $this->getRows('produtos', $conditions);
        //var_dump($response);
        if($response['gotData']){
            $empresas_id = $response['data'][0]['empresas_id'];
            $empresasApi = new EmpresasApi();
            $empresa_data = $empresasApi->getEmpresaById($empresas_id);
            if($empresa_data['gotData']){
                $response['data'][0]['empresa'] = $empresa_data['data'][0]['nome_fantasia'];
            }else{
                $response['data'][0]['empresa'] = 'N/A';
            }
        }else{
            $response =  $response['gotData'];
        }

        return $response;
    }


    public function add(array $data)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);

        $insert =  $this->insert('produtos', $data);

        if($insert){
            $response['produtos_id'] = $insert;
            $response['status'] = 'success';
            $response['status-message'] = 'O produto foi registrado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }

    public function uploadPhoto(int $produtos_id, $file){

        if(!is_dir("uploads/produtos/$produtos_id")){

            if(mkdir("uploads/produtos/$produtos_id")){
                //echo "created;";
            } 
        }

        $target_path = "uploads/produtos/$produtos_id/" . md5(uniqid(rand(), true)) . '_' . $file['name'][0];
        $data['image_path'] = $target_path;
        //$status['md5'] = md5_file($file_tmp_name);
        if(move_uploaded_file($file['tmp_name'][0], $target_path)){
            $where['produtos_id'] = $produtos_id;
            $update = $this->update('produtos', $data, $where);


            $response['status'] = 'success';
            $response['status-message'] = 'Os dados foram inseridos com sucesso';

        }else{
            $response['status'] = 'error';
            $response['status-message'] = 'Occoreu um erro, entre em contato com o administrador.';
        }
        return $response;

     

    }

    public function edit(array $data, int $produtos_id)
    {

        $response['status'] = 'error';

        if (empty($data)) {
            $response['status-message'] = 'Preencha todos os campos.';
            return $response;
        }
        extract($data);



        if (!$this->getProdutoById($produtos_id))
        {
            $response['status-message'] = 'O ID do produto não existe.';
            return $response;
        }

    
        if($cod_sku !== $this->getProdutoById($produtos_id)['data'][0]['cod_sku']){

            if ($this->isProdutoCodeSkuExist($cod_sku))
            {
                $response['status-message'] = 'Este SKU já está cadastrado';
                return $response;
            }
        }
        if($barcode !== $this->getProdutoById($produtos_id)['data'][0]['barcode']){
            if ($this->isProdutoBarCodeExist($barcode))
            {
                $response['status-message'] = 'Este Código de Barras(EAN) já está cadastrado';
                return $response;
            }
        }

        $where['produtos_id'] = $produtos_id;
        $update =  $this->update('produtos', $data, $where);

        if($update){
            $response['produtos_id'] = $produtos_id;
            $response['status'] = 'success';
            $response['status-message'] = 'O produto foi atualizado com sucesso!';
        }else{
            $response['status-message'] = 'Occoreu um erro na inserção, tente novamente.!';
        }
        return $response;
    }


    public function disable(int $produtos_id)
    {
        $response['status'] = 'error';

        if (!$this->isProdutoIdValid($produtos_id))
		{
            $response['status-message'] = 'A empresa não existe, ou já foi deletada';
            return $response;
        }
        
        $where['produtos_id'] = $produtos_id;
        $data['enabled'] = '0';
        $update =  $this->update('produtos', $data, $where);

        if($update){
            $response['status'] = 'success';
            $response['status-message'] = 'O produto foi deletado com sucesso!';
            return $response;
        }
    }

    /* Verifica se a empresa existe */
    public function isProdutoIdValid(int $produtos_id): bool
    {
        $conditions['where']['produtos_id'] = $produtos_id;
        $conditions['where']['enabled'] = 1;
        $response =  $this->getRows('produtos', $conditions);
        return $response['gotData'];
    }

    /* Verifica se a empresa pelo CNPJ */
    public function isProdutoCodeSkuExist(string $cod_sku):bool
    {
        $conditions['where']['cod_sku'] = $cod_sku;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('produtos', $conditions);
        return $response['gotData'];
    }

    /* Verifica se a empresa pelo CNPJ */
    public function isProdutoBarCodeExist(string $barcode):bool
    {
        $conditions['where']['barcode'] = $barcode;
        $conditions['where']['enabled'] = 1;
        $conditions['select'] = '*';
        $response =  $this->getRows('produtos', $conditions);
        return $response['gotData'];
    }

    public function existProdutoForEmpresaId(string $codigo, int $empresaId)
    {
        $conditions['custom_where_query'] = "WHERE cod_sku = '$codigo' AND empresas_id = $empresaId";
        $conditions['select'] = 'produtos_id';
        $response =  $this->getRows('produtos', $conditions);
        return $response['gotData'] ? $response['data'][0]['produtos_id'] : 0;
    }
}
