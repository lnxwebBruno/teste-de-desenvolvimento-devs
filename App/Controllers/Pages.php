<?php

namespace App\Controllers;

use \Core\View;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Pages extends \Core\Controller
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function guiaAction()
    {
        $arg =  $this->route_params;
        View::renderTemplate('Pages/guia.html', $arg);;
    }
 
}
