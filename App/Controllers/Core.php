<?php

namespace App\Controllers;

use \Core\View;
use App\Models\EmpresasApi;
/**
 * Home controller
 *
 * PHP version 7.0
 */
class Core extends \Core\Controller
{

    public $id;
    public $singular_name = "Core Management";
    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $arg =  $this->route_params;
        $arg['singular_name'] = $this->singular_name;
        View::renderTemplate('Core/index.html', $arg);
    }

    public function editAction()
    {
        $arg =  $this->route_params;
    
        $empresasApi = new EmpresasApi();

        $empresa_data = $empresasApi->getEmpresaById(intval($this->route_params['id']))['data'][0];

        $arg['singular_name'] = $this->singular_name;
        
        $arg = array_merge($arg, $empresa_data);
        //var_dump($arg);
        View::renderTemplate('Core/edit.html', $arg);
    
    }
    public function novoAction()
    {
        $empresasApi = new EmpresasApi();

        $arg =  $this->route_params;
        $arg['singular_name'] = $this->singular_name;
        View::renderTemplate('Core/add.html', $arg);
    
    }

    //Api
    public function listAction()
    {

        if(isset($_POST['columns'][1]['search']['value'])){
            $nome_fantasia = $_POST['columns'][1]['search']['value'];
        }else{
            $nome_fantasia = "";
        }

        if(isset($_POST['columns'][3]['search']['value'])){
            $cnpj = $_POST['columns'][3]['search']['value'];
        }else{
            $cnpj = "";
        }

        if(isset($_POST['start'])){
            $offset = $_POST['start'];
        }else{
            $offset = 0;
        }

        if(isset($_POST['length'])){
            $limit = $_POST['length'];
        }else{
            $limit = 15;
        }

        if(isset($_POST['draw'])){
            $draw = $_POST['draw'];
        }else{
            $draw = 1;
        }

        $empresasApi = new EmpresasApi();

        $response = $empresasApi->list($nome_fantasia, $cnpj, $offset, $limit);

        $response['draw'] =  $draw;
        //View
        //var_dump($list);

        header('Content-Type: application/json');
        echo json_encode($response);    
    }


    public function simpleListAction()
    {

        if(isset($_POST['start'])){
            $offset = $_POST['start'];
        }else{
            $offset = 0;
        }

        if(isset($_POST['length'])){
            $limit = $_POST['length'];
        }else{
            $limit = 15;
        }

        if(isset($_GET['q'])){
            $query = addslashes($_GET['q']);
        }else{
            $query = "";
        }

        $empresasApi = new EmpresasApi();

        $response = $empresasApi->simpleList($query, $offset, $limit);

        //$response['draw'] =  $draw;
        //View
        //var_dump($_GET);

        header('Content-Type: application/json');
        echo json_encode($response);    
    }
    public function disableAction()
    {   

        $response['status'] = 'error';
        $response['status-message'] = 'Missing Data';

        if(isset($_POST['empresas_id'])  && !empty($_POST['empresas_id'])){
            $empresa_id = intval($_POST['empresas_id']);
            $empresasApi = new EmpresasApi();
            $response = $empresasApi->disable($empresa_id);
        }
    
        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }

    public function disable_marketplaceAction()
    {   

        $response['status'] = 'error';
        $response['status-message'] = 'Missing Data';

        if(isset($_POST['loja_id'])  && !empty($_POST['loja_id'])){
            $empresa_id = intval($_POST['loja_id']);
            $empresasApi = new EmpresasApi();
            $response = $empresasApi->disable_marketplace($empresa_id);
        }
    
        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }

    public function addAction()
    {

        $response['status'] = 'error';
        $response['status-message'] = 'Por favor preecnha todos os campos';

        $empresasApi = new EmpresasApi();
        $response = $empresasApi->add($_POST);

        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }

    public function add_marketplaceAction()
    {

        $response['status'] = 'error';
        $response['status-message'] = 'Por favor preecnha todos os campos';

        $empresasApi = new EmpresasApi();
        /*echo "<pre>";
        var_dump($_POST);
        echo "</pre>";
        die();*/
        $response = $empresasApi->add_marketplace($_POST, intval($this->route_params['id']));

        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }

    public function uploadPhoto()
    {
        $response['status'] = 'error';
        $response['status-message'] = 'Por favor preencha todos os campos';
        //var_dump($_POST);
        //var_dump($_FILES);
        if(isset($_POST['empresas_lojas_id']) && isset($_FILES)){

            $empresas_lojas_id = intval($_POST['empresas_lojas_id']);
            $empresasApi = new EmpresasApi();
            $response = $empresasApi->uploadPhoto($empresas_lojas_id, $_FILES['upload']);
        }

        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }

    public function updateAction()
    {
        $response['status'] = 'error';
        $response['status-message'] = 'Por favor preecnha todos os campos';

        $empresasApi = new EmpresasApi();

       // var_dump($_POST);
        $response = $empresasApi->edit($_POST, intval($this->route_params['id']));

        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }

    public function update_marketplaceAction()
    {
        $response['status'] = 'error';
        $response['status-message'] = 'Por favor preecnha todos os campos';

        $empresasApi = new EmpresasApi();

       // var_dump($_POST);
        $response = $empresasApi->edit_marketplace($_POST, intval($this->route_params['id']));

        //View
        header('Content-Type: application/json');
        echo json_encode($response);    
    }
}