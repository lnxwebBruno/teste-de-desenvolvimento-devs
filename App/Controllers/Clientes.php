<?php

namespace App\Controllers;

use \Core\View;

use App\Models\ClientesApi;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Clientes extends \Core\Controller
{

    public $id;
    public $singular_name = "Cliente";
    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $arg =  $this->route_params;
        $arg['singular_name'] = $this->singular_name;
        View::renderTemplate('Clientes/index.html', $arg);
    }

    public function editAction()
    {
        $arg =  $this->route_params;
        $arg['singular_name'] = $this->singular_name;
        //var_dump($arg);
        View::renderTemplate('Clientes/edit.html', $arg);
    
    }
    public function novoAction()
    {
        $arg =  $this->route_params;
        $arg['singular_name'] = $this->singular_name;
        View::renderTemplate('Clientes/add.html', $arg);
    
    }
}
